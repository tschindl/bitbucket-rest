/*******************************************************************************
 * Copyright (c) 2013 BestSolution.at and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Tom Schindl<tom.schindl@bestsolution.at> - initial API and implementation
 *******************************************************************************/
package at.bestsolution.bitbucket.rest;

import java.util.List;
import java.util.Map;

import at.bestsolution.bitbucket.rest.model.repositories.RepChangeset;
import at.bestsolution.bitbucket.rest.model.repositories.RepChangesetComment;
import at.bestsolution.bitbucket.rest.model.repositories.RepChangesetDiff;
import at.bestsolution.bitbucket.rest.model.repositories.RepChangesetLike;
import at.bestsolution.bitbucket.rest.model.repositories.RepChangesetDiffStat;
import at.bestsolution.bitbucket.rest.model.repositories.RepIssue;
import at.bestsolution.bitbucket.rest.model.repositories.RepIssueComment;
import at.bestsolution.bitbucket.rest.model.repositories.RepIssueComponent;
import at.bestsolution.bitbucket.rest.model.repositories.RepIssueMilestone;
import at.bestsolution.bitbucket.rest.model.repositories.RepIssueVersion;
import at.bestsolution.bitbucket.rest.model.repositories.RepPullRequestComment;
import at.bestsolution.bitbucket.rest.model.repositories.RepPullRequestLike;
import at.bestsolution.bitbucket.rest.model.repositories.RepRepository;
import at.bestsolution.bitbucket.rest.model.repositories.RepSlugBranch;
import at.bestsolution.bitbucket.rest.model.repositories.RepSlugFileHistory;
import at.bestsolution.bitbucket.rest.model.repositories.RepSlugTag;
import at.bestsolution.bitbucket.rest.model.user.UserUser;
import at.bestsolution.bitbucket.rest.util.ProgressMonitor;
import at.bestsolution.bitbucket.rest.util.ResponseObject;
import at.bestsolution.bitbucket.rest.util.SegmentedList;

public interface BitbucketRepositoriesService {
	public ResponseObject<RepRepository> getRepository(ProgressMonitor monitor, String slug);
	public ResponseObject<RepRepository> getRepository(ProgressMonitor monitor, String accountname, String slug);
	
	/* ------------------------------------------------------------------------
	 * Changeset APIS
	 * ------------------------------------------------------------------------
	 */
	public ResponseObject<SegmentedList<RepChangeset>> getSegmentedChangesets(ProgressMonitor monitor, String accountname, String slug);
	public ResponseObject<SegmentedList<RepChangeset>> getSegmentedChangesets(ProgressMonitor monitor, String accountname, String slug, Integer segmentSize);
	
	public ResponseObject<List<RepChangeset>> getChangesets(ProgressMonitor monitor, String accountname, String slug);
	public ResponseObject<List<RepChangeset>> getChangesets(ProgressMonitor monitor, String accountname, String slug, Integer limit, String start);
	
	public ResponseObject<RepChangeset> getChangeset(ProgressMonitor monitor, String accountname, String slug, String rawNode);
	public ResponseObject<List<RepChangesetDiffStat>> getChangesetStats(ProgressMonitor monitor, String accountname, String slug, String rawNode);
	public ResponseObject<List<RepChangesetDiff>> getChangesetDiffs(ProgressMonitor monitor, String accountname, String slug, String rawNode);
	public ResponseObject<List<RepChangesetLike>> getChangesetLikes(ProgressMonitor monitor, String accountname, String slug, String rawNode);
	public ResponseObject<RepChangesetLike> getChangesetLike(ProgressMonitor monitor, String accountname, String slug, String rawNode, String likeraccount);
	public ResponseObject<List<RepChangesetComment>> getChangesetComments(ProgressMonitor monitor, String accountname, String slug, String rawNode);
	
	
	/* ------------------------------------------------------------------------
	 * Issue APIS
	 * ------------------------------------------------------------------------
	 */
	public ResponseObject<SegmentedList<RepIssue>> getSegmentedIssues(ProgressMonitor monitor, String accountname, String slug);
	public ResponseObject<SegmentedList<RepIssue>> getSegmentedIssues(ProgressMonitor monitor, String accountname, String slug, Integer segmentSize, Integer start);
	
	public ResponseObject<List<RepIssue>> getIssues(ProgressMonitor monitor, String accountname, String slug);
	public ResponseObject<List<RepIssue>> getIssues(ProgressMonitor monitor, String accountname, String slug, Integer limit, Integer start);
	
	public ResponseObject<RepIssue> getIssue(ProgressMonitor monitor, String accountname, String slug, int issueId);
	
	public ResponseObject<List<UserUser>> getIssueFollowers(ProgressMonitor monitor, String accountname, String slug, int issueId);

	public ResponseObject<List<RepIssueComment>> getIssueComments(ProgressMonitor monitor, String accountname, String slug, int issueId);
	public ResponseObject<RepIssueComment> getIssueComment(ProgressMonitor monitor, String accountname, String slug, int issueId, int commentId);
	
	public ResponseObject<List<RepIssueComponent>> getIssueComponents(ProgressMonitor monitor, String accountname, String slug); 
	public ResponseObject<RepIssueComponent> getIssueComponent(ProgressMonitor monitor, String accountname, String slug, int id);
	
	public ResponseObject<List<RepIssueVersion>> getIssueVersions(ProgressMonitor monitor, String accountname, String slug);
	public ResponseObject<RepIssueVersion> getIssueVersion(ProgressMonitor monitor, String accountname, String slug, int id);
	
	public ResponseObject<List<RepIssueMilestone>> getIssueMilestones(ProgressMonitor monitor, String accountname, String slug);
	public ResponseObject<RepIssueMilestone> getIssueMilestone(ProgressMonitor monitor, String accountname, String slug, int id);
	
	
	/* ------------------------------------------------------------------------
	 * Pull APIS
	 * ------------------------------------------------------------------------
	 */
	
	public ResponseObject<List<RepPullRequestComment>> getPullRequestComments(ProgressMonitor monitor, String accountname, String slug, int pullRequestId);
	public ResponseObject<RepPullRequestComment> getPullRequestComment(ProgressMonitor monitor, String accountname, String slug, int pullRequestId, int commentId);
	public ResponseObject<List<RepPullRequestLike>> getPullRequestLikes(ProgressMonitor monitor, String accountname, String slug, int pullRequestId);
	public ResponseObject<RepPullRequestLike> getPullRequestLike(ProgressMonitor monitor, String accountname, String slug, int pullRequestId, String likeraccount);
	
	/* ------------------------------------------------------------------------
	 * RepSlug APIS
	 * ------------------------------------------------------------------------
	 */
	
	public ResponseObject<Map<String,RepSlugBranch>> getBranches(ProgressMonitor monitor, String accountname, String slug);
	public ResponseObject<String> getMainBranch(ProgressMonitor monitor, String accountname, String slug);
	public ResponseObject<Map<String, RepSlugTag>> getTags(ProgressMonitor monitor, String accountname, String slug);
	public ResponseObject<List<RepSlugFileHistory>> getFileHistory(ProgressMonitor monitor, String accountname, String slug, String node, String path);
}
