/*******************************************************************************
 * Copyright (c) 2013 BestSolution.at and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Tom Schindl<tom.schindl@bestsolution.at> - initial API and implementation
 *******************************************************************************/
package at.bestsolution.bitbucket.rest.impl;

import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map.Entry;

import at.bestsolution.bitbucket.rest.BitbucketRepositoriesService;
import at.bestsolution.bitbucket.rest.BitbucketService;
import at.bestsolution.bitbucket.rest.BitbucketSession;
import at.bestsolution.bitbucket.rest.impl.services.util.Constants;
import at.bestsolution.bitbucket.rest.impl.services.util.EncodingUtils;
import at.bestsolution.bitbucket.rest.model.repositories.RepIssue;
import at.bestsolution.bitbucket.rest.util.ProgressMonitor;
import at.bestsolution.bitbucket.rest.util.ResponseObject;
import at.bestsolution.bitbucket.rest.util.SegmentedList;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

public class BitbucketServiceImpl implements BitbucketService {
	private static int bufferSize = 8192;

	@Override
	public BitbucketSession createSession(ProgressMonitor monitor,
		String username, String password) {
		ServiceEndpoint w = new ServiceEndpoint(
					"https://api.bitbucket.org/1.0/user/", username, password);
		ServiceResponse<JsonElement> r = w.get();
		if( r.isOk() ) {
			return new BitbucketSessionImpl(username, password, Constants.SERVICE_URL, Constants.SERVICE_VERSION);
		}

		return null;
	}
	
	public static void main(String[] args) {
		BitbucketServiceImpl i = new BitbucketServiceImpl();
		BitbucketSession session = i.createSession(ProgressMonitor.NULLMONITOR, args[0], args[1]);
//		BitbucketUserService service = session.createUserService();
//		ResponseObject<List<UserRepository>> rv = service.getRepositories(ProgressMonitor.NULLMONITOR);
//		if( rv.isOk() ) {
//			for( UserRepository r : rv.value() ) {
//				System.err.println(r);
//			}
//		} else {
//			System.err.println(rv.getException().getMessage());
//		}
		
//		ResponseObject<UserRepositoryOverview> rv = service.getRespositoriesOverview(ProgressMonitor.NULLMONITOR);
//		if( rv.isOk() ) {
//			for( UserOverviewRepository f : rv.value().getUpdated() ) {
//				System.err.println(f);
//			}
//		} else {
//			System.err.println(rv.getException().getMessage());
//		}
		BitbucketRepositoriesService service = session.createRepositoryService();
//		ResponseObject<RepRepository> rv = service.getRepository("bitbucket-rest");
//		if( rv.isOk() ) {
//			System.err.println(rv.value());	
//		} else {
//			System.err.println(rv.getException().getMessage());
//		}
//		
//		ResponseObject<List<RepChangeset>> rv = service.getChangesets(ProgressMonitor.NULLMONITOR, "tschindl", "jfx-rt",15,null);
//		if( rv.isOk() ) {
//			for( RepChangeset c : rv.value() ) {
//				System.err.println(c.getNode() + " => " + c.getUtctimestamp());
//			}
//		} else {
//			System.err.println(rv.getException().getMessage());
//		}
//
//		ResponseObject<SegmentedList<RepChangeset>> rv = service.getSegmentedChangesets(ProgressMonitor.NULLMONITOR, "tschindl", "jfx-rt", 2);
//		
//		if( rv.isOk() ) {
//			SegmentedList<RepChangeset> l = rv.value();
//			for( int j = 0; j < 2; j++ ) {
//				System.err.println("=================================");
//				System.err.println("=================================");
//				System.err.println(l.getSegment(ProgressMonitor.NULLMONITOR, j));
//			}
//		} else {
//			System.err.println(rv.getException().getMessage());
//		}
		
//		ResponseObject<List<RepIssue>> rv = service.getIssues(ProgressMonitor.NULLMONITOR, "tschindl", "bitbucket-rest", 10, null);
//		if( rv.isOk() ) {
//			System.err.println("hello ");
//			for( RepIssue r : rv.value() ) {
//				System.err.println(r.getLocalId());
//			}
//		}
		
		ResponseObject<SegmentedList<RepIssue>> rv = service.getSegmentedIssues(ProgressMonitor.NULLMONITOR, "tschindl", "bitbucket-rest");
		if( rv.isOk() ) {
			SegmentedList<RepIssue> l = rv.value();
			for( int j = 0; j < 2; j++ ) {
				for( RepIssue r : l.getSegment(ProgressMonitor.NULLMONITOR, j) ) {
					System.err.println(r.getLocalId());
				}
				
			}
		}
		
	}
	
	public static ServiceEndpoint createEndpoint(BitbucketSession session, String url) {
		return new ServiceEndpoint(url, session.getUsername(), session.getPassword());
	}
	
	public static class ServiceEndpoint {
		private final String url;
		private final String credentials;

		private ServiceEndpoint(String url, String username, String password) {
			this.url = url;
			this.credentials = "Basic "
					+ EncodingUtils.toBase64(username + ':' + password);
		}

		public <O extends JsonElement> ServiceResponse<O> get() {
			return get(null);
		}
		
		public <O extends JsonElement> ServiceResponse<O> get(HashMap<String, String> params) {
			try {
				String url = this.url;
				
				if( params != null && !params.isEmpty() ) {
					url += "?";
					boolean flag = false;
					for( Entry<String,String> p : params.entrySet() ) {
						if( flag ) {
							url += "&";
						}
						url += p.getKey() + "=" + p.getValue();
						flag = true;
					}
				}
				
				System.err.println("URL: " + url);
				
				HttpURLConnection connection = (HttpURLConnection) new URL(url)
						.openConnection();
				connection.setRequestMethod(Constants.HTTP_GET);
				if (credentials != null) {
					connection.setRequestProperty(
							Constants.HEADER_AUTHORIZATION, credentials);
				}
				
				O rv = parseJson(getStream(connection));
				System.err.println(rv);
				return new ServiceResponse<O>(rv);
			} catch (IOException e) {
				return new ServiceResponse<O>(e);
			}
		}

		private InputStream getStream(HttpURLConnection connection)
				throws IOException {
			if (connection.getResponseCode() < HTTP_BAD_REQUEST)
				return connection.getInputStream();
			else {
				InputStream stream = connection.getErrorStream();
				return stream != null ? stream : connection.getInputStream();
			}
		}

		private static <O extends JsonElement> O parseJson(InputStream stream)
				throws IOException {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					stream, Constants.CHARSET_UTF8), bufferSize);
			JsonParser p = new JsonParser();
			return (O) p.parse(reader);
		}
	}

	public static class ServiceResponse<O extends JsonElement> {
		private final O content;
		private final IOException exception;
		
		private ServiceResponse(O content) {
			this.content = content;
			this.exception = null;
		}
		
		private ServiceResponse(IOException exception) {
			this.content = null;
			this.exception = exception;
		}
		
		public boolean isOk() {
			return exception == null;
		}
		
		public O getContent() {
			return (O)content;
		}
		
		public IOException getException() {
			return exception;
		}
		
		@Override
		public String toString() {
			if( exception != null ) {
				return super.toString() + " - Failure: " + exception.getMessage();
			}
			return super.toString() + "- " + content.toString();
		}
	}
}