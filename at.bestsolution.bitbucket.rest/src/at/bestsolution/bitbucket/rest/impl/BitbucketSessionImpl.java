/*******************************************************************************
 * Copyright (c) 2013 BestSolution.at and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Tom Schindl<tom.schindl@bestsolution.at> - initial API and implementation
 *******************************************************************************/
package at.bestsolution.bitbucket.rest.impl;

import at.bestsolution.bitbucket.rest.BitbucketRepositoriesService;
import at.bestsolution.bitbucket.rest.BitbucketSession;
import at.bestsolution.bitbucket.rest.BitbucketUserService;
import at.bestsolution.bitbucket.rest.BitbucketUsersService;
import at.bestsolution.bitbucket.rest.impl.services.BitbucketRepositoriesServiceImpl;
import at.bestsolution.bitbucket.rest.impl.services.BitbucketUserServiceImpl;

public final class BitbucketSessionImpl implements BitbucketSession {
	
	private final String username;
	private final String password;
	private final String serviceUrl;
	private final String serviceVersion;
	
	public BitbucketSessionImpl(String username, String password, String serviceUrl, String serviceVersion) {
		this.username = username;
		this.password = password;
		this.serviceUrl = serviceUrl;
		this.serviceVersion = serviceVersion;
	}
	
	@Override
	public String getUsername() {
		return username;
	}
	
	@Override
	public String getPassword() {
		return password;
	}
	
	public String getServiceVersion() {
		return serviceVersion;
	}
	
	public String getServiceUrl() {
		return serviceUrl;
	}
	
	@Override
	public BitbucketRepositoriesService createRepositoryService() {
		return new BitbucketRepositoriesServiceImpl(this);
	}

	@Override
	public BitbucketUserService createUserService() {
		return new BitbucketUserServiceImpl(this);
	}

	@Override
	public BitbucketUsersService createUsersService() {
		// TODO Auto-generated method stub
		return null;
	}

}
