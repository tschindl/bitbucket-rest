package at.bestsolution.bitbucket.rest.impl.model.repositories

import at.bestsolution.bitbucket.rest.impl.model.base.BaseObject
import at.bestsolution.bitbucket.rest.model.repositories.RepChangesetComment
import java.util.Date

import static extension at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil.*
import com.google.gson.JsonObject
import at.bestsolution.bitbucket.rest.BitbucketSession

@Data
class RepChangesetCommentImpl extends BaseObject implements RepChangesetComment {
	val String username
	val String node
	val int commentId
	val String displayName
	val Integer parentId
	val boolean deleted
	val Date utcLastUpdated
	val String filenameHash
	val String filename
	val String content
	val String contentRendered
	val String userAvatarUrl
	val Integer lineFrom
	val Integer lineTo
	val Date utcCreatedOn
	val boolean spam
	
	def static toRepChangesetComment(JsonObject o, BitbucketSession session) {
		val _username = o.string("username")
		val _node = o.string("node")
		val _commentId = o.integer("comment_id")
		val _displayName = o.string("display_name")
		val _parentId = o.integerObject("parent_id")
		val _deleted = o.bool("deleted")
		val _utcLastUpdated = o.date("utc_last_updated")
		val _filenameHash = o.string("filename_hash")
		val _filename = o.string("filename")
		val _content = o.string("content")
		val _contentRendered = o.string("content_rendered")
		val _userAvatarUrl = o.string("user_avatar_url")
		val _lineFrom = o.integerObject("line_from")
		val _lineTo = o.integerObject("line_to")
		val _utcCreatedOn = o.date("utc_created_on")
		val _spam = o.bool("is_spam")
		
		val RepChangesetComment rv = new RepChangesetCommentImpl(session,_username,_node,_commentId,_displayName,_parentId,_deleted,_utcLastUpdated,_filenameHash,_filename,_content,_contentRendered,_userAvatarUrl,_lineFrom,_lineTo,_utcCreatedOn,_spam)
		return rv;
	}
}