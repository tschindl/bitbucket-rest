package at.bestsolution.bitbucket.rest.impl.model.repositories

import at.bestsolution.bitbucket.rest.impl.model.base.BaseObject
import at.bestsolution.bitbucket.rest.model.repositories.RepChangesetDiff$FromFile
import at.bestsolution.bitbucket.rest.model.repositories.RepChangesetDiff$Hunk
import java.util.List
import at.bestsolution.bitbucket.rest.model.repositories.RepChangesetDiff
import com.google.gson.JsonObject
import at.bestsolution.bitbucket.rest.BitbucketSession

import static extension at.bestsolution.bitbucket.rest.impl.model.repositories.RepChangesetDiffImpl_FromFile.*
import static extension at.bestsolution.bitbucket.rest.impl.model.repositories.RepChangesetDiffImpl_Hunk.*
import static extension at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil.*

@Data
class RepChangesetDiffImpl extends BaseObject implements RepChangesetDiff {
	val FromFile fromFile
	val List<Hunk> hunks
	
	def static toRepChangesetDiff(JsonObject o, BitbucketSession session) {
		val _fromFile = o.jsonObject("from_file").toRepChangesetDiff_FromFile(session)
		val Iterable<JsonObject> hunksIt = o.iterable("hunks");
		val _hunks = hunksIt.map([e|e.toRepChangesetDiff_Hunk(session)]).toList
		val RepChangesetDiff rv = new RepChangesetDiffImpl(session,_fromFile,_hunks);
		return rv;
	}
}