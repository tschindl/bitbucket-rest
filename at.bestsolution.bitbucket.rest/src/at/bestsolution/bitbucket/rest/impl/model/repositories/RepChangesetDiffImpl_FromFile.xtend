package at.bestsolution.bitbucket.rest.impl.model.repositories

import at.bestsolution.bitbucket.rest.impl.model.base.BaseObject
import at.bestsolution.bitbucket.rest.model.repositories.RepChangesetDiff$FromFile
import at.bestsolution.bitbucket.rest.model.repositories.RepChangesetDiff$Meta
import at.bestsolution.bitbucket.rest.model.repositories.RepChangesetDiff$Page
import java.util.List
import com.google.gson.JsonObject
import at.bestsolution.bitbucket.rest.BitbucketSession

import static extension at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil.*
import static extension at.bestsolution.bitbucket.rest.impl.model.repositories.RepChangesetDiffImpl_Meta.*
import static extension at.bestsolution.bitbucket.rest.impl.model.repositories.RepChangesetDiffImpl_Page.*

@Data
class RepChangesetDiffImpl_FromFile extends BaseObject implements FromFile {
	val Meta meta
	val List<Page> pages
	val int blockNum
	
	def static toRepChangesetDiff_FromFile(JsonObject o, BitbucketSession session) {
		val _meta = o.jsonObject("meta").toRepChangesetDiff_Meta(session)
		val Iterable<JsonObject> pageIt = o.iterable("pages")
		val _pages = pageIt.map([e|e.toRepChangesetDiff_Page(session)]).toList
		val _blockNum = o.integer("block_num");
		val FromFile rv = new RepChangesetDiffImpl_FromFile(session,_meta,_pages,_blockNum);
		return rv;
	}
}