package at.bestsolution.bitbucket.rest.impl.model.repositories

import at.bestsolution.bitbucket.rest.impl.model.base.BaseObject
import at.bestsolution.bitbucket.rest.model.repositories.RepChangesetDiff$Hunk
import java.util.List
import com.google.gson.JsonObject
import at.bestsolution.bitbucket.rest.BitbucketSession
import static extension at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil.*

@Data
class RepChangesetDiffImpl_Hunk extends BaseObject implements Hunk {
	val List<String> toLines
	val int fromCount
	val int toStart
	val int fromStart
	val int toCount
	val String type
	val boolean conflict
	val List<String> fromLines
	
	def static toRepChangesetDiff_Hunk(JsonObject o, BitbucketSession session) {
		val Iterable<JsonObject> toLinesIt = o.iterable("to_lines")
		val _toLines = toLinesIt.map([e|e.asString]).toList
		val _fromCount = o.integer("from_count")
		val _toStart = o.integer("to_start")
		val _fromStart = o.integer("from_start")
		val _toCount = o.integer("to_count");
		val _type = o.string("type")
		val _conflict = o.bool("conflict")
		val Iterable<JsonObject> fromLinesIt = o.iterable("from_lines")
		val _fromLines = fromLinesIt.map([e|e.asString]).toList
		
		val Hunk rv = new RepChangesetDiffImpl_Hunk(session,_toLines,_fromCount,_toStart,_fromStart,_toCount,_type,_conflict,_fromLines)
		return rv; 
	}
}