package at.bestsolution.bitbucket.rest.impl.model.repositories

import at.bestsolution.bitbucket.rest.model.repositories.RepChangesetDiff$LongestLine
import at.bestsolution.bitbucket.rest.impl.model.base.BaseObject
import com.google.gson.JsonObject
import at.bestsolution.bitbucket.rest.BitbucketSession
import static extension at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil.*

@Data
class RepChangesetDiffImpl_LongestLine extends BaseObject implements LongestLine {
	val String html
	
	def static toRepChangesetDiff_LongestLine(JsonObject o, BitbucketSession session) {
		val _html = o.string("html")
		val LongestLine rv = new RepChangesetDiffImpl_LongestLine(session,_html);
		return rv;
	}
}