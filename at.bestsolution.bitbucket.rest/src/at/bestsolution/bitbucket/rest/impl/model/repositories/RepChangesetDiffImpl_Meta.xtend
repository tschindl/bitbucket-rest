package at.bestsolution.bitbucket.rest.impl.model.repositories

import at.bestsolution.bitbucket.rest.model.repositories.RepChangesetDiff$Meta
import at.bestsolution.bitbucket.rest.model.repositories.RepChangesetDiff$LongestLine
import at.bestsolution.bitbucket.rest.impl.model.base.BaseObject
import java.util.List
import com.google.gson.JsonObject
import at.bestsolution.bitbucket.rest.BitbucketSession
import static extension at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil.*
import static extension at.bestsolution.bitbucket.rest.impl.model.repositories.RepChangesetDiffImpl_LongestLine.*

@Data
class RepChangesetDiffImpl_Meta extends BaseObject implements Meta {
	val int numLines
	val List<Integer> position
	val LongestLine longestLine
	val String name
	val String filename
	
	def static toRepChangesetDiff_Meta(JsonObject o, BitbucketSession session) {
		val _numLines = o.integer("num_lines")
		val Iterable<JsonObject> posIt = o.iterable("position");  
		val _position = posIt.map([e|e.asInt]).toList
		val _longestLine = o.jsonObject("longest_line").toRepChangesetDiff_LongestLine(session)
		val _name = o.string("name")
		val _filename = o.string("filename");
		
		val Meta rv = new RepChangesetDiffImpl_Meta(session,_numLines,_position,_longestLine,_name,_filename)
		return rv;
	}
}