package at.bestsolution.bitbucket.rest.impl.model.repositories

import at.bestsolution.bitbucket.rest.model.repositories.RepChangesetDiff$Page
import at.bestsolution.bitbucket.rest.impl.model.base.BaseObject
import com.google.gson.JsonObject
import at.bestsolution.bitbucket.rest.BitbucketSession
import static extension at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil.*

@Data
class RepChangesetDiffImpl_Page extends BaseObject implements Page {
	val String html
	
	def static toRepChangesetDiff_Page(JsonObject o, BitbucketSession session) {
		val _html = o.string("html")
		val Page rv = new RepChangesetDiffImpl_Page(session,_html);
		return rv;
	}
}