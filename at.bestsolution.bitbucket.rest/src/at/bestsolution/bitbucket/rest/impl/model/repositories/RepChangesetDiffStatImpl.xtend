package at.bestsolution.bitbucket.rest.impl.model.repositories

import at.bestsolution.bitbucket.rest.impl.model.base.BaseObject
import at.bestsolution.bitbucket.rest.model.repositories.RepChangesetDiffStat
import at.bestsolution.bitbucket.rest.model.repositories.RepChangesetDiffStat$Diffstat
import com.google.gson.JsonObject
import at.bestsolution.bitbucket.rest.BitbucketSession

import static extension at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil.*
import static extension at.bestsolution.bitbucket.rest.impl.model.repositories.RepChangesetDiffStatImpl_Diffstat.*

@Data
class RepChangesetDiffStatImpl extends BaseObject implements RepChangesetDiffStat {
	val String type
	val String file
	val Diffstat diffstat
	
	def static toRepChangesetDiffStat(JsonObject o, BitbucketSession session) {
		val _type = o.string("type")
		val _file = o.string("file")
		val _diffstat = o.toRepChangesetDiffStat_Diffstat(session)
		
		val RepChangesetDiffStat rv = new RepChangesetDiffStatImpl(session,_type,_file,_diffstat)
		return rv;
	}
}