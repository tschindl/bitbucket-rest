package at.bestsolution.bitbucket.rest.impl.model.repositories

import at.bestsolution.bitbucket.rest.impl.model.base.BaseObject
import at.bestsolution.bitbucket.rest.model.repositories.RepChangesetDiffStat$Diffstat
import com.google.gson.JsonObject
import at.bestsolution.bitbucket.rest.BitbucketSession

import static extension at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil.*

@Data
class RepChangesetDiffStatImpl_Diffstat extends BaseObject implements Diffstat {
	val Integer removed
	val Integer added
	
	def static toRepChangesetDiffStat_Diffstat(JsonObject o, BitbucketSession session) {
		val _removed = o.integerObject("removed")
		val _added = o.integerObject("added");
		
		val Diffstat rv = new RepChangesetDiffStatImpl_Diffstat(session,_removed,_added);
		return rv;
	}
}