/*******************************************************************************
 * Copyright (c) 2013 BestSolution.at and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Tom Schindl<tom.schindl@bestsolution.at> - initial API and implementation
 *******************************************************************************/
package at.bestsolution.bitbucket.rest.impl.model.repositories

import at.bestsolution.bitbucket.rest.impl.model.base.BaseObject
import java.util.Date
import java.util.List
import com.google.gson.JsonObject
import at.bestsolution.bitbucket.rest.BitbucketSession
import static extension at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil.*
import at.bestsolution.bitbucket.rest.model.repositories.RepChangeset
import com.google.gson.JsonPrimitive

@Data
class RepChangesetImpl extends BaseObject implements RepChangeset {
	val String node
	val String rawAuthor
	val Date utctimestamp
	val String author
	val String rawNode
	val List<String> parents
	val String branch
	val String message
	val int revision
	val int size
	
	def static toRepChangeset(JsonObject o, BitbucketSession session) {
		val String _node = o.string("node")
		val String _rawAuthor = o.string("raw_author") 
		val Date _utctimestamp = o.date("utctimestamp")
		val String _author = o.string("author")
		val String _rawNode = o.string("raw_node")
		val Iterable<JsonPrimitive> parentsIt = o.iterable("parents")
		val _parents = parentsIt.map([e|e.asString]).toList.immutableCopy
		val String _branch = o.string("branch")
		val String _message = o.string("message")
		val int _revision = o.integer("revision")
		val int _size = o.integer("size")
		
		val RepChangeset rv = new RepChangesetImpl(session,_node,_rawAuthor,_utctimestamp,_author,_rawNode,_parents,_branch,_message,_revision,_size)
		return rv;
		
	}
}