package at.bestsolution.bitbucket.rest.impl.model.repositories

import at.bestsolution.bitbucket.rest.impl.model.base.BaseObject
import java.util.Date
import at.bestsolution.bitbucket.rest.model.user.UserUser
import com.google.gson.JsonObject
import at.bestsolution.bitbucket.rest.BitbucketSession
import static extension at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil.*
import static extension at.bestsolution.bitbucket.rest.impl.model.user.UserUserImpl.*
import at.bestsolution.bitbucket.rest.model.repositories.RepChangesetLike

@Data
class RepChangesetLikeImpl extends BaseObject implements RepChangesetLike {
	val Date utcLikedOn;
	val UserUser user;
	
	def static toRepChangesetLike(JsonObject o, BitbucketSession session) {
		val _utcLikedOn = o.date("utc_liked_on")
		val _user = o.toUserUser(session);
		
		val RepChangesetLike rv = new RepChangesetLikeImpl(session,_utcLikedOn,_user);
		return rv;
	}
}