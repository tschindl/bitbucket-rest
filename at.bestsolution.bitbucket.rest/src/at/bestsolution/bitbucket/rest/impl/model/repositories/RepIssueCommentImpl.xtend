package at.bestsolution.bitbucket.rest.impl.model.repositories

import at.bestsolution.bitbucket.rest.impl.model.base.BaseObject
import at.bestsolution.bitbucket.rest.model.user.UserUser
import java.util.Date
import at.bestsolution.bitbucket.rest.model.repositories.RepIssueComment
import com.google.gson.JsonObject
import at.bestsolution.bitbucket.rest.BitbucketSession

import static extension at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil.*
import static extension at.bestsolution.bitbucket.rest.impl.model.user.UserUserImpl.*

@Data
class RepIssueCommentImpl extends BaseObject implements RepIssueComment {
	val String content
	val UserUser authorInfo
	val int commentId
	val Date utcUpdatedOn
	val Date utcCreatedOn
	val boolean spam
	
	def static toRepIssueComment(JsonObject o, BitbucketSession session) {
		val _content = o.string("content")
		val _authorId = o.jsonObject("author_info").toUserUser(session)
		val _commentId = o.integer("comment_id")
		val _utcUpdatedOn = o.date("utc_updated_on")
		val _utcCreatedOn = o.date("utc_created_on")
		val _spam = o.bool("is_spam");
		val RepIssueComment rv = new RepIssueCommentImpl(session,_content,_authorId,_commentId,_utcUpdatedOn,_utcCreatedOn,_spam);
		return rv;
	}
}