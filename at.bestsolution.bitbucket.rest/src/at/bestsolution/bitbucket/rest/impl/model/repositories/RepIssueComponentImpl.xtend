package at.bestsolution.bitbucket.rest.impl.model.repositories

import at.bestsolution.bitbucket.rest.impl.model.base.BaseObject
import at.bestsolution.bitbucket.rest.model.repositories.RepIssueComponent

import static extension at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil.*
import com.google.gson.JsonObject
import at.bestsolution.bitbucket.rest.BitbucketSession

@Data
class RepIssueComponentImpl extends BaseObject implements RepIssueComponent {
	val int objectId
	val String name
	
	def static toRepIssueComponent(JsonObject o, BitbucketSession session) {
		val _objectId = o.integer("object_id")
		val _name = o.string("name")
		val RepIssueComponent rv = new RepIssueComponentImpl(session,_objectId,_name);
		return rv;
	}
}