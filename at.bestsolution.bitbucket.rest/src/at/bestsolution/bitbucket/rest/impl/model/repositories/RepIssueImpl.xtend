package at.bestsolution.bitbucket.rest.impl.model.repositories

import at.bestsolution.bitbucket.rest.impl.model.base.BaseObject
import at.bestsolution.bitbucket.rest.model.repositories.RepIssue
import at.bestsolution.bitbucket.rest.model.repositories.RepIssue$State
import at.bestsolution.bitbucket.rest.model.repositories.RepIssue$Priority
import at.bestsolution.bitbucket.rest.model.repositories.RepIssue$Metadata
import java.util.Date
import com.google.gson.JsonObject
import at.bestsolution.bitbucket.rest.BitbucketSession

import static extension at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil.*
import static extension at.bestsolution.bitbucket.rest.impl.model.repositories.RepIssueImpl_Metadata.*
import static extension at.bestsolution.bitbucket.rest.impl.model.user.UserUserImpl.*
import at.bestsolution.bitbucket.rest.model.user.UserUser

@Data
class RepIssueImpl extends BaseObject implements RepIssue {
	val State status
	val Priority priority
	val String title
	val UserUser reportedBy
	val Date utcLastUpdated
	val int commentCount
	val Metadata metadata
	val String content
	val int localId
	val int followerCount
	val Date utcCreatedOn
	val String resourceUri
	val boolean spam
	
	def static toRepIssue(JsonObject o, BitbucketSession session) {
		val _status = o.enumeration("status",typeof(State))
		val _priority = o.enumeration("priority",typeof(Priority))
		val _title = o.string("title")
		val _reportedBy = o.jsonObject("reported_by").toUserUser(session)
		val _utcLastUpdated = o.date("utc_last_updated")
		val _commentCount = o.integer("comment_count")
		val _metadata = o.jsonObject("metadata").toRepIssue_Metadata(session)
		val _content = o.string("content")
		val _localId = o.integer("local_id")
		val _followerCount = o.integer("follower_count")
		val _utcCreatedOn = o.date("utc_created_on")
		val _resourceUri = o.string("resource_uri")
		val _spam = o.bool("is_spam");
		
		val RepIssue rv = new RepIssueImpl(
			session, 
			_status, 
			_priority, 
			_title, 
			_reportedBy,
			_utcLastUpdated,
			_commentCount,
			_metadata,
			_content,
			_localId,
			_followerCount,
			_utcCreatedOn,
			_resourceUri,
			_spam
		);
		return rv;
	}
}