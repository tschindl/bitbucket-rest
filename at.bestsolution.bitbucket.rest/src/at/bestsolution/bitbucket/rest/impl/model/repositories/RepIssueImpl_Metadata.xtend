package at.bestsolution.bitbucket.rest.impl.model.repositories

import at.bestsolution.bitbucket.rest.impl.model.base.BaseObject
import at.bestsolution.bitbucket.rest.model.repositories.RepIssue$Metadata
import at.bestsolution.bitbucket.rest.model.repositories.RepIssue$Kind
import com.google.gson.JsonObject
import at.bestsolution.bitbucket.rest.BitbucketSession

import static extension at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil.*

@Data
class RepIssueImpl_Metadata extends BaseObject implements Metadata {
	val Kind kind
	val String version
	val String component
	val String milestone
	
	def static toRepIssue_Metadata(JsonObject o, BitbucketSession session) {
		val _kind = o.enumeration("kind",typeof(Kind))
		val _version = o.string("version")
		val _component = o.string("component")
		val _milestone = o.string("milestone")
		
		val Metadata rv = new RepIssueImpl_Metadata(session,_kind,_version,_component,_milestone);
		return rv;
	}
}