package at.bestsolution.bitbucket.rest.impl.model.repositories

import static extension at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil.*
import com.google.gson.JsonObject
import at.bestsolution.bitbucket.rest.BitbucketSession
import at.bestsolution.bitbucket.rest.impl.model.base.BaseObject
import at.bestsolution.bitbucket.rest.model.repositories.RepIssueVersion

@Data
class RepIssueVersionImpl extends BaseObject implements RepIssueVersion {
	val int objectId
	val String name
	
	def static toRepIssueVersion(JsonObject o, BitbucketSession session) {
		val _objectId = o.integer("object_id")
		val _name = o.string("name")
		val RepIssueVersion rv = new RepIssueVersionImpl(session,_objectId,_name);
		return rv;
	}
}