package at.bestsolution.bitbucket.rest.impl.model.repositories

import java.util.Date
import at.bestsolution.bitbucket.rest.model.user.UserUser
import at.bestsolution.bitbucket.rest.impl.model.base.BaseObject
import at.bestsolution.bitbucket.rest.model.repositories.RepPullRequestComment
import com.google.gson.JsonObject
import at.bestsolution.bitbucket.rest.BitbucketSession

import static extension at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil.*
import static extension at.bestsolution.bitbucket.rest.impl.model.user.UserUserImpl.*

@Data
class RepPullRequestCommentImpl extends BaseObject implements RepPullRequestComment {
	val int pullRequestId
	val int commentId
	val Integer parentId	
	val Date utcLastUpdated
	val String filenameHash
	val String baseRev	
	val String filename
	val String content
	val String contentRendered	
	val UserUser authorInfo
	val Integer lineFrom
	val Integer lineTo	
	val String destRev	
	val Date utcCreatedOn
	val String anchor	
	val boolean spam
	
	def static toRepPullRequestComment(JsonObject o, BitbucketSession session) {
		val _pullRequestId = o.integer("pull_request_id")
		val _commentId = o.integer("comment_id")
		val _parentId = o.integerObject("parent_id")
		val _utcLastUpdated = o.date("utc_last_updated")
		val _filenameHash = o.string("filename_hash")
		val _baseRev = o.string("base_rev")
		val _filename = o.string("filename")
		val _content = o.string("content")
		val _contentRendered = o.string("content_rendered")
		val _authorInfo = o.jsonObject("author_info").toUserUser(session)
		val _lineFrom = o.integerObject("line_from")
		val _lineTo	= o.integerObject("line_to")
		val _destRev = o.string("dest_rev")
		val _utcCreatedOn = o.date("utc_created_on")
		val _anchor = o.string("anchor")
		val _spam = o.bool("spam")
		val RepPullRequestComment rv = new RepPullRequestCommentImpl(session,_pullRequestId,_commentId,_parentId,_utcLastUpdated,_filenameHash,_baseRev,_filename,_content,_contentRendered,_authorInfo,_lineFrom,_lineTo,_destRev,_utcCreatedOn,_anchor,_spam)
		return rv;
	}
}