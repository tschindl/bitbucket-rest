package at.bestsolution.bitbucket.rest.impl.model.repositories

import at.bestsolution.bitbucket.rest.impl.model.base.BaseObject
import java.util.Date
import at.bestsolution.bitbucket.rest.model.user.UserUser
import com.google.gson.JsonObject
import at.bestsolution.bitbucket.rest.BitbucketSession
import static extension at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil.*
import static extension at.bestsolution.bitbucket.rest.impl.model.user.UserUserImpl.*
import at.bestsolution.bitbucket.rest.model.repositories.RepPullRequestLike

@Data
class RepPullRequestLikeImpl extends BaseObject implements RepPullRequestLike {
	val Date utcLikedOn;
	val UserUser user;
	
	def static toRepPullRequestLike(JsonObject o, BitbucketSession session) {
		val _utcLikedOn = o.date("utc_liked_on")
		val _user = o.toUserUser(session);
		
		val RepPullRequestLike rv = new RepPullRequestLikeImpl(session,_utcLikedOn,_user);
		return rv;
	}
}