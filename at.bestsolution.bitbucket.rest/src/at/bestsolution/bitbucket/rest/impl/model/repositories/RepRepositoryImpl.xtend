/*******************************************************************************
 * Copyright (c) 2013 BestSolution.at and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Tom Schindl<tom.schindl@bestsolution.at> - initial API and implementation
 *******************************************************************************/
package at.bestsolution.bitbucket.rest.impl.model.repositories

import at.bestsolution.bitbucket.rest.impl.model.base.BasicRepositoryImpl
import at.bestsolution.bitbucket.rest.model.repositories.RepRepository
import java.util.Date
import com.google.gson.JsonObject
import at.bestsolution.bitbucket.rest.BitbucketSession
import static extension at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil.*

@Data
class RepRepositoryImpl extends BasicRepositoryImpl implements RepRepository {
	val String scm;
	val boolean wiki;
	val String creator;
	val String logo;
	val String emailMailinglist;
	val boolean mq;
	val int size;
	val boolean readOnly;
	val String forkOf; 
	val String mqOf;
	val int followersCount;
	val Date utcCreatedOn;
	val String website;
	val String description;
	val boolean issues;
	val boolean fork;
	val String language;
	val Date utcLastUpdated;
	val boolean emailWriters;
	val boolean noPublicForks;
	val String resourceURI;
	val int forksCount
	val String state;
	
	override hasIssues() {
		return issues;
	}
	
	override hasWiki() {
		return wiki;
	}
	
	def static toRepRepository(JsonObject o, BitbucketSession session) {
		val _name = o.string("name")
		val _owner = o.string("owner")
		val _slug = o.string("slug");
		val _private = o.bool("is_private")
		val _scm = o.string("scm");
		val _wiki = o.bool("has_wiki");
		val _creator = o.string("creator")
		val _logo = o.string("logo")
		val _emailMailinglist = o.string("email_mailinglist")
		val _mq = o.bool("is_mq")
		val _size = o.integer("size")
		val _readOnly = o.bool("read_only")
		val _forkOf = o.string("fork_of")
		val _mqOf = o.string("mq_of")
		val _followersCount = o.integer("followers_count")
		val _utcCreatedOn = o.date("utc_created_on")
		val _website = o.string("website")
		val _description = o.string("description")
		val _issues = o.bool("has_issues")
		val _fork = o.bool("is_fork")
		val _language = o.string("language")
		val _utcLastUpdated = o.date("utc_last_update")
		val _emailWriters = o.bool("email_writers")
		val _noPublicForks = o.bool("no_public_forks")
		val _resourceURI = o.string("resoure_uri")
		val _forksCount = o.integer("forks_count");
		val _state = o.string("state");
		
		val RepRepository rv = new RepRepositoryImpl(
			session,
			_name,
			_owner,
			_slug,
			_private,
			_scm,
			_wiki,
			_creator,
			_logo,
			_emailMailinglist,
			_mq,
			_size,
			_readOnly,
			_forkOf,
			_mqOf,
			_followersCount,
			_utcCreatedOn,
			_website,
			_description,
			_issues,
			_fork,
			_language,
			_utcLastUpdated,
			_emailWriters,
			_noPublicForks,
			_resourceURI,
			_forksCount,
			_state
		)
		return rv
	}
}