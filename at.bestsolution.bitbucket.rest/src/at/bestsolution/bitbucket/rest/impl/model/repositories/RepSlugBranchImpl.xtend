package at.bestsolution.bitbucket.rest.impl.model.repositories

import static extension at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil.*
import static extension at.bestsolution.bitbucket.rest.impl.model.repositories.RepSlugBranchImpl_File.*
import java.util.List
import at.bestsolution.bitbucket.rest.model.repositories.RepSlugBranch$File
import java.util.Date
import com.google.gson.JsonObject
import at.bestsolution.bitbucket.rest.BitbucketSession
import at.bestsolution.bitbucket.rest.impl.model.base.BaseObject
import at.bestsolution.bitbucket.rest.model.repositories.RepSlugBranch

@Data
class RepSlugBranchImpl extends BaseObject implements RepSlugBranch {
	val String node
	val List<File> files
	val String rawAuthor
	val Date utcTimestamp
	val String author
	val String rawNode
	val List<String> parents
	val String branch
	val String message
	val int revision
	val int size
	
	def static toRepSlugBranch(JsonObject o, BitbucketSession session) {
		val _node = o.string("node")
		val _files = (o.iterable("files") as Iterable<JsonObject>).map([e|e.toRepSlugBranch_File(session)]).toList 
		val _rawAuthor = o.string("raw_author")
		val _utcTimestamp = o.date("utctimestampe")
		val _author = o.string("author")
		val _rawNode = o.string("raw_node")
		val _parents = (o.iterable("parents") as Iterable<JsonObject>).map([e|e.asString]).toList
		val _branch = o.string("branch")
		val _message = o.string("message")
		val _revision = o.integer("revision")
		val _size = o.integer("size")
		val RepSlugBranch rv = new RepSlugBranchImpl(session,_node,_files,_rawAuthor,_utcTimestamp,_author,_rawNode,_parents,_branch,_message,_revision,_size);
		return rv;
	}
}