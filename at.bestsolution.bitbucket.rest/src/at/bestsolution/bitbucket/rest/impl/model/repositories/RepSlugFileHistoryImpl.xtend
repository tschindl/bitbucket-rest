package at.bestsolution.bitbucket.rest.impl.model.repositories

import java.util.Date
import java.util.List

import at.bestsolution.bitbucket.rest.impl.model.base.BaseObject
import at.bestsolution.bitbucket.rest.model.repositories.RepSlugFileHistory$File
import at.bestsolution.bitbucket.rest.model.user.UserUser
import com.google.gson.JsonObject
import at.bestsolution.bitbucket.rest.BitbucketSession

import static extension at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil.*
import static extension at.bestsolution.bitbucket.rest.impl.model.repositories.RepSlugFileHistoryImpl_File.*
import static extension at.bestsolution.bitbucket.rest.impl.model.user.UserUserImpl.*
import at.bestsolution.bitbucket.rest.model.repositories.RepSlugFileHistory


@Data
class RepSlugFileHistoryImpl extends BaseObject implements RepSlugFileHistory {
	val String node
	val List<File> files
	val String rawAuthor
	val Date utcTimestamp
	val UserUser author
	val String rawNode
	val List<String> parents
	val String branch
	val String message
	val Integer revision
	val int size
	
	def static toRepSlugFileHistory(JsonObject o, BitbucketSession session) {
		val _node = o.string("node")
		val _files = o.list("files",[e|e.toRepSlugFileHistory_File(session)])
		val _rawAuthor = o.string("raw_author")
		val _utcTimestamp = o.date("utctimestamp")
		val _author = o.jsonObject("author").toUserUser(session)
		val _rawNode = o.string("raw_node")
		val _parents = o.list("parents",[e|e.asString])
		val _branch = o.string("branch")
		val _message = o.string("message")
		val _revision = o.integerObject("revision")
		val int _size = o.integer("size")
		val RepSlugFileHistory rv = new RepSlugFileHistoryImpl(session,_node,_files,_rawAuthor,_utcTimestamp,_author,_rawNode,_parents,_branch,_message,_revision,_size)
		return rv;
	}
}