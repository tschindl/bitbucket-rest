package at.bestsolution.bitbucket.rest.impl.model.repositories

import at.bestsolution.bitbucket.rest.impl.model.base.BaseObject
import com.google.gson.JsonObject
import at.bestsolution.bitbucket.rest.BitbucketSession
import static extension at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil.*
import at.bestsolution.bitbucket.rest.model.repositories.RepSlugFileHistory$File

@Data
class RepSlugFileHistoryImpl_File extends BaseObject implements File {
	val String type
	val String file
	
	def static toRepSlugFileHistory_File(JsonObject o, BitbucketSession session) {
		val _type = o.string("type")
		val _file = o.string("file")
		val File rv = new RepSlugFileHistoryImpl_File(session,_type,_file);
		return rv;
	}
}