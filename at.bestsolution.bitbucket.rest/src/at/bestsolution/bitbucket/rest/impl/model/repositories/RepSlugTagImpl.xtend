package at.bestsolution.bitbucket.rest.impl.model.repositories

import java.util.List
import at.bestsolution.bitbucket.rest.model.repositories.RepSlugTag$File
import java.util.Date
import at.bestsolution.bitbucket.rest.impl.model.base.BaseObject
import at.bestsolution.bitbucket.rest.model.repositories.RepSlugTag
import com.google.gson.JsonObject
import at.bestsolution.bitbucket.rest.BitbucketSession

import static extension at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil.*
import static extension at.bestsolution.bitbucket.rest.impl.model.repositories.RepSlugTagImpl_File.*

@Data
class RepSlugTagImpl extends BaseObject implements RepSlugTag {
	val String node
	val List<File> files
	val String rawAuthor
	val Date utcTimestamp
	val String author
	val String rawNode
	val List<String> parents
	val String branch
	val String message
	val int revision
	val int size
	
	def static toRepSlugTag(JsonObject o, BitbucketSession session) {
		val _node = o.string("node")
		val _files = o.list("files",[e|e.toRepSlugTag_File(session)])
		val _rawAuthor = o.string("raw_author")
		val _utcTimestamp = o.date("utctimestamp")
		val _author = o.string("author")
		val _rawNode = o.string("raw_node")
		val _parents = o.list("parents",[e|e.asString])
		val _branch = o.string("branch")
		val _message = o.string("message")
		val _revision = o.integer("revision")
		val _size = o.integer("size")
		val RepSlugTag rv = new RepSlugTagImpl(session,_node,_files,_rawAuthor,_utcTimestamp,_author,_rawNode,_parents,_branch,_message,_revision,_size)
		return rv;

	}
}