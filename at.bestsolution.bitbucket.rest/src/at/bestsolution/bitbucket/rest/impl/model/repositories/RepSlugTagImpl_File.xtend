package at.bestsolution.bitbucket.rest.impl.model.repositories

import at.bestsolution.bitbucket.rest.impl.model.base.BaseObject

import static extension at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil.*
import com.google.gson.JsonObject
import at.bestsolution.bitbucket.rest.BitbucketSession
import at.bestsolution.bitbucket.rest.model.repositories.RepSlugTag$File

@Data
class RepSlugTagImpl_File extends BaseObject implements File {
	val String type
	val String file
	
	def static toRepSlugTag_File(JsonObject o, BitbucketSession session) {
		val _type = o.string("type")
		val _file = o.string("file")
		val File rv = new RepSlugTagImpl_File(session,_type,_file);
		return rv;
	}
}