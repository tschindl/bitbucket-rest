/*******************************************************************************
 * Copyright (c) 2013 BestSolution.at and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Tom Schindl<tom.schindl@bestsolution.at> - initial API and implementation
 *******************************************************************************/
package at.bestsolution.bitbucket.rest.impl.model.user

import at.bestsolution.bitbucket.rest.impl.model.base.BasicRepositoryImpl
import at.bestsolution.bitbucket.rest.model.user.UserRepository
import com.google.gson.JsonObject
import at.bestsolution.bitbucket.rest.BitbucketSession
import static extension at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil.*

@Data
class UserRepositoryImpl extends BasicRepositoryImpl implements UserRepository {
	val String scm;
	
	def static toUserRepository(JsonObject o, BitbucketSession session) {
		val _name = o.string("name")
		val _owner = o.string("owner")
		val _slug = o.string("slug");
		val _private = o.bool("is_private")
		val _scm = o.string("name")
		
		val UserRepository r = new UserRepositoryImpl(
			session,
			_name,
			_owner,
			_slug,
			_private,
			_scm
		);
		
		return r;
	}
}