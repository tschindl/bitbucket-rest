/*******************************************************************************
 * Copyright (c) 2013 BestSolution.at and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Tom Schindl<tom.schindl@bestsolution.at> - initial API and implementation
 *******************************************************************************/
package at.bestsolution.bitbucket.rest.impl.model.user

import at.bestsolution.bitbucket.rest.impl.model.base.BaseObject
import at.bestsolution.bitbucket.rest.model.user.UserOverviewRepository
import at.bestsolution.bitbucket.rest.model.user.UserRepositoryOverview
import at.bestsolution.bitbucket.rest.BitbucketSession
import com.google.gson.JsonObject
import java.util.List

import static extension at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil.*
import static extension at.bestsolution.bitbucket.rest.impl.model.user.UserOverviewRepositoryImpl.*

@Data
class UserRepositoryOverviewImpl extends BaseObject implements UserRepositoryOverview {
	var List<UserOverviewRepository> updated;
	var List<UserOverviewRepository> viewed;
	
	def static toUserRepositoryOverview(JsonObject o, BitbucketSession session) {
		val Iterable<JsonObject> _updated = o.iterable("updated")
		val Iterable<JsonObject> _viewed = o.iterable("viewed")
		
		val updatedList = _updated.map([e|e.toUserOverviewRepository(session)]).toList.immutableCopy
		val viewedList = _viewed.map([e|e.toUserOverviewRepository(session)]).toList.immutableCopy
		
		val UserRepositoryOverview rv = new UserRepositoryOverviewImpl(session, updatedList, viewedList);
		return rv;
	}
}