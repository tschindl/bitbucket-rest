package at.bestsolution.bitbucket.rest.impl.model.user

import at.bestsolution.bitbucket.rest.impl.model.base.BaseObject
import at.bestsolution.bitbucket.rest.model.user.UserUser
import com.google.gson.JsonObject
import at.bestsolution.bitbucket.rest.BitbucketSession

import static extension at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil.*

@Data
class UserUserImpl extends BaseObject implements UserUser {
	val String username
	val String firstName
	val String lastName
	val String avatar
	val String resourceUri
	val boolean team
	
	def static toUserUser(JsonObject o, BitbucketSession session) {
		val _username = o.string("username");
		val _firstName = o.string("first_name")
		val _lastName = o.string("last_name");
		val _avatar = o.string("avatar")
		val _resourceUri = o.string("resource_uri")
		val _team = o.bool("is_team")
		
		val UserUser rv = new UserUserImpl(
			session,
			_username,
			_firstName,
			_lastName,
			_avatar,
			_resourceUri,
			_team
		);
		return rv;
	}
}