/*******************************************************************************
 * Copyright (c) 2013 BestSolution.at and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Tom Schindl<tom.schindl@bestsolution.at> - initial API and implementation
 *******************************************************************************/
package at.bestsolution.bitbucket.rest.impl.services;

import at.bestsolution.bitbucket.rest.BitbucketSession;
import at.bestsolution.bitbucket.rest.impl.BitbucketServiceImpl;
import at.bestsolution.bitbucket.rest.impl.BitbucketServiceImpl.ServiceEndpoint;
import at.bestsolution.bitbucket.rest.util.ResponseObject;

public class BasicServiceImpl {
	private final BitbucketSession session;
	
	public BasicServiceImpl(BitbucketSession session) {
		this.session = session;
	}
	
	protected BitbucketSession getSession() {
		return session;
	}
	
	protected ServiceEndpoint createEndpoint(String url) {
		return BitbucketServiceImpl.createEndpoint(session, session.getServiceUrl() + "/" + session.getServiceVersion() + "/" + url);
	}
	
	protected static <O> ResponseObject<O> createErrorObject(final Throwable t) {
		return new ResponseObject<O>() {

			@Override
			public O value() {
				return null;
			}

			@Override
			public boolean isOk() {
				return false;
			}

			@Override
			public Throwable getException() {
				return t;
			}
		};
	}
	
	protected static <O> ResponseObject<O> createResponseObject(final O o) {
		return new ResponseObject<O>() {

			@Override
			public O value() {
				return o;
			}

			@Override
			public boolean isOk() {
				return true;
			}

			@Override
			public Throwable getException() {
				return null;
			}
		};
	}
}
