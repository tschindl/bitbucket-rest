/*******************************************************************************
 * Copyright (c) 2013 BestSolution.at and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Tom Schindl<tom.schindl@bestsolution.at> - initial API and implementation
 *******************************************************************************/
package at.bestsolution.bitbucket.rest.impl.services

import at.bestsolution.bitbucket.rest.BitbucketRepositoriesService
import at.bestsolution.bitbucket.rest.BitbucketSession
import at.bestsolution.bitbucket.rest.impl.BitbucketServiceImpl$ServiceResponse
import at.bestsolution.bitbucket.rest.impl.services.util.BasicSegementedList
import at.bestsolution.bitbucket.rest.model.repositories.RepChangeset
import at.bestsolution.bitbucket.rest.util.ProgressMonitor
import at.bestsolution.bitbucket.rest.util.ResponseObject

import com.google.gson.JsonObject
import java.util.ArrayList
import java.util.HashMap
import java.util.List

import static at.bestsolution.bitbucket.rest.impl.services.BasicServiceImpl.*

import static extension at.bestsolution.bitbucket.rest.impl.model.repositories.RepChangesetImpl.*
import static extension at.bestsolution.bitbucket.rest.impl.model.repositories.RepRepositoryImpl.*
import static extension at.bestsolution.bitbucket.rest.impl.model.repositories.RepIssueImpl.*
import static extension at.bestsolution.bitbucket.rest.impl.model.repositories.RepIssueCommentImpl.*
import static extension at.bestsolution.bitbucket.rest.impl.model.repositories.RepIssueComponentImpl.*
import static extension at.bestsolution.bitbucket.rest.impl.model.repositories.RepIssueMilestoneImpl.*
import static extension at.bestsolution.bitbucket.rest.impl.model.repositories.RepIssueVersionImpl.*
import static extension at.bestsolution.bitbucket.rest.impl.model.repositories.RepChangesetDiffStatImpl.*
import static extension at.bestsolution.bitbucket.rest.impl.model.repositories.RepChangesetLikeImpl.*
import static extension at.bestsolution.bitbucket.rest.impl.model.repositories.RepChangesetCommentImpl.*
import static extension at.bestsolution.bitbucket.rest.impl.model.repositories.RepChangesetDiffImpl.*
import static extension at.bestsolution.bitbucket.rest.impl.model.repositories.RepPullRequestCommentImpl.*
import static extension at.bestsolution.bitbucket.rest.impl.model.repositories.RepPullRequestLikeImpl.*
import static extension at.bestsolution.bitbucket.rest.impl.model.repositories.RepSlugBranchImpl.*
import static extension at.bestsolution.bitbucket.rest.impl.model.repositories.RepSlugTagImpl.*
import static extension at.bestsolution.bitbucket.rest.impl.model.repositories.RepSlugFileHistoryImpl.*
import static extension at.bestsolution.bitbucket.rest.impl.model.user.UserUserImpl.*
import static extension at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil.*
import at.bestsolution.bitbucket.rest.model.repositories.RepIssue
import at.bestsolution.bitbucket.rest.model.repositories.RepSlugBranch
import java.util.Map
import at.bestsolution.bitbucket.rest.model.repositories.RepSlugTag

class BitbucketRepositoriesServiceImpl extends BasicServiceImpl implements BitbucketRepositoriesService {
	val CHANGESET_MAX_PATCHSIZE = 50;
	val DEFAULT_SEGMENT_SIZE = 20;
	
	new(BitbucketSession session) {
		super(session)
		
	}

	/*
	 * =========================================================================================
	 * REPOSITORY APIS
	 * =========================================================================================
	 */

	override getRepository(ProgressMonitor monitor, String slug) {
		return getRepository(monitor,session.username,slug)
	}
	
	override getRepository(ProgressMonitor monitor, String accountname, String slug) {
		val ServiceResponse<JsonObject> r = createEndpoint("repositories/"+accountname+"/"+slug).get
		
		if( r.ok ) {
			return createResponseObject(r.content.toRepRepository(session))
		}
		
		return createErrorObject(r.exception);
	}
	
	/*
	 * =========================================================================================
	 * CHANGESET APIS
	 * =========================================================================================
	 */
	
	override getSegmentedChangesets(ProgressMonitor monitor, String accountname, String slug) {
		return getSegmentedChangesets(monitor,accountname,slug,null);
	}
	
	override getSegmentedChangesets(ProgressMonitor monitor, String accountname, String slug, Integer segmentSize) {
		var segSize = segmentSize
		
		if( segmentSize == null ) {
			segSize = DEFAULT_SEGMENT_SIZE;
		}
		
		val fSegSize = segSize;
		
		val endpoint = createEndpoint("repositories/"+accountname+"/"+slug+"/changesets")
		
		val p = new HashMap<String,String>();
		p.put("limit","1");
		
		val ServiceResponse<JsonObject> r = endpoint.get(p)
		
		if( r.ok ) {
			val itemCount = r.content.integer("count");
			if( itemCount == 0 ) {
				return createResponseObject(BasicSegementedList::emptyList);
			} else {
				return createResponseObject(BasicSegementedList::list(itemCount,segSize) [
					mon,l,idx,segsize |
					if( idx == 0 ) {
						val ResponseObject<List<RepChangeset>> rv = getChangesets(mon,accountname,slug,fSegSize,null);
						return rv.value;
					} else {
						val List<RepChangeset> tmp = l.getSegment(mon,idx-1);
						val ResponseObject<List<RepChangeset>> rv = getChangesets(mon,accountname,slug,fSegSize+1,tmp.get(0).node);
						// remove the first segment because it equals
						// the last element from the previous request
						rv.value.remove(0);
						return rv.value;
					}
				]);
			}
		}
		
		return createErrorObject(r.exception); 
	}
	
	override getChangesets(ProgressMonitor monitor, String accountname, String slug) {
		return getChangesets(monitor,accountname, slug, null, null); 
	}
	
	override getChangesets(ProgressMonitor monitor, String accountname, String slug, Integer limit, String start) {
		val p = new HashMap<String,String>();
		if( start != null ) {
			p.put("start",start);
		}
		
		val endpoint = createEndpoint("repositories/"+accountname+"/"+slug+"/changesets")
		
		if( limit != null && limit > CHANGESET_MAX_PATCHSIZE ) {
			p.put("limit",CHANGESET_MAX_PATCHSIZE+"");
			
			val List<RepChangeset> rv = new ArrayList<RepChangeset>
			var ServiceResponse<JsonObject> r = endpoint.get(p)
			var iLimit = limit.intValue
			
			while( true ) {
				if( r.ok ) {
					val Iterable<JsonObject> items = r.content.iterable("changesets")
					val tmp = items.map([e|e.toRepChangeset(session)]).toList.reverse
					
					val length = tmp.size;
					
					// remove the first segment because it equals
					// the last element from the previous request
					if( rv.size != 0 ) {
						tmp.remove(0)
					}
					
					rv.addAll(tmp)
					
					if( length < CHANGESET_MAX_PATCHSIZE ) {
						return createResponseObject(rv)
					}
					
					p.put("start",tmp.last.node);
					iLimit = iLimit - tmp.size;
					
					// Should not happen but make sure we never call it with
					// a smaller value than 0
					if( iLimit <= 0 ) {
						return createResponseObject(rv)
					}
					
					// on the last request we need to increment by 1 because the
					// first segment will be stripped
					if( iLimit < CHANGESET_MAX_PATCHSIZE ) {
						iLimit = iLimit + 1
					}
					
					p.put("limit",iLimit+"");
					
					r = endpoint.get(p)	
				} else {
					return createErrorObject(r.exception);
				}
				
			}
			return createResponseObject(rv)
		} else {
			if( limit != null ) {
				p.put("limit",limit.toString);	
			}
			
			val ServiceResponse<JsonObject> r = endpoint.get(p)
			
			if( r.ok ) {
				val Iterable<JsonObject> items = r.content.iterable("changesets")
				return createResponseObject(items.map([e|e.toRepChangeset(session)]).toList.reverse)
			}
			
			return createErrorObject(r.exception);
		}
	}
	
	override getChangeset(ProgressMonitor monitor, String accountname, String slug, String rawNode) {
		val endpoint = createEndpoint("repositories/"+accountname+"/"+slug+"/changesets/"+rawNode)
		var ServiceResponse<JsonObject> r = endpoint.get();
		
		if( r.ok ) {
			return createResponseObject(r.content.toRepChangeset(session))
		}
		
		return createErrorObject(r.exception);
	}
		
	override getChangesetDiffs(ProgressMonitor monitor, String accountname, String slug, String rawNode) {
		val endpoint = createEndpoint("repositories/"+accountname+"/"+slug+"/changesets/"+rawNode+"/diff")
		var ServiceResponse<JsonObject> r = endpoint.get();

		if( r.ok ) {
			var Iterable<JsonObject> diffs = r.content.asJsonArray.iterable;
			return createResponseObject(diffs.map([e|e.toRepChangesetDiff(session)]).toList);
		}
		
		return createErrorObject(r.exception);
	}
	
	override getChangesetLike(ProgressMonitor monitor, String accountname, String slug, String rawNode, String likeraccount) {
		val endpoint = createEndpoint("repositories/"+accountname+"/"+slug+"/changesets/"+rawNode+"/likes/" + likeraccount)
		var ServiceResponse<JsonObject> r = endpoint.get();
		
		if( r.ok ) {
			return createResponseObject(r.content.toRepChangesetLike(session));
		}
		
		return createErrorObject(r.exception);
	}
	
	override getChangesetLikes(ProgressMonitor monitor, String accountname, String slug, String rawNode) {
		val endpoint = createEndpoint("repositories/"+accountname+"/"+slug+"/changesets/"+rawNode+"/likes")
		var ServiceResponse<JsonObject> r = endpoint.get();
		
		if( r.ok ) {
			var Iterable<JsonObject> likes = r.content.asJsonArray.iterable
			return createResponseObject(likes.map([e|e.toRepChangesetLike(session)]).toList);
		}
		
		return createErrorObject(r.exception);
	}
	
	override getChangesetStats(ProgressMonitor monitor, String accountname, String slug, String rawNode) {
		val endpoint = createEndpoint("repositories/"+accountname+"/"+slug+"/changesets/"+rawNode+"/diffstat")
		var ServiceResponse<JsonObject> r = endpoint.get();
		
		if( r.ok ) {
			var Iterable<JsonObject> stats = r.content.asJsonArray.iterable
			return createResponseObject(stats.map([e|e.toRepChangesetDiffStat(session)]).toList);
		}
		
		return createErrorObject(r.exception);
	}
	
	override getChangesetComments(ProgressMonitor monitor, String accountname, String slug, String rawNode) {
		val endpoint = createEndpoint("repositories/"+accountname+"/"+slug+"/changesets/"+rawNode+"/comments")
		var ServiceResponse<JsonObject> r = endpoint.get();
		
		if( r.ok ) {
			var Iterable<JsonObject> comments = r.content.asJsonArray.iterable
			return createResponseObject(comments.map([e|e.toRepChangesetComment(session)]).toList);
		}
		
		return createErrorObject(r.exception);
	}
	
	
	/*
	 * =========================================================================================
	 * ISSUE APIS
	 * =========================================================================================
	 */
	
	override getSegmentedIssues(ProgressMonitor monitor, String accountname, String slug) {
		return getSegmentedIssues(monitor,accountname,slug,null,null);
	}
	
	override getSegmentedIssues(ProgressMonitor monitor, String accountname, String slug, Integer segmentSize, Integer start) {
		var segSize = segmentSize
		
		if( segmentSize == null ) {
			segSize = DEFAULT_SEGMENT_SIZE;
		}
		
		val fSegSize = segSize;
		
		val endpoint = createEndpoint("repositories/"+accountname+"/"+slug+"/issues")
		
		val p = new HashMap<String,String>();
		p.put("limit","1");
		
		val ServiceResponse<JsonObject> r = endpoint.get(p)
		
		if( r.ok ) {
			val itemCount = r.content.integer("count");
			if( itemCount == 0 ) {
				return createResponseObject(BasicSegementedList::emptyList);
			} else {
				return createResponseObject(BasicSegementedList::list(itemCount,segSize) [
					mon,l,idx,segsize |
					if( idx == 0 ) {
						val ResponseObject<List<RepIssue>> rv = getIssues(mon,accountname,slug,fSegSize,null);
						return rv.value;
					} else {
						val ResponseObject<List<RepIssue>> rv = getIssues(mon,accountname,slug,fSegSize,idx*fSegSize);
						return rv.value;
					}
				]);
			}
		}
		
		return createErrorObject(r.exception); 
	}
	
	override getIssues(ProgressMonitor monitor, String accountname, String slug) {
		return getIssues(monitor,accountname,slug,null,null)
	}
	
	override getIssues(ProgressMonitor monitor, String accountname, String slug, Integer limit, Integer start) {
		val p = new HashMap<String,String>();
		if( start != null ) {
			p.put("start",start.toString);
		}
		
		val endpoint = createEndpoint("repositories/"+accountname+"/"+slug+"/issues")
		
		if( limit != null && limit > CHANGESET_MAX_PATCHSIZE ) {
			p.put("limit",CHANGESET_MAX_PATCHSIZE+"");
			
			val List<RepIssue> rv = new ArrayList<RepIssue>
			var ServiceResponse<JsonObject> r = endpoint.get(p)
			var iLimit = limit.intValue
			
			while( true ) {
				if( r.ok ) {
					val Iterable<JsonObject> items = r.content.iterable("issues")
					val tmp = items.map([e|e.toRepIssue(session)]).toList
					
					val length = tmp.size;
					
					rv.addAll(tmp)
					
					if( length < CHANGESET_MAX_PATCHSIZE ) {
						return createResponseObject(rv)
					}
					
					p.put("start",rv.size.toString);
					iLimit = iLimit - tmp.size;
					
					// Should not happen but make sure we never call it with
					// a smaller value than 0
					if( iLimit <= 0 ) {
						return createResponseObject(rv)
					}
					
					p.put("limit",iLimit+"");
					
					r = endpoint.get(p)	
				} else {
					return createErrorObject(r.exception);
				}
				
			}
			return createResponseObject(rv)
		} else {
			if( limit != null ) {
				p.put("limit",limit.toString);	
			}
			
			val ServiceResponse<JsonObject> r = endpoint.get(p)
			
			if( r.ok ) {
				val Iterable<JsonObject> items = r.content.iterable("issues")
				return createResponseObject(items.map([e|e.toRepIssue(session)]).toList)
			}
			
			return createErrorObject(r.exception);
		}
	}
	
	override getIssue(ProgressMonitor monitor, String accountname, String slug, int issueId) {
		val endpoint = createEndpoint("repositories/"+accountname+"/"+slug+"/issues/"+issueId)
		val ServiceResponse<JsonObject> r = endpoint.get
		
		if( r.ok ) {
			return createResponseObject(r.content.toRepIssue(session))
		}
		
		return createErrorObject(r.exception);
	}
	
	override getIssueComment(ProgressMonitor monitor, String accountname, String slug, int issueId, int commentId) {
		val endpoint = createEndpoint("repositories/"+accountname+"/"+slug+"/issues/"+issueId+"/comments/"+commentId)
		val ServiceResponse<JsonObject> r = endpoint.get
		
		if( r.ok ) {
			return createResponseObject(r.content.toRepIssueComment(session))
		}
		
		return createErrorObject(r.exception);
	}
	
	override getIssueComments(ProgressMonitor monitor, String accountname, String slug, int issueId) {
		val endpoint = createEndpoint("repositories/"+accountname+"/"+slug+"/issues/"+issueId+"/comments")
		val ServiceResponse<JsonObject> r = endpoint.get
		
		if( r.ok ) {
			val Iterable<JsonObject> comments = r.content.asJsonArray.iterable();
			return createResponseObject(comments.map([e|e.toRepIssueComment(session)]).toList)
		}
		
		return createErrorObject(r.exception);
	}
	
	override getIssueComponent(ProgressMonitor monitor, String accountname, String slug, int id) {
		val endpoint = createEndpoint("repositories/"+accountname+"/"+slug+"/issues/components/"+id)
		val ServiceResponse<JsonObject> r = endpoint.get
		
		if( r.ok ) {
			return createResponseObject(r.content.toRepIssueComponent(session))
		}
		
		return createErrorObject(r.exception);
	}
	
	override getIssueComponents(ProgressMonitor monitor, String accountname, String slug) {
		val endpoint = createEndpoint("repositories/"+accountname+"/"+slug+"/issues/components")
		val ServiceResponse<JsonObject> r = endpoint.get
		
		if( r.ok ) {
			val Iterable<JsonObject> components = r.content.asJsonArray.iterable();
			return createResponseObject(components.map([e|e.toRepIssueComponent(session)]).toList)
		}
		
		return createErrorObject(r.exception);
	}
	
	override getIssueFollowers(ProgressMonitor monitor, String accountname, String slug, int issueId) {
		val endpoint = createEndpoint("repositories/"+accountname+"/"+slug+"/issues/"+issueId+"/follower")
		val ServiceResponse<JsonObject> r = endpoint.get
		
		if( r.ok ) {
			val Iterable<JsonObject> followers = r.content.iterable("followers");
			return createResponseObject(followers.map([e|e.toUserUser(session)]).toList)
		}
		
		return createErrorObject(r.exception);
	}
	
	override getIssueMilestone(ProgressMonitor monitor, String accountname, String slug, int id) {
		val endpoint = createEndpoint("repositories/"+accountname+"/"+slug+"/issues/milestones/"+id)
		val ServiceResponse<JsonObject> r = endpoint.get
		
		if( r.ok ) {
			return createResponseObject(r.content.toRepIssueMilestone(session))
		}
		
		return createErrorObject(r.exception);
	}
	
	override getIssueMilestones(ProgressMonitor monitor, String accountname, String slug) {
		val endpoint = createEndpoint("repositories/"+accountname+"/"+slug+"/issues/milestones")
		val ServiceResponse<JsonObject> r = endpoint.get
		
		if( r.ok ) {
			val Iterable<JsonObject> components = r.content.asJsonArray.iterable();
			return createResponseObject(components.map([e|e.toRepIssueMilestone(session)]).toList)
		}
		
		return createErrorObject(r.exception);
	}
	
	override getIssueVersion(ProgressMonitor monitor, String accountname, String slug, int id) {
		val endpoint = createEndpoint("repositories/"+accountname+"/"+slug+"/issues/versions/"+id)
		val ServiceResponse<JsonObject> r = endpoint.get
		
		if( r.ok ) {
			return createResponseObject(r.content.toRepIssueVersion(session))
		}
		
		return createErrorObject(r.exception);
	}
	
	override getIssueVersions(ProgressMonitor monitor, String accountname, String slug) {
		val endpoint = createEndpoint("repositories/"+accountname+"/"+slug+"/issues/milestones")
		val ServiceResponse<JsonObject> r = endpoint.get
		
		if( r.ok ) {
			val Iterable<JsonObject> components = r.content.asJsonArray.iterable();
			return createResponseObject(components.map([e|e.toRepIssueVersion(session)]).toList)
		}
		
		return createErrorObject(r.exception);
	}
	
	/*
	 * =========================================================================================
	 * PULLREQUEST APIS
	 * =========================================================================================
	 */
	 
	override getPullRequestComments(ProgressMonitor monitor, String accountname, String slug, int pullRequestId) {
		val endpoint = createEndpoint("repositories/"+accountname+"/"+slug+"/pullrequests/"+pullRequestId+"/comments")
		val ServiceResponse<JsonObject> r = endpoint.get
		
		if( r.ok ) {
			val Iterable<JsonObject> components = r.content.asJsonArray.iterable();
			return createResponseObject(components.map([e|e.toRepPullRequestComment(session)]).toList)
		}
		
		return createErrorObject(r.exception);
	}
	
	override getPullRequestComment(ProgressMonitor monitor, String accountname, String slug, int pullRequestId, int commentId) {
		val endpoint = createEndpoint("repositories/"+accountname+"/"+slug+"/pullrequests/"+pullRequestId+"/comments/"+commentId)
		val ServiceResponse<JsonObject> r = endpoint.get
		
		if( r.ok ) {
			return createResponseObject(r.content.toRepPullRequestComment(session))
		}
		
		return createErrorObject(r.exception);
	}
	
	override getPullRequestLike(ProgressMonitor monitor, String accountname, String slug, int pullRequestId, String likeraccount) {
		val endpoint = createEndpoint("repositories/"+accountname+"/"+slug+"/pullrequests/"+pullRequestId+"/likes/"+likeraccount)
		val ServiceResponse<JsonObject> r = endpoint.get
		
		if( r.ok ) {
			return createResponseObject(r.content.toRepPullRequestLike(session))
		}
		
		return createErrorObject(r.exception);
	}
	
	override getPullRequestLikes(ProgressMonitor monitor, String accountname, String slug, int pullRequestId) {
		val endpoint = createEndpoint("repositories/"+accountname+"/"+slug+"/pullrequests/"+pullRequestId+"/likes")
		val ServiceResponse<JsonObject> r = endpoint.get
		
		if( r.ok ) {
			val Iterable<JsonObject> components = r.content.asJsonArray.iterable();
			return createResponseObject(components.map([e|e.toRepPullRequestLike(session)]).toList)
		}
		
		return createErrorObject(r.exception);
	}
	
	/*
	 * =========================================================================================
	 * Repo Slug APIS
	 * =========================================================================================
	 */
	
	override getBranches(ProgressMonitor monitor, String accountname, String slug) {
		val endpoint = createEndpoint("repositories/"+accountname+"/"+slug+"/branches")
		val ServiceResponse<JsonObject> r = endpoint.get
		
		if( r.ok ) {
			val Map<String,RepSlugBranch> rv = new HashMap<String,RepSlugBranch>
			r.content.entrySet.forEach([e|rv.put(e.key,e.value.asJsonObject.toRepSlugBranch(session))])
			return createResponseObject(rv);
		}
		
		return createErrorObject(r.exception);
	}
	
	override getMainBranch(ProgressMonitor monitor, String accountname, String slug) {
		val endpoint = createEndpoint("repositories/"+accountname+"/"+slug+"/branches")
		val ServiceResponse<JsonObject> r = endpoint.get
		
		if( r.ok ) {
			return createResponseObject(r.content.string("name"));
		}
		
		return createErrorObject(r.exception);
	}
	
	override getTags(ProgressMonitor monitor, String accountname, String slug) {
		val endpoint = createEndpoint("repositories/"+accountname+"/"+slug+"/branches")
		val ServiceResponse<JsonObject> r = endpoint.get
		
		if( r.ok ) {
			val Map<String,RepSlugTag> rv = new HashMap<String,RepSlugTag>
			r.content.entrySet.forEach([e|rv.put(e.key,e.value.asJsonObject.toRepSlugTag(session))])
			return createResponseObject(rv);
		}
		
		return createErrorObject(r.exception);
	}
	
	override getFileHistory(ProgressMonitor monitor, String accountname, String slug, String node, String path) {
		val endpoint = createEndpoint("repositories/"+accountname+"/"+slug+"/filehistory/"+node+"/"+path)
		val ServiceResponse<JsonObject> r = endpoint.get
		
		if( r.ok ) {
			val Iterable<JsonObject> histories = r.content.asJsonArray.iterable();
			return createResponseObject(histories.map([e|e.toRepSlugFileHistory(session)]).toList)
		}
		
		return createErrorObject(r.exception);		
	}
}