/*******************************************************************************
 * Copyright (c) 2013 BestSolution.at and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Tom Schindl<tom.schindl@bestsolution.at> - initial API and implementation
 *******************************************************************************/
package at.bestsolution.bitbucket.rest.impl.services

import at.bestsolution.bitbucket.rest.BitbucketUserService
import at.bestsolution.bitbucket.rest.BitbucketSession
import at.bestsolution.bitbucket.rest.util.ProgressMonitor
import at.bestsolution.bitbucket.rest.impl.BitbucketServiceImpl$ServiceResponse
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import static extension at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil.*
import static extension at.bestsolution.bitbucket.rest.impl.model.user.UserRepositoryImpl.*
import static extension at.bestsolution.bitbucket.rest.impl.model.user.UserRepositoryOverviewImpl.*

class BitbucketUserServiceImpl extends BasicServiceImpl implements BitbucketUserService {
	
	new(BitbucketSession session) {
		super(session)
	}
	
	override getRepositories(ProgressMonitor monitor) {
		val ServiceResponse<JsonArray> r = createEndpoint("user/repositories").get
		
		if( r.ok ) {
			val Iterable<JsonObject> items = r.content.iterable
			return createResponseObject(items.map([e|e.toUserRepository(session)]).toList)
		} else {
			return createErrorObject(r.exception);
		}
	}
	
	override getRespositoriesOverview(ProgressMonitor monitor) {
		val ServiceResponse<JsonObject> r = createEndpoint("user/repositories/overview").get
		
		if( r.ok ) {
			return createResponseObject(r.content.toUserRepositoryOverview(session))
		}
		
		return createErrorObject(r.exception);
	}
	
}