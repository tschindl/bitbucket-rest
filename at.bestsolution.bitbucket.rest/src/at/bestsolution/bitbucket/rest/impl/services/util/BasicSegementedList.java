/*******************************************************************************
 * Copyright (c) 2013 BestSolution.at and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Tom Schindl<tom.schindl@bestsolution.at> - initial API and implementation
 *******************************************************************************/
package at.bestsolution.bitbucket.rest.impl.services.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import at.bestsolution.bitbucket.rest.util.ProgressMonitor;
import at.bestsolution.bitbucket.rest.util.SegmentedList;

public class BasicSegementedList<T> implements SegmentedList<T> {
	private final int segmentSize;
	private final int itemCount;
	
	private final SegmentCallback<T> segmentCallback;
	
	private final List<List<T>> segmentsCache;
	
	public BasicSegementedList(int itemCount, int segmentSize, SegmentCallback<T> segmentCallback) {
		this.itemCount = itemCount;
		this.segmentSize = segmentSize;
		this.segmentCallback = segmentCallback;
		this.segmentsCache = new ArrayList<>(segmentCount());
		
		for( int i = 0; i < segmentCount(); i++ ) {
			this.segmentsCache.add(null);
		}
	}
	
	@Override
	public List<T> getSegment(ProgressMonitor monitor, int idx) {
		List<T> rv = segmentsCache.get(idx); 
		
		if( rv == null ) {
			rv = this.segmentCallback.fetch(monitor, this, idx, segmentSize());
			segmentsCache.set(idx, rv);
		}
		
		return rv;
	}
	
	@Override
	public int segmentCount() {
		if( itemCount == 0 ) {
			return 0;
		}
		return itemCount / segmentSize + 1;
	}
	
	@Override
	public int segmentSize() {
		return segmentSize;
	}

	@Override
	public int itemCount() {
		return itemCount;
	}
	
	public static <T> SegmentedList<T> emptyList() {
		return new BasicSegementedList<>(0, 0, null);
	}
	
	public static <T> SegmentedList<T> list(int itemCount, int segmentSize, SegmentCallback<T> segmentCallback) {
		return new BasicSegementedList<>(itemCount, segmentSize, segmentCallback);
	}
	
	
	public interface SegmentCallback<T> {
		public List<T> fetch(ProgressMonitor monitor, SegmentedList<T> l, int segmentIndex, int segmentCount);
	}
	
	@Override
	public Iterator<List<T>> iterator() {
		return new Iterator<List<T>>() {
			private int idx = -1;
			
			@Override
			public boolean hasNext() {
				return idx + 1 < segmentCount();
			}

			@Override
			public List<T> next() {
				return getSegment(ProgressMonitor.NULLMONITOR, ++idx);
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException("Collection is not modifiable");
			}
		};
	}
	
	@Override
	public List<T> asFlatList(final ProgressMonitor monitor) {
		return new FlatSegmentedList<>(monitor, this);
	}
}
