/*******************************************************************************
 * Copyright (c) 2013 BestSolution.at and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Tom Schindl<tom.schindl@bestsolution.at> - initial API and implementation
 *******************************************************************************/
package at.bestsolution.bitbucket.rest.impl.services.util;

public class Constants {
	public static final String CHARSET_UTF8 = "UTF-8";
	
	public static final String HTTP_GET = "GET";

	public static final String HEADER_AUTHORIZATION = "Authorization";
	
	public static final String SERVICE_URL = "https://api.bitbucket.org";
	
	public static final String SERVICE_VERSION = "1.0";
	
	Constants() {
	}
}
