/*******************************************************************************
 * Copyright (c) 2013 BestSolution.at and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Tom Schindl<tom.schindl@bestsolution.at> - initial API and implementation
 *******************************************************************************/
package at.bestsolution.bitbucket.rest.impl.services.util;

import java.util.AbstractList;

import at.bestsolution.bitbucket.rest.util.ProgressMonitor;
import at.bestsolution.bitbucket.rest.util.SegmentedList;

public class FlatSegmentedList<T> extends AbstractList<T> {
	private final ProgressMonitor monitor;
	private final SegmentedList<T> list;
	
	public FlatSegmentedList(ProgressMonitor monitor, SegmentedList<T> list) {
		this.monitor = monitor;
		this.list = list;
	}
	
	@Override
	public T get(int index) {
		int segment = index / list.segmentSize();
		int idxInSegment = index % list.segmentSize();
		return list.getSegment(monitor, segment).get(idxInSegment);
	}

	@Override
	public int size() {
		return list.itemCount();
	}

}
