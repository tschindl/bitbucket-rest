/*******************************************************************************
 * Copyright (c) 2013 BestSolution.at and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Tom Schindl<tom.schindl@bestsolution.at> - initial API and implementation
 *******************************************************************************/
package at.bestsolution.bitbucket.rest.impl.services.util;

import java.util.Iterator;

import com.google.gson.JsonArray;

class JsonArrayIterator<T> implements Iterator<T> {
	private int idx = -1;
	
	private final JsonArray o;
	
	public JsonArrayIterator(JsonArray o) {
		this.o = o;
	}
	
	@Override
	public boolean hasNext() {
		return idx + 1 < o.size();
	}

	@SuppressWarnings("unchecked")
	@Override
	public T next() {
		return (T) o.get(++idx);
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException("Removal not supported");
	}
	
	static <T> Iterable<T> createIterable(final JsonArray o) {
		return new Iterable<T>() {
			@Override
			public Iterator<T> iterator() {
				return new JsonArrayIterator<>(o);
			}
		};
	}
}
