package at.bestsolution.bitbucket.rest.impl.services.util

import com.google.gson.JsonObject
import com.google.gson.JsonElement
import com.google.gson.JsonNull
import java.util.Collections
import java.util.List
import java.util.Date
import at.bestsolution.bitbucket.rest.model.base.RestEnum
import com.google.gson.JsonArray
import java.text.SimpleDateFormat

class JsonUtil {
	
	def static String string(JsonObject o, String name) {
		val e = o.get(name);
		
		switch(e) {
			JsonNull: null
			default: e?.asString
		}
	}
	
	def static boolean bool(JsonObject o, String name) {
		val e = o.get(name); 
		switch(e) {
			JsonNull: false
			default: e?.asBoolean
		} 
	}
	
	def static int integer(JsonObject o, String name) {
		val e = o.get(name);
		switch(e) {
			JsonNull: 0
			default: e?.asInt
		}
	}
	
	def static Integer integerObject(JsonObject o, String name) {
		val e = o.get(name);
		
		switch(e) {
			JsonNull: null
			default: e?.asInt
		}
	}
	
	def static JsonObject jsonObject(JsonObject o, String name) {
		val e = o.get(name);
		switch(e) {
			JsonNull: null
			default: e?.asJsonObject
		}
	}
	
	def static Date date(JsonObject o, String name) {
		val e = o.get(name);
		val v = e?.asString;
		if( v != null ) {
			return new SimpleDateFormat("yyyy-MM-dd hh:mm:ssX").parse(v);
		}
		return null;
	}
	
	def static <T extends Enum<T> & RestEnum> T enumeration(JsonObject o, String name, Class<T> enumClass) {
		val s = string(o, name);
		for( T t : enumClass.getEnumConstants() ) {
			if( t.getRestValue().equals(s) ) {
				return t;
			}
		}
		throw new IllegalArgumentException("No enumeration '"+s+"' is found in " + enumClass.getName());
	}
	
	def static <T extends JsonElement> Iterable<T> iterable(JsonObject o, String name) {
		val JsonElement e = o.get(name);
		if( e == null || e instanceof JsonNull ) {
			return Collections::emptyList();
		}
		return JsonUtil::iterable(o.get(name).getAsJsonArray());
	}
	
	def static <T> List<T> list(JsonObject o, String name, JsonObjectConverter<T> converter) {
		return (iterable(o,name) as Iterable<JsonObject>).map([e|converter.create(e)]).toList
	}
	
	def static <T extends JsonElement> Iterable<T> iterable(JsonArray o) {
		return JsonArrayIterator::createIterable(o);
	}
}