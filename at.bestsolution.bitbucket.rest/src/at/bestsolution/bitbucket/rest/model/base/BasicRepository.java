/*******************************************************************************
 * Copyright (c) 2013 BestSolution.at and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Tom Schindl<tom.schindl@bestsolution.at> - initial API and implementation
 *******************************************************************************/
package at.bestsolution.bitbucket.rest.model.base;

public interface BasicRepository {
	/**
	 * @return The owner's account name.
	 */
	public String getOwner();
	/**
	 * @return The slug used to identify the repository.
	 */
	public String getSlug();
	/**
	 * @return A boolean indicating that a repository is private.
	 */
	public boolean isPrivate();
	/**
	 * @return The name of the repository.
	 */
	public String getName();
}
