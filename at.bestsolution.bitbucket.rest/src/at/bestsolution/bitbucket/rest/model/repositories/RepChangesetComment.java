/*******************************************************************************
 * Copyright (c) 2013 BestSolution.at and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Tom Schindl<tom.schindl@bestsolution.at> - initial API and implementation
 *******************************************************************************/
package at.bestsolution.bitbucket.rest.model.repositories;

import java.util.Date;

public interface RepChangesetComment {
	public String getUsername();
	public String getNode();
	public int getCommentId();
	public String getDisplayName();
	public Integer getParentId();
	public boolean isDeleted();
	public Date getUtcLastUpdated();
	public String getFilenameHash();
	public String getFilename();
	public String getContent();
	public String getContentRendered();
	public String getUserAvatarUrl();
	public Integer getLineFrom();
	public Integer getLineTo();
	public Date getUtcCreatedOn();
	public boolean isSpam();
}
