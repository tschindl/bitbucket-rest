/*******************************************************************************
 * Copyright (c) 2013 BestSolution.at and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Tom Schindl<tom.schindl@bestsolution.at> - initial API and implementation
 *******************************************************************************/
package at.bestsolution.bitbucket.rest.model.repositories;

import java.util.List;

public interface RepChangesetDiff {
	public FromFile getFromFile();
	public List<Hunk> getHunks();
//	public List<List<Integer>> getUnifiedMap();
	
	public interface FromFile {
		public Meta getMeta();
		public List<Page> getPages();
		public int getBlockNum();	
	}
	
	public interface Meta {
		public int getNumLines();
		public List<Integer> getPosition();
		public LongestLine getLongestLine();
		public String getName();
		public String getFilename();
	}
	
	public interface LongestLine {
		public String getHtml();
	}
	
	public interface Page {
		public String getHtml();
//		public List<Object> getGutters();
	}
	
	public interface Hunk {
		public List<String> getToLines();
		public int getFromCount();
		public int getToStart();
		public int getFromStart();
		public int getToCount();
		public String getType();
		public boolean isConflict();
		public List<String> getFromLines();
	}
}
