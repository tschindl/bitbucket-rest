/*******************************************************************************
 * Copyright (c) 2013 BestSolution.at and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Tom Schindl<tom.schindl@bestsolution.at> - initial API and implementation
 *******************************************************************************/
package at.bestsolution.bitbucket.rest.model.repositories;

import java.util.Date;

import at.bestsolution.bitbucket.rest.model.base.RestEnum;
import at.bestsolution.bitbucket.rest.model.user.UserUser;

public interface RepIssue {
	public enum State implements RestEnum {
		NEW("new"),
		OPEN("open"),
		RESOLVED("resolved"),
		ON_HOLD("on hold"),
		INVALID("invalid"),
		DUPLICATE("duplicate"),
		WONTFIX("wontfix");
		
		private final String restValue;
		
		private State(String restValue) {
			this.restValue = restValue;
		}
		
		public String getRestValue() {
			return restValue;
		}
	}
	
	public enum Priority implements RestEnum {
		TRIVIAL("trivial"),
		MINOR("minor"),
		MAJOR("major"),
		CRITICAL("critical"),
		BLOCKER("blocker");
		
		private final String restValue;
		
		private Priority(String restValue) {
			this.restValue = restValue;
		}
		
		public String getRestValue() {
			return restValue;
		}
	}
	
	public enum Kind implements RestEnum {
		BUG("bug"),
		ENHANCEMENT("enhancement"),
		PROPOSAL("proposal"),
		TASK("task");
		
		private final String restValue;
		
		private Kind(String restValue) {
			this.restValue = restValue;
		}
		
		public String getRestValue() {
			return restValue;
		}
	}
	
	public interface Metadata {
		public Kind getKind();
		public String getVersion();
		public String getComponent();
		public String getMilestone();
	}
	
	public State getStatus();
	public Priority getPriority();
	public String getTitle();
	public UserUser getReportedBy();
	public Date getUtcLastUpdated();
	public int getCommentCount();
	public Metadata getMetadata();
	public String getContent();
	public int getLocalId();
	public int getFollowerCount();
	public Date getUtcCreatedOn();
	public String getResourceUri();
	public boolean isSpam();
}
