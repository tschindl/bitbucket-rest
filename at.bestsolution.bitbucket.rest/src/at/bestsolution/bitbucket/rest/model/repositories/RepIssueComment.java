/*******************************************************************************
 * Copyright (c) 2013 BestSolution.at and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Tom Schindl<tom.schindl@bestsolution.at> - initial API and implementation
 *******************************************************************************/
package at.bestsolution.bitbucket.rest.model.repositories;

import java.util.Date;

import at.bestsolution.bitbucket.rest.model.user.UserUser;

public interface RepIssueComment {
	public String getContent();
	public UserUser getAuthorInfo();
	public int getCommentId();
	public Date getUtcUpdatedOn();
	public Date getUtcCreatedOn();
	public boolean isSpam();
}
