/*******************************************************************************
 * Copyright (c) 2013 BestSolution.at and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Tom Schindl<tom.schindl@bestsolution.at> - initial API and implementation
 *******************************************************************************/
package at.bestsolution.bitbucket.rest.model.repositories;

import java.util.Date;

import at.bestsolution.bitbucket.rest.model.user.UserUser;

public interface RepPullRequestComment {
	/**
	 * @return An integer representing an id for the request. This is created by Bitbucket.
	 */
	public int getPullRequestId();
	/**
	 * @return An integer representing an id for the comment. This is created by Bitbucket.
	 */
	public int getCommentId();
	/**
	 * @return An integer representing an id for the comment's. This  This is created by Bitbucket.
	 */
	public Integer getParentId();	
	/**
	 * @return A Universal Timestamp Coordinate timestamp for the last time the comment was updated.
	 */
	public Date getUtcLastUpdated();
	/**
	 * @return Currently not used.
	 */
	public String getFilenameHash();
	/**
	 * @return The revision where the changeset originated.
	 */
	public String getBaseRev();	
	/**
	 * @return The filename the new comment concerns.
	 */
	public String getFilename();
	/**
	 * @return The content of the comment.
	 */
	public String getContent();
	/**
	 * @return The content including the HTML and other formatting material.
	 */
	public String getContentRendered();	
	/**
	 * @return The account profile of the user that added the comment. Only Bitbucket account holders can comment on requests.
	 */
	public UserUser getAuthorInfo();
	/**
	 * @return The starting line of the comment.
	 */
	public Integer getLineFrom();
	/**
	 * @return The ending line of the comment.
	 */
	public Integer getLineTo();	
	/**
	 * @return The revision representing the destination source.
	 */
	public String getDestRev();	
	/**
	 * @return A Universal Timestamp Coordinate timestamp for the comment creation.
	 */
	public Date getUtcCreatedOn();
	/**
	 * @return Currently not used.
	 */
	public String getAnchor();	
	/**
	 * @return A boolean indicating if the request is spam or not. The Bitbucket service uses Akismet to protect its users from spam. When Akismet indicates a comment may be spam, Bitbucket sets is_spam to true.
	 */
	public boolean isSpam();	
}
