/*******************************************************************************
 * Copyright (c) 2013 BestSolution.at and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Tom Schindl<tom.schindl@bestsolution.at> - initial API and implementation
 *******************************************************************************/
package at.bestsolution.bitbucket.rest.model.repositories;

import java.util.Date;

import at.bestsolution.bitbucket.rest.model.base.BasicRepository;

public interface RepRepository extends BasicRepository {
	public String getScm();
	
	public boolean hasWiki();
	
	public String getCreator();
	
	public String getLogo();
	
	public String getEmailMailinglist();
	
	public boolean isMq();
	
	public int getSize();
	
	public boolean isReadOnly();
	
	public String getForkOf(); 
	
	public String getMqOf();
	
	public int getFollowersCount();
	
	public String getWebsite();
	
	public String getDescription();
	
	public boolean hasIssues();
	
	public boolean isFork();
	
	public String getLanguage();
	
	public Date getUtcLastUpdated();
	
	public Date getUtcCreatedOn();
	
	public boolean isEmailWriters();
	
	public boolean isNoPublicForks();
	
	public String getResourceURI();
	
	public int getForksCount();
	
	public String getState();
}
