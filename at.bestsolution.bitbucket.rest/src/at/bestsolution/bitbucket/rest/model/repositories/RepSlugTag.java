/*******************************************************************************
 * Copyright (c) 2013 BestSolution.at and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Tom Schindl<tom.schindl@bestsolution.at> - initial API and implementation
 *******************************************************************************/
package at.bestsolution.bitbucket.rest.model.repositories;

import java.util.Date;
import java.util.List;

public interface RepSlugTag {
	public interface File {
		public String getType();
		public String getFile();
	}
	
	public String getNode();
	public List<File> getFiles();
	public String getRawAuthor();
	public Date getUtcTimestamp();
	public String getAuthor();
	public String getRawNode();
	public List<String> getParents();
	public String getBranch();
	public String getMessage();
	public int getRevision();
	public int getSize();
}
