package at.bestsolution.bitbucket.rest.impl.model.base;

import at.bestsolution.bitbucket.rest.BitbucketSession;
import org.eclipse.xtend.lib.Data;
import org.eclipse.xtext.xbase.lib.util.ToStringHelper;

@Data
@SuppressWarnings("all")
public class BaseObject {
  private final BitbucketSession _session;
  
  public BitbucketSession getSession() {
    return this._session;
  }
  
  public BaseObject(final BitbucketSession session) {
    super();
    this._session = session;
  }
  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((_session== null) ? 0 : _session.hashCode());
    return result;
  }
  
  @Override
  public boolean equals(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    BaseObject other = (BaseObject) obj;
    if (_session == null) {
      if (other._session != null)
        return false;
    } else if (!_session.equals(other._session))
      return false;
    return true;
  }
  
  @Override
  public String toString() {
    String result = new ToStringHelper().toString(this);
    return result;
  }
}
