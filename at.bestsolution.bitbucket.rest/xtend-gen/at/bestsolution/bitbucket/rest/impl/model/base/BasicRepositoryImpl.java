package at.bestsolution.bitbucket.rest.impl.model.base;

import at.bestsolution.bitbucket.rest.BitbucketSession;
import at.bestsolution.bitbucket.rest.impl.model.base.BaseObject;
import at.bestsolution.bitbucket.rest.model.base.BasicRepository;
import org.eclipse.xtend.lib.Data;
import org.eclipse.xtext.xbase.lib.util.ToStringHelper;

@Data
@SuppressWarnings("all")
public abstract class BasicRepositoryImpl extends BaseObject implements BasicRepository {
  private final String _name;
  
  public String getName() {
    return this._name;
  }
  
  private final String _owner;
  
  public String getOwner() {
    return this._owner;
  }
  
  private final String _slug;
  
  public String getSlug() {
    return this._slug;
  }
  
  private final boolean __private;
  
  public boolean is_private() {
    return this.__private;
  }
  
  public boolean isPrivate() {
    return this.is_private();
  }
  
  public BasicRepositoryImpl(final BitbucketSession session, final String name, final String owner, final String slug, final boolean _private) {
    super(session);
    this._name = name;
    this._owner = owner;
    this._slug = slug;
    this.__private = _private;
  }
  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + ((_name== null) ? 0 : _name.hashCode());
    result = prime * result + ((_owner== null) ? 0 : _owner.hashCode());
    result = prime * result + ((_slug== null) ? 0 : _slug.hashCode());
    result = prime * result + (__private ? 1231 : 1237);
    return result;
  }
  
  @Override
  public boolean equals(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    if (!super.equals(obj))
      return false;
    BasicRepositoryImpl other = (BasicRepositoryImpl) obj;
    if (_name == null) {
      if (other._name != null)
        return false;
    } else if (!_name.equals(other._name))
      return false;
    if (_owner == null) {
      if (other._owner != null)
        return false;
    } else if (!_owner.equals(other._owner))
      return false;
    if (_slug == null) {
      if (other._slug != null)
        return false;
    } else if (!_slug.equals(other._slug))
      return false;
    if (other.__private != __private)
      return false;
    return true;
  }
  
  @Override
  public String toString() {
    String result = new ToStringHelper().toString(this);
    return result;
  }
}
