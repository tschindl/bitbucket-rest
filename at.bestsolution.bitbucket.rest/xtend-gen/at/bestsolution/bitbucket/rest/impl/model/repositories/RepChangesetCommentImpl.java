package at.bestsolution.bitbucket.rest.impl.model.repositories;

import at.bestsolution.bitbucket.rest.BitbucketSession;
import at.bestsolution.bitbucket.rest.impl.model.base.BaseObject;
import at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil;
import at.bestsolution.bitbucket.rest.model.repositories.RepChangesetComment;
import com.google.gson.JsonObject;
import java.util.Date;
import org.eclipse.xtend.lib.Data;
import org.eclipse.xtext.xbase.lib.util.ToStringHelper;

@Data
@SuppressWarnings("all")
public class RepChangesetCommentImpl extends BaseObject implements RepChangesetComment {
  private final String _username;
  
  public String getUsername() {
    return this._username;
  }
  
  private final String _node;
  
  public String getNode() {
    return this._node;
  }
  
  private final int _commentId;
  
  public int getCommentId() {
    return this._commentId;
  }
  
  private final String _displayName;
  
  public String getDisplayName() {
    return this._displayName;
  }
  
  private final Integer _parentId;
  
  public Integer getParentId() {
    return this._parentId;
  }
  
  private final boolean _deleted;
  
  public boolean isDeleted() {
    return this._deleted;
  }
  
  private final Date _utcLastUpdated;
  
  public Date getUtcLastUpdated() {
    return this._utcLastUpdated;
  }
  
  private final String _filenameHash;
  
  public String getFilenameHash() {
    return this._filenameHash;
  }
  
  private final String _filename;
  
  public String getFilename() {
    return this._filename;
  }
  
  private final String _content;
  
  public String getContent() {
    return this._content;
  }
  
  private final String _contentRendered;
  
  public String getContentRendered() {
    return this._contentRendered;
  }
  
  private final String _userAvatarUrl;
  
  public String getUserAvatarUrl() {
    return this._userAvatarUrl;
  }
  
  private final Integer _lineFrom;
  
  public Integer getLineFrom() {
    return this._lineFrom;
  }
  
  private final Integer _lineTo;
  
  public Integer getLineTo() {
    return this._lineTo;
  }
  
  private final Date _utcCreatedOn;
  
  public Date getUtcCreatedOn() {
    return this._utcCreatedOn;
  }
  
  private final boolean _spam;
  
  public boolean isSpam() {
    return this._spam;
  }
  
  public static RepChangesetComment toRepChangesetComment(final JsonObject o, final BitbucketSession session) {
    final String _username = JsonUtil.string(o, "username");
    final String _node = JsonUtil.string(o, "node");
    final int _commentId = JsonUtil.integer(o, "comment_id");
    final String _displayName = JsonUtil.string(o, "display_name");
    final Integer _parentId = JsonUtil.integerObject(o, "parent_id");
    final boolean _deleted = JsonUtil.bool(o, "deleted");
    final Date _utcLastUpdated = JsonUtil.date(o, "utc_last_updated");
    final String _filenameHash = JsonUtil.string(o, "filename_hash");
    final String _filename = JsonUtil.string(o, "filename");
    final String _content = JsonUtil.string(o, "content");
    final String _contentRendered = JsonUtil.string(o, "content_rendered");
    final String _userAvatarUrl = JsonUtil.string(o, "user_avatar_url");
    final Integer _lineFrom = JsonUtil.integerObject(o, "line_from");
    final Integer _lineTo = JsonUtil.integerObject(o, "line_to");
    final Date _utcCreatedOn = JsonUtil.date(o, "utc_created_on");
    final boolean _spam = JsonUtil.bool(o, "is_spam");
    RepChangesetCommentImpl _repChangesetCommentImpl = new RepChangesetCommentImpl(session, _username, _node, _commentId, _displayName, _parentId, _deleted, _utcLastUpdated, _filenameHash, _filename, _content, _contentRendered, _userAvatarUrl, _lineFrom, _lineTo, _utcCreatedOn, _spam);
    final RepChangesetComment rv = _repChangesetCommentImpl;
    return rv;
  }
  
  public RepChangesetCommentImpl(final BitbucketSession session, final String username, final String node, final int commentId, final String displayName, final Integer parentId, final boolean deleted, final Date utcLastUpdated, final String filenameHash, final String filename, final String content, final String contentRendered, final String userAvatarUrl, final Integer lineFrom, final Integer lineTo, final Date utcCreatedOn, final boolean spam) {
    super(session);
    this._username = username;
    this._node = node;
    this._commentId = commentId;
    this._displayName = displayName;
    this._parentId = parentId;
    this._deleted = deleted;
    this._utcLastUpdated = utcLastUpdated;
    this._filenameHash = filenameHash;
    this._filename = filename;
    this._content = content;
    this._contentRendered = contentRendered;
    this._userAvatarUrl = userAvatarUrl;
    this._lineFrom = lineFrom;
    this._lineTo = lineTo;
    this._utcCreatedOn = utcCreatedOn;
    this._spam = spam;
  }
  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + ((_username== null) ? 0 : _username.hashCode());
    result = prime * result + ((_node== null) ? 0 : _node.hashCode());
    result = prime * result + _commentId;
    result = prime * result + ((_displayName== null) ? 0 : _displayName.hashCode());
    result = prime * result + ((_parentId== null) ? 0 : _parentId.hashCode());
    result = prime * result + (_deleted ? 1231 : 1237);
    result = prime * result + ((_utcLastUpdated== null) ? 0 : _utcLastUpdated.hashCode());
    result = prime * result + ((_filenameHash== null) ? 0 : _filenameHash.hashCode());
    result = prime * result + ((_filename== null) ? 0 : _filename.hashCode());
    result = prime * result + ((_content== null) ? 0 : _content.hashCode());
    result = prime * result + ((_contentRendered== null) ? 0 : _contentRendered.hashCode());
    result = prime * result + ((_userAvatarUrl== null) ? 0 : _userAvatarUrl.hashCode());
    result = prime * result + ((_lineFrom== null) ? 0 : _lineFrom.hashCode());
    result = prime * result + ((_lineTo== null) ? 0 : _lineTo.hashCode());
    result = prime * result + ((_utcCreatedOn== null) ? 0 : _utcCreatedOn.hashCode());
    result = prime * result + (_spam ? 1231 : 1237);
    return result;
  }
  
  @Override
  public boolean equals(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    if (!super.equals(obj))
      return false;
    RepChangesetCommentImpl other = (RepChangesetCommentImpl) obj;
    if (_username == null) {
      if (other._username != null)
        return false;
    } else if (!_username.equals(other._username))
      return false;
    if (_node == null) {
      if (other._node != null)
        return false;
    } else if (!_node.equals(other._node))
      return false;
    if (other._commentId != _commentId)
      return false;
    if (_displayName == null) {
      if (other._displayName != null)
        return false;
    } else if (!_displayName.equals(other._displayName))
      return false;
    if (_parentId == null) {
      if (other._parentId != null)
        return false;
    } else if (!_parentId.equals(other._parentId))
      return false;
    if (other._deleted != _deleted)
      return false;
    if (_utcLastUpdated == null) {
      if (other._utcLastUpdated != null)
        return false;
    } else if (!_utcLastUpdated.equals(other._utcLastUpdated))
      return false;
    if (_filenameHash == null) {
      if (other._filenameHash != null)
        return false;
    } else if (!_filenameHash.equals(other._filenameHash))
      return false;
    if (_filename == null) {
      if (other._filename != null)
        return false;
    } else if (!_filename.equals(other._filename))
      return false;
    if (_content == null) {
      if (other._content != null)
        return false;
    } else if (!_content.equals(other._content))
      return false;
    if (_contentRendered == null) {
      if (other._contentRendered != null)
        return false;
    } else if (!_contentRendered.equals(other._contentRendered))
      return false;
    if (_userAvatarUrl == null) {
      if (other._userAvatarUrl != null)
        return false;
    } else if (!_userAvatarUrl.equals(other._userAvatarUrl))
      return false;
    if (_lineFrom == null) {
      if (other._lineFrom != null)
        return false;
    } else if (!_lineFrom.equals(other._lineFrom))
      return false;
    if (_lineTo == null) {
      if (other._lineTo != null)
        return false;
    } else if (!_lineTo.equals(other._lineTo))
      return false;
    if (_utcCreatedOn == null) {
      if (other._utcCreatedOn != null)
        return false;
    } else if (!_utcCreatedOn.equals(other._utcCreatedOn))
      return false;
    if (other._spam != _spam)
      return false;
    return true;
  }
  
  @Override
  public String toString() {
    String result = new ToStringHelper().toString(this);
    return result;
  }
}
