package at.bestsolution.bitbucket.rest.impl.model.repositories;

import at.bestsolution.bitbucket.rest.BitbucketSession;
import at.bestsolution.bitbucket.rest.impl.model.base.BaseObject;
import at.bestsolution.bitbucket.rest.impl.model.repositories.RepChangesetDiffImpl_FromFile;
import at.bestsolution.bitbucket.rest.impl.model.repositories.RepChangesetDiffImpl_Hunk;
import at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil;
import at.bestsolution.bitbucket.rest.model.repositories.RepChangesetDiff;
import at.bestsolution.bitbucket.rest.model.repositories.RepChangesetDiff.FromFile;
import at.bestsolution.bitbucket.rest.model.repositories.RepChangesetDiff.Hunk;
import com.google.gson.JsonObject;
import java.util.List;
import org.eclipse.xtend.lib.Data;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.util.ToStringHelper;

@Data
@SuppressWarnings("all")
public class RepChangesetDiffImpl extends BaseObject implements RepChangesetDiff {
  private final FromFile _fromFile;
  
  public FromFile getFromFile() {
    return this._fromFile;
  }
  
  private final List<Hunk> _hunks;
  
  public List<Hunk> getHunks() {
    return this._hunks;
  }
  
  public static RepChangesetDiff toRepChangesetDiff(final JsonObject o, final BitbucketSession session) {
    JsonObject _jsonObject = JsonUtil.jsonObject(o, "from_file");
    final FromFile _fromFile = RepChangesetDiffImpl_FromFile.toRepChangesetDiff_FromFile(_jsonObject, session);
    final Iterable<JsonObject> hunksIt = JsonUtil.<JsonObject>iterable(o, "hunks");
    final Function1<JsonObject,Hunk> _function = new Function1<JsonObject,Hunk>() {
        public Hunk apply(final JsonObject e) {
          Hunk _repChangesetDiff_Hunk = RepChangesetDiffImpl_Hunk.toRepChangesetDiff_Hunk(e, session);
          return _repChangesetDiff_Hunk;
        }
      };
    Iterable<Hunk> _map = IterableExtensions.<JsonObject, Hunk>map(hunksIt, _function);
    final List<Hunk> _hunks = IterableExtensions.<Hunk>toList(_map);
    RepChangesetDiffImpl _repChangesetDiffImpl = new RepChangesetDiffImpl(session, _fromFile, _hunks);
    final RepChangesetDiff rv = _repChangesetDiffImpl;
    return rv;
  }
  
  public RepChangesetDiffImpl(final BitbucketSession session, final FromFile fromFile, final List<Hunk> hunks) {
    super(session);
    this._fromFile = fromFile;
    this._hunks = hunks;
  }
  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + ((_fromFile== null) ? 0 : _fromFile.hashCode());
    result = prime * result + ((_hunks== null) ? 0 : _hunks.hashCode());
    return result;
  }
  
  @Override
  public boolean equals(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    if (!super.equals(obj))
      return false;
    RepChangesetDiffImpl other = (RepChangesetDiffImpl) obj;
    if (_fromFile == null) {
      if (other._fromFile != null)
        return false;
    } else if (!_fromFile.equals(other._fromFile))
      return false;
    if (_hunks == null) {
      if (other._hunks != null)
        return false;
    } else if (!_hunks.equals(other._hunks))
      return false;
    return true;
  }
  
  @Override
  public String toString() {
    String result = new ToStringHelper().toString(this);
    return result;
  }
}
