package at.bestsolution.bitbucket.rest.impl.model.repositories;

import at.bestsolution.bitbucket.rest.BitbucketSession;
import at.bestsolution.bitbucket.rest.impl.model.base.BaseObject;
import at.bestsolution.bitbucket.rest.impl.model.repositories.RepChangesetDiffImpl_Meta;
import at.bestsolution.bitbucket.rest.impl.model.repositories.RepChangesetDiffImpl_Page;
import at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil;
import at.bestsolution.bitbucket.rest.model.repositories.RepChangesetDiff.FromFile;
import at.bestsolution.bitbucket.rest.model.repositories.RepChangesetDiff.Meta;
import at.bestsolution.bitbucket.rest.model.repositories.RepChangesetDiff.Page;
import com.google.gson.JsonObject;
import java.util.List;
import org.eclipse.xtend.lib.Data;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.util.ToStringHelper;

@Data
@SuppressWarnings("all")
public class RepChangesetDiffImpl_FromFile extends BaseObject implements FromFile {
  private final Meta _meta;
  
  public Meta getMeta() {
    return this._meta;
  }
  
  private final List<Page> _pages;
  
  public List<Page> getPages() {
    return this._pages;
  }
  
  private final int _blockNum;
  
  public int getBlockNum() {
    return this._blockNum;
  }
  
  public static FromFile toRepChangesetDiff_FromFile(final JsonObject o, final BitbucketSession session) {
    JsonObject _jsonObject = JsonUtil.jsonObject(o, "meta");
    final Meta _meta = RepChangesetDiffImpl_Meta.toRepChangesetDiff_Meta(_jsonObject, session);
    final Iterable<JsonObject> pageIt = JsonUtil.<JsonObject>iterable(o, "pages");
    final Function1<JsonObject,Page> _function = new Function1<JsonObject,Page>() {
        public Page apply(final JsonObject e) {
          Page _repChangesetDiff_Page = RepChangesetDiffImpl_Page.toRepChangesetDiff_Page(e, session);
          return _repChangesetDiff_Page;
        }
      };
    Iterable<Page> _map = IterableExtensions.<JsonObject, Page>map(pageIt, _function);
    final List<Page> _pages = IterableExtensions.<Page>toList(_map);
    final int _blockNum = JsonUtil.integer(o, "block_num");
    RepChangesetDiffImpl_FromFile _repChangesetDiffImpl_FromFile = new RepChangesetDiffImpl_FromFile(session, _meta, _pages, _blockNum);
    final FromFile rv = _repChangesetDiffImpl_FromFile;
    return rv;
  }
  
  public RepChangesetDiffImpl_FromFile(final BitbucketSession session, final Meta meta, final List<Page> pages, final int blockNum) {
    super(session);
    this._meta = meta;
    this._pages = pages;
    this._blockNum = blockNum;
  }
  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + ((_meta== null) ? 0 : _meta.hashCode());
    result = prime * result + ((_pages== null) ? 0 : _pages.hashCode());
    result = prime * result + _blockNum;
    return result;
  }
  
  @Override
  public boolean equals(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    if (!super.equals(obj))
      return false;
    RepChangesetDiffImpl_FromFile other = (RepChangesetDiffImpl_FromFile) obj;
    if (_meta == null) {
      if (other._meta != null)
        return false;
    } else if (!_meta.equals(other._meta))
      return false;
    if (_pages == null) {
      if (other._pages != null)
        return false;
    } else if (!_pages.equals(other._pages))
      return false;
    if (other._blockNum != _blockNum)
      return false;
    return true;
  }
  
  @Override
  public String toString() {
    String result = new ToStringHelper().toString(this);
    return result;
  }
}
