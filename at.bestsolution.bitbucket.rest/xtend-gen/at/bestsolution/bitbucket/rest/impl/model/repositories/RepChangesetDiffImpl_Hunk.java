package at.bestsolution.bitbucket.rest.impl.model.repositories;

import at.bestsolution.bitbucket.rest.BitbucketSession;
import at.bestsolution.bitbucket.rest.impl.model.base.BaseObject;
import at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil;
import at.bestsolution.bitbucket.rest.model.repositories.RepChangesetDiff.Hunk;
import com.google.gson.JsonObject;
import java.util.List;
import org.eclipse.xtend.lib.Data;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.util.ToStringHelper;

@Data
@SuppressWarnings("all")
public class RepChangesetDiffImpl_Hunk extends BaseObject implements Hunk {
  private final List<String> _toLines;
  
  public List<String> getToLines() {
    return this._toLines;
  }
  
  private final int _fromCount;
  
  public int getFromCount() {
    return this._fromCount;
  }
  
  private final int _toStart;
  
  public int getToStart() {
    return this._toStart;
  }
  
  private final int _fromStart;
  
  public int getFromStart() {
    return this._fromStart;
  }
  
  private final int _toCount;
  
  public int getToCount() {
    return this._toCount;
  }
  
  private final String _type;
  
  public String getType() {
    return this._type;
  }
  
  private final boolean _conflict;
  
  public boolean isConflict() {
    return this._conflict;
  }
  
  private final List<String> _fromLines;
  
  public List<String> getFromLines() {
    return this._fromLines;
  }
  
  public static Hunk toRepChangesetDiff_Hunk(final JsonObject o, final BitbucketSession session) {
    final Iterable<JsonObject> toLinesIt = JsonUtil.<JsonObject>iterable(o, "to_lines");
    final Function1<JsonObject,String> _function = new Function1<JsonObject,String>() {
        public String apply(final JsonObject e) {
          String _asString = e.getAsString();
          return _asString;
        }
      };
    Iterable<String> _map = IterableExtensions.<JsonObject, String>map(toLinesIt, _function);
    final List<String> _toLines = IterableExtensions.<String>toList(_map);
    final int _fromCount = JsonUtil.integer(o, "from_count");
    final int _toStart = JsonUtil.integer(o, "to_start");
    final int _fromStart = JsonUtil.integer(o, "from_start");
    final int _toCount = JsonUtil.integer(o, "to_count");
    final String _type = JsonUtil.string(o, "type");
    final boolean _conflict = JsonUtil.bool(o, "conflict");
    final Iterable<JsonObject> fromLinesIt = JsonUtil.<JsonObject>iterable(o, "from_lines");
    final Function1<JsonObject,String> _function_1 = new Function1<JsonObject,String>() {
        public String apply(final JsonObject e) {
          String _asString = e.getAsString();
          return _asString;
        }
      };
    Iterable<String> _map_1 = IterableExtensions.<JsonObject, String>map(fromLinesIt, _function_1);
    final List<String> _fromLines = IterableExtensions.<String>toList(_map_1);
    RepChangesetDiffImpl_Hunk _repChangesetDiffImpl_Hunk = new RepChangesetDiffImpl_Hunk(session, _toLines, _fromCount, _toStart, _fromStart, _toCount, _type, _conflict, _fromLines);
    final Hunk rv = _repChangesetDiffImpl_Hunk;
    return rv;
  }
  
  public RepChangesetDiffImpl_Hunk(final BitbucketSession session, final List<String> toLines, final int fromCount, final int toStart, final int fromStart, final int toCount, final String type, final boolean conflict, final List<String> fromLines) {
    super(session);
    this._toLines = toLines;
    this._fromCount = fromCount;
    this._toStart = toStart;
    this._fromStart = fromStart;
    this._toCount = toCount;
    this._type = type;
    this._conflict = conflict;
    this._fromLines = fromLines;
  }
  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + ((_toLines== null) ? 0 : _toLines.hashCode());
    result = prime * result + _fromCount;
    result = prime * result + _toStart;
    result = prime * result + _fromStart;
    result = prime * result + _toCount;
    result = prime * result + ((_type== null) ? 0 : _type.hashCode());
    result = prime * result + (_conflict ? 1231 : 1237);
    result = prime * result + ((_fromLines== null) ? 0 : _fromLines.hashCode());
    return result;
  }
  
  @Override
  public boolean equals(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    if (!super.equals(obj))
      return false;
    RepChangesetDiffImpl_Hunk other = (RepChangesetDiffImpl_Hunk) obj;
    if (_toLines == null) {
      if (other._toLines != null)
        return false;
    } else if (!_toLines.equals(other._toLines))
      return false;
    if (other._fromCount != _fromCount)
      return false;
    if (other._toStart != _toStart)
      return false;
    if (other._fromStart != _fromStart)
      return false;
    if (other._toCount != _toCount)
      return false;
    if (_type == null) {
      if (other._type != null)
        return false;
    } else if (!_type.equals(other._type))
      return false;
    if (other._conflict != _conflict)
      return false;
    if (_fromLines == null) {
      if (other._fromLines != null)
        return false;
    } else if (!_fromLines.equals(other._fromLines))
      return false;
    return true;
  }
  
  @Override
  public String toString() {
    String result = new ToStringHelper().toString(this);
    return result;
  }
}
