package at.bestsolution.bitbucket.rest.impl.model.repositories;

import at.bestsolution.bitbucket.rest.BitbucketSession;
import at.bestsolution.bitbucket.rest.impl.model.base.BaseObject;
import at.bestsolution.bitbucket.rest.impl.model.repositories.RepChangesetDiffImpl_LongestLine;
import at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil;
import at.bestsolution.bitbucket.rest.model.repositories.RepChangesetDiff.LongestLine;
import at.bestsolution.bitbucket.rest.model.repositories.RepChangesetDiff.Meta;
import com.google.gson.JsonObject;
import java.util.List;
import org.eclipse.xtend.lib.Data;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.util.ToStringHelper;

@Data
@SuppressWarnings("all")
public class RepChangesetDiffImpl_Meta extends BaseObject implements Meta {
  private final int _numLines;
  
  public int getNumLines() {
    return this._numLines;
  }
  
  private final List<Integer> _position;
  
  public List<Integer> getPosition() {
    return this._position;
  }
  
  private final LongestLine _longestLine;
  
  public LongestLine getLongestLine() {
    return this._longestLine;
  }
  
  private final String _name;
  
  public String getName() {
    return this._name;
  }
  
  private final String _filename;
  
  public String getFilename() {
    return this._filename;
  }
  
  public static Meta toRepChangesetDiff_Meta(final JsonObject o, final BitbucketSession session) {
    final int _numLines = JsonUtil.integer(o, "num_lines");
    final Iterable<JsonObject> posIt = JsonUtil.<JsonObject>iterable(o, "position");
    final Function1<JsonObject,Integer> _function = new Function1<JsonObject,Integer>() {
        public Integer apply(final JsonObject e) {
          int _asInt = e.getAsInt();
          return Integer.valueOf(_asInt);
        }
      };
    Iterable<Integer> _map = IterableExtensions.<JsonObject, Integer>map(posIt, _function);
    final List<Integer> _position = IterableExtensions.<Integer>toList(_map);
    JsonObject _jsonObject = JsonUtil.jsonObject(o, "longest_line");
    final LongestLine _longestLine = RepChangesetDiffImpl_LongestLine.toRepChangesetDiff_LongestLine(_jsonObject, session);
    final String _name = JsonUtil.string(o, "name");
    final String _filename = JsonUtil.string(o, "filename");
    RepChangesetDiffImpl_Meta _repChangesetDiffImpl_Meta = new RepChangesetDiffImpl_Meta(session, _numLines, _position, _longestLine, _name, _filename);
    final Meta rv = _repChangesetDiffImpl_Meta;
    return rv;
  }
  
  public RepChangesetDiffImpl_Meta(final BitbucketSession session, final int numLines, final List<Integer> position, final LongestLine longestLine, final String name, final String filename) {
    super(session);
    this._numLines = numLines;
    this._position = position;
    this._longestLine = longestLine;
    this._name = name;
    this._filename = filename;
  }
  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + _numLines;
    result = prime * result + ((_position== null) ? 0 : _position.hashCode());
    result = prime * result + ((_longestLine== null) ? 0 : _longestLine.hashCode());
    result = prime * result + ((_name== null) ? 0 : _name.hashCode());
    result = prime * result + ((_filename== null) ? 0 : _filename.hashCode());
    return result;
  }
  
  @Override
  public boolean equals(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    if (!super.equals(obj))
      return false;
    RepChangesetDiffImpl_Meta other = (RepChangesetDiffImpl_Meta) obj;
    if (other._numLines != _numLines)
      return false;
    if (_position == null) {
      if (other._position != null)
        return false;
    } else if (!_position.equals(other._position))
      return false;
    if (_longestLine == null) {
      if (other._longestLine != null)
        return false;
    } else if (!_longestLine.equals(other._longestLine))
      return false;
    if (_name == null) {
      if (other._name != null)
        return false;
    } else if (!_name.equals(other._name))
      return false;
    if (_filename == null) {
      if (other._filename != null)
        return false;
    } else if (!_filename.equals(other._filename))
      return false;
    return true;
  }
  
  @Override
  public String toString() {
    String result = new ToStringHelper().toString(this);
    return result;
  }
}
