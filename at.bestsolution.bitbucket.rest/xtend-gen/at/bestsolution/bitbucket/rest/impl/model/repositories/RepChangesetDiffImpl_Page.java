package at.bestsolution.bitbucket.rest.impl.model.repositories;

import at.bestsolution.bitbucket.rest.BitbucketSession;
import at.bestsolution.bitbucket.rest.impl.model.base.BaseObject;
import at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil;
import at.bestsolution.bitbucket.rest.model.repositories.RepChangesetDiff.Page;
import com.google.gson.JsonObject;
import org.eclipse.xtend.lib.Data;
import org.eclipse.xtext.xbase.lib.util.ToStringHelper;

@Data
@SuppressWarnings("all")
public class RepChangesetDiffImpl_Page extends BaseObject implements Page {
  private final String _html;
  
  public String getHtml() {
    return this._html;
  }
  
  public static Page toRepChangesetDiff_Page(final JsonObject o, final BitbucketSession session) {
    final String _html = JsonUtil.string(o, "html");
    RepChangesetDiffImpl_Page _repChangesetDiffImpl_Page = new RepChangesetDiffImpl_Page(session, _html);
    final Page rv = _repChangesetDiffImpl_Page;
    return rv;
  }
  
  public RepChangesetDiffImpl_Page(final BitbucketSession session, final String html) {
    super(session);
    this._html = html;
  }
  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + ((_html== null) ? 0 : _html.hashCode());
    return result;
  }
  
  @Override
  public boolean equals(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    if (!super.equals(obj))
      return false;
    RepChangesetDiffImpl_Page other = (RepChangesetDiffImpl_Page) obj;
    if (_html == null) {
      if (other._html != null)
        return false;
    } else if (!_html.equals(other._html))
      return false;
    return true;
  }
  
  @Override
  public String toString() {
    String result = new ToStringHelper().toString(this);
    return result;
  }
}
