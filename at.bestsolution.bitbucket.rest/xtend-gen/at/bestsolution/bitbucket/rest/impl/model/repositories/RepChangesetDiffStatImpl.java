package at.bestsolution.bitbucket.rest.impl.model.repositories;

import at.bestsolution.bitbucket.rest.BitbucketSession;
import at.bestsolution.bitbucket.rest.impl.model.base.BaseObject;
import at.bestsolution.bitbucket.rest.impl.model.repositories.RepChangesetDiffStatImpl_Diffstat;
import at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil;
import at.bestsolution.bitbucket.rest.model.repositories.RepChangesetDiffStat;
import at.bestsolution.bitbucket.rest.model.repositories.RepChangesetDiffStat.Diffstat;
import com.google.gson.JsonObject;
import org.eclipse.xtend.lib.Data;
import org.eclipse.xtext.xbase.lib.util.ToStringHelper;

@Data
@SuppressWarnings("all")
public class RepChangesetDiffStatImpl extends BaseObject implements RepChangesetDiffStat {
  private final String _type;
  
  public String getType() {
    return this._type;
  }
  
  private final String _file;
  
  public String getFile() {
    return this._file;
  }
  
  private final Diffstat _diffstat;
  
  public Diffstat getDiffstat() {
    return this._diffstat;
  }
  
  public static RepChangesetDiffStat toRepChangesetDiffStat(final JsonObject o, final BitbucketSession session) {
    final String _type = JsonUtil.string(o, "type");
    final String _file = JsonUtil.string(o, "file");
    final Diffstat _diffstat = RepChangesetDiffStatImpl_Diffstat.toRepChangesetDiffStat_Diffstat(o, session);
    RepChangesetDiffStatImpl _repChangesetDiffStatImpl = new RepChangesetDiffStatImpl(session, _type, _file, _diffstat);
    final RepChangesetDiffStat rv = _repChangesetDiffStatImpl;
    return rv;
  }
  
  public RepChangesetDiffStatImpl(final BitbucketSession session, final String type, final String file, final Diffstat diffstat) {
    super(session);
    this._type = type;
    this._file = file;
    this._diffstat = diffstat;
  }
  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + ((_type== null) ? 0 : _type.hashCode());
    result = prime * result + ((_file== null) ? 0 : _file.hashCode());
    result = prime * result + ((_diffstat== null) ? 0 : _diffstat.hashCode());
    return result;
  }
  
  @Override
  public boolean equals(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    if (!super.equals(obj))
      return false;
    RepChangesetDiffStatImpl other = (RepChangesetDiffStatImpl) obj;
    if (_type == null) {
      if (other._type != null)
        return false;
    } else if (!_type.equals(other._type))
      return false;
    if (_file == null) {
      if (other._file != null)
        return false;
    } else if (!_file.equals(other._file))
      return false;
    if (_diffstat == null) {
      if (other._diffstat != null)
        return false;
    } else if (!_diffstat.equals(other._diffstat))
      return false;
    return true;
  }
  
  @Override
  public String toString() {
    String result = new ToStringHelper().toString(this);
    return result;
  }
}
