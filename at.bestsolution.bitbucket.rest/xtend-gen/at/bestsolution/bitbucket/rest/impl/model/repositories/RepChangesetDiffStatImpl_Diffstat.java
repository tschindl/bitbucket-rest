package at.bestsolution.bitbucket.rest.impl.model.repositories;

import at.bestsolution.bitbucket.rest.BitbucketSession;
import at.bestsolution.bitbucket.rest.impl.model.base.BaseObject;
import at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil;
import at.bestsolution.bitbucket.rest.model.repositories.RepChangesetDiffStat.Diffstat;
import com.google.gson.JsonObject;
import org.eclipse.xtend.lib.Data;
import org.eclipse.xtext.xbase.lib.util.ToStringHelper;

@Data
@SuppressWarnings("all")
public class RepChangesetDiffStatImpl_Diffstat extends BaseObject implements Diffstat {
  private final Integer _removed;
  
  public Integer getRemoved() {
    return this._removed;
  }
  
  private final Integer _added;
  
  public Integer getAdded() {
    return this._added;
  }
  
  public static Diffstat toRepChangesetDiffStat_Diffstat(final JsonObject o, final BitbucketSession session) {
    final Integer _removed = JsonUtil.integerObject(o, "removed");
    final Integer _added = JsonUtil.integerObject(o, "added");
    RepChangesetDiffStatImpl_Diffstat _repChangesetDiffStatImpl_Diffstat = new RepChangesetDiffStatImpl_Diffstat(session, _removed, _added);
    final Diffstat rv = _repChangesetDiffStatImpl_Diffstat;
    return rv;
  }
  
  public RepChangesetDiffStatImpl_Diffstat(final BitbucketSession session, final Integer removed, final Integer added) {
    super(session);
    this._removed = removed;
    this._added = added;
  }
  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + ((_removed== null) ? 0 : _removed.hashCode());
    result = prime * result + ((_added== null) ? 0 : _added.hashCode());
    return result;
  }
  
  @Override
  public boolean equals(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    if (!super.equals(obj))
      return false;
    RepChangesetDiffStatImpl_Diffstat other = (RepChangesetDiffStatImpl_Diffstat) obj;
    if (_removed == null) {
      if (other._removed != null)
        return false;
    } else if (!_removed.equals(other._removed))
      return false;
    if (_added == null) {
      if (other._added != null)
        return false;
    } else if (!_added.equals(other._added))
      return false;
    return true;
  }
  
  @Override
  public String toString() {
    String result = new ToStringHelper().toString(this);
    return result;
  }
}
