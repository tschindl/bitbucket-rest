package at.bestsolution.bitbucket.rest.impl.model.repositories;

import at.bestsolution.bitbucket.rest.BitbucketSession;
import at.bestsolution.bitbucket.rest.impl.model.base.BaseObject;
import at.bestsolution.bitbucket.rest.impl.model.user.UserUserImpl;
import at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil;
import at.bestsolution.bitbucket.rest.model.repositories.RepIssueComment;
import at.bestsolution.bitbucket.rest.model.user.UserUser;
import com.google.gson.JsonObject;
import java.util.Date;
import org.eclipse.xtend.lib.Data;
import org.eclipse.xtext.xbase.lib.util.ToStringHelper;

@Data
@SuppressWarnings("all")
public class RepIssueCommentImpl extends BaseObject implements RepIssueComment {
  private final String _content;
  
  public String getContent() {
    return this._content;
  }
  
  private final UserUser _authorInfo;
  
  public UserUser getAuthorInfo() {
    return this._authorInfo;
  }
  
  private final int _commentId;
  
  public int getCommentId() {
    return this._commentId;
  }
  
  private final Date _utcUpdatedOn;
  
  public Date getUtcUpdatedOn() {
    return this._utcUpdatedOn;
  }
  
  private final Date _utcCreatedOn;
  
  public Date getUtcCreatedOn() {
    return this._utcCreatedOn;
  }
  
  private final boolean _spam;
  
  public boolean isSpam() {
    return this._spam;
  }
  
  public static RepIssueComment toRepIssueComment(final JsonObject o, final BitbucketSession session) {
    final String _content = JsonUtil.string(o, "content");
    JsonObject _jsonObject = JsonUtil.jsonObject(o, "author_info");
    final UserUser _authorId = UserUserImpl.toUserUser(_jsonObject, session);
    final int _commentId = JsonUtil.integer(o, "comment_id");
    final Date _utcUpdatedOn = JsonUtil.date(o, "utc_updated_on");
    final Date _utcCreatedOn = JsonUtil.date(o, "utc_created_on");
    final boolean _spam = JsonUtil.bool(o, "is_spam");
    RepIssueCommentImpl _repIssueCommentImpl = new RepIssueCommentImpl(session, _content, _authorId, _commentId, _utcUpdatedOn, _utcCreatedOn, _spam);
    final RepIssueComment rv = _repIssueCommentImpl;
    return rv;
  }
  
  public RepIssueCommentImpl(final BitbucketSession session, final String content, final UserUser authorInfo, final int commentId, final Date utcUpdatedOn, final Date utcCreatedOn, final boolean spam) {
    super(session);
    this._content = content;
    this._authorInfo = authorInfo;
    this._commentId = commentId;
    this._utcUpdatedOn = utcUpdatedOn;
    this._utcCreatedOn = utcCreatedOn;
    this._spam = spam;
  }
  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + ((_content== null) ? 0 : _content.hashCode());
    result = prime * result + ((_authorInfo== null) ? 0 : _authorInfo.hashCode());
    result = prime * result + _commentId;
    result = prime * result + ((_utcUpdatedOn== null) ? 0 : _utcUpdatedOn.hashCode());
    result = prime * result + ((_utcCreatedOn== null) ? 0 : _utcCreatedOn.hashCode());
    result = prime * result + (_spam ? 1231 : 1237);
    return result;
  }
  
  @Override
  public boolean equals(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    if (!super.equals(obj))
      return false;
    RepIssueCommentImpl other = (RepIssueCommentImpl) obj;
    if (_content == null) {
      if (other._content != null)
        return false;
    } else if (!_content.equals(other._content))
      return false;
    if (_authorInfo == null) {
      if (other._authorInfo != null)
        return false;
    } else if (!_authorInfo.equals(other._authorInfo))
      return false;
    if (other._commentId != _commentId)
      return false;
    if (_utcUpdatedOn == null) {
      if (other._utcUpdatedOn != null)
        return false;
    } else if (!_utcUpdatedOn.equals(other._utcUpdatedOn))
      return false;
    if (_utcCreatedOn == null) {
      if (other._utcCreatedOn != null)
        return false;
    } else if (!_utcCreatedOn.equals(other._utcCreatedOn))
      return false;
    if (other._spam != _spam)
      return false;
    return true;
  }
  
  @Override
  public String toString() {
    String result = new ToStringHelper().toString(this);
    return result;
  }
}
