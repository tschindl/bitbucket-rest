package at.bestsolution.bitbucket.rest.impl.model.repositories;

import at.bestsolution.bitbucket.rest.BitbucketSession;
import at.bestsolution.bitbucket.rest.impl.model.base.BaseObject;
import at.bestsolution.bitbucket.rest.impl.model.repositories.RepIssueImpl_Metadata;
import at.bestsolution.bitbucket.rest.impl.model.user.UserUserImpl;
import at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil;
import at.bestsolution.bitbucket.rest.model.repositories.RepIssue;
import at.bestsolution.bitbucket.rest.model.repositories.RepIssue.Metadata;
import at.bestsolution.bitbucket.rest.model.repositories.RepIssue.Priority;
import at.bestsolution.bitbucket.rest.model.repositories.RepIssue.State;
import at.bestsolution.bitbucket.rest.model.user.UserUser;
import com.google.gson.JsonObject;
import java.util.Date;
import org.eclipse.xtend.lib.Data;
import org.eclipse.xtext.xbase.lib.util.ToStringHelper;

@Data
@SuppressWarnings("all")
public class RepIssueImpl extends BaseObject implements RepIssue {
  private final State _status;
  
  public State getStatus() {
    return this._status;
  }
  
  private final Priority _priority;
  
  public Priority getPriority() {
    return this._priority;
  }
  
  private final String _title;
  
  public String getTitle() {
    return this._title;
  }
  
  private final UserUser _reportedBy;
  
  public UserUser getReportedBy() {
    return this._reportedBy;
  }
  
  private final Date _utcLastUpdated;
  
  public Date getUtcLastUpdated() {
    return this._utcLastUpdated;
  }
  
  private final int _commentCount;
  
  public int getCommentCount() {
    return this._commentCount;
  }
  
  private final Metadata _metadata;
  
  public Metadata getMetadata() {
    return this._metadata;
  }
  
  private final String _content;
  
  public String getContent() {
    return this._content;
  }
  
  private final int _localId;
  
  public int getLocalId() {
    return this._localId;
  }
  
  private final int _followerCount;
  
  public int getFollowerCount() {
    return this._followerCount;
  }
  
  private final Date _utcCreatedOn;
  
  public Date getUtcCreatedOn() {
    return this._utcCreatedOn;
  }
  
  private final String _resourceUri;
  
  public String getResourceUri() {
    return this._resourceUri;
  }
  
  private final boolean _spam;
  
  public boolean isSpam() {
    return this._spam;
  }
  
  public static RepIssue toRepIssue(final JsonObject o, final BitbucketSession session) {
    final State _status = JsonUtil.<State>enumeration(o, "status", State.class);
    final Priority _priority = JsonUtil.<Priority>enumeration(o, "priority", Priority.class);
    final String _title = JsonUtil.string(o, "title");
    JsonObject _jsonObject = JsonUtil.jsonObject(o, "reported_by");
    final UserUser _reportedBy = UserUserImpl.toUserUser(_jsonObject, session);
    final Date _utcLastUpdated = JsonUtil.date(o, "utc_last_updated");
    final int _commentCount = JsonUtil.integer(o, "comment_count");
    JsonObject _jsonObject_1 = JsonUtil.jsonObject(o, "metadata");
    final Metadata _metadata = RepIssueImpl_Metadata.toRepIssue_Metadata(_jsonObject_1, session);
    final String _content = JsonUtil.string(o, "content");
    final int _localId = JsonUtil.integer(o, "local_id");
    final int _followerCount = JsonUtil.integer(o, "follower_count");
    final Date _utcCreatedOn = JsonUtil.date(o, "utc_created_on");
    final String _resourceUri = JsonUtil.string(o, "resource_uri");
    final boolean _spam = JsonUtil.bool(o, "is_spam");
    RepIssueImpl _repIssueImpl = new RepIssueImpl(session, _status, _priority, _title, _reportedBy, _utcLastUpdated, _commentCount, _metadata, _content, _localId, _followerCount, _utcCreatedOn, _resourceUri, _spam);
    final RepIssue rv = _repIssueImpl;
    return rv;
  }
  
  public RepIssueImpl(final BitbucketSession session, final State status, final Priority priority, final String title, final UserUser reportedBy, final Date utcLastUpdated, final int commentCount, final Metadata metadata, final String content, final int localId, final int followerCount, final Date utcCreatedOn, final String resourceUri, final boolean spam) {
    super(session);
    this._status = status;
    this._priority = priority;
    this._title = title;
    this._reportedBy = reportedBy;
    this._utcLastUpdated = utcLastUpdated;
    this._commentCount = commentCount;
    this._metadata = metadata;
    this._content = content;
    this._localId = localId;
    this._followerCount = followerCount;
    this._utcCreatedOn = utcCreatedOn;
    this._resourceUri = resourceUri;
    this._spam = spam;
  }
  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + ((_status== null) ? 0 : _status.hashCode());
    result = prime * result + ((_priority== null) ? 0 : _priority.hashCode());
    result = prime * result + ((_title== null) ? 0 : _title.hashCode());
    result = prime * result + ((_reportedBy== null) ? 0 : _reportedBy.hashCode());
    result = prime * result + ((_utcLastUpdated== null) ? 0 : _utcLastUpdated.hashCode());
    result = prime * result + _commentCount;
    result = prime * result + ((_metadata== null) ? 0 : _metadata.hashCode());
    result = prime * result + ((_content== null) ? 0 : _content.hashCode());
    result = prime * result + _localId;
    result = prime * result + _followerCount;
    result = prime * result + ((_utcCreatedOn== null) ? 0 : _utcCreatedOn.hashCode());
    result = prime * result + ((_resourceUri== null) ? 0 : _resourceUri.hashCode());
    result = prime * result + (_spam ? 1231 : 1237);
    return result;
  }
  
  @Override
  public boolean equals(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    if (!super.equals(obj))
      return false;
    RepIssueImpl other = (RepIssueImpl) obj;
    if (_status == null) {
      if (other._status != null)
        return false;
    } else if (!_status.equals(other._status))
      return false;
    if (_priority == null) {
      if (other._priority != null)
        return false;
    } else if (!_priority.equals(other._priority))
      return false;
    if (_title == null) {
      if (other._title != null)
        return false;
    } else if (!_title.equals(other._title))
      return false;
    if (_reportedBy == null) {
      if (other._reportedBy != null)
        return false;
    } else if (!_reportedBy.equals(other._reportedBy))
      return false;
    if (_utcLastUpdated == null) {
      if (other._utcLastUpdated != null)
        return false;
    } else if (!_utcLastUpdated.equals(other._utcLastUpdated))
      return false;
    if (other._commentCount != _commentCount)
      return false;
    if (_metadata == null) {
      if (other._metadata != null)
        return false;
    } else if (!_metadata.equals(other._metadata))
      return false;
    if (_content == null) {
      if (other._content != null)
        return false;
    } else if (!_content.equals(other._content))
      return false;
    if (other._localId != _localId)
      return false;
    if (other._followerCount != _followerCount)
      return false;
    if (_utcCreatedOn == null) {
      if (other._utcCreatedOn != null)
        return false;
    } else if (!_utcCreatedOn.equals(other._utcCreatedOn))
      return false;
    if (_resourceUri == null) {
      if (other._resourceUri != null)
        return false;
    } else if (!_resourceUri.equals(other._resourceUri))
      return false;
    if (other._spam != _spam)
      return false;
    return true;
  }
  
  @Override
  public String toString() {
    String result = new ToStringHelper().toString(this);
    return result;
  }
}
