package at.bestsolution.bitbucket.rest.impl.model.repositories;

import at.bestsolution.bitbucket.rest.BitbucketSession;
import at.bestsolution.bitbucket.rest.impl.model.base.BaseObject;
import at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil;
import at.bestsolution.bitbucket.rest.model.repositories.RepIssue.Kind;
import at.bestsolution.bitbucket.rest.model.repositories.RepIssue.Metadata;
import com.google.gson.JsonObject;
import org.eclipse.xtend.lib.Data;
import org.eclipse.xtext.xbase.lib.util.ToStringHelper;

@Data
@SuppressWarnings("all")
public class RepIssueImpl_Metadata extends BaseObject implements Metadata {
  private final Kind _kind;
  
  public Kind getKind() {
    return this._kind;
  }
  
  private final String _version;
  
  public String getVersion() {
    return this._version;
  }
  
  private final String _component;
  
  public String getComponent() {
    return this._component;
  }
  
  private final String _milestone;
  
  public String getMilestone() {
    return this._milestone;
  }
  
  public static Metadata toRepIssue_Metadata(final JsonObject o, final BitbucketSession session) {
    final Kind _kind = JsonUtil.<Kind>enumeration(o, "kind", Kind.class);
    final String _version = JsonUtil.string(o, "version");
    final String _component = JsonUtil.string(o, "component");
    final String _milestone = JsonUtil.string(o, "milestone");
    RepIssueImpl_Metadata _repIssueImpl_Metadata = new RepIssueImpl_Metadata(session, _kind, _version, _component, _milestone);
    final Metadata rv = _repIssueImpl_Metadata;
    return rv;
  }
  
  public RepIssueImpl_Metadata(final BitbucketSession session, final Kind kind, final String version, final String component, final String milestone) {
    super(session);
    this._kind = kind;
    this._version = version;
    this._component = component;
    this._milestone = milestone;
  }
  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + ((_kind== null) ? 0 : _kind.hashCode());
    result = prime * result + ((_version== null) ? 0 : _version.hashCode());
    result = prime * result + ((_component== null) ? 0 : _component.hashCode());
    result = prime * result + ((_milestone== null) ? 0 : _milestone.hashCode());
    return result;
  }
  
  @Override
  public boolean equals(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    if (!super.equals(obj))
      return false;
    RepIssueImpl_Metadata other = (RepIssueImpl_Metadata) obj;
    if (_kind == null) {
      if (other._kind != null)
        return false;
    } else if (!_kind.equals(other._kind))
      return false;
    if (_version == null) {
      if (other._version != null)
        return false;
    } else if (!_version.equals(other._version))
      return false;
    if (_component == null) {
      if (other._component != null)
        return false;
    } else if (!_component.equals(other._component))
      return false;
    if (_milestone == null) {
      if (other._milestone != null)
        return false;
    } else if (!_milestone.equals(other._milestone))
      return false;
    return true;
  }
  
  @Override
  public String toString() {
    String result = new ToStringHelper().toString(this);
    return result;
  }
}
