package at.bestsolution.bitbucket.rest.impl.model.repositories;

import at.bestsolution.bitbucket.rest.BitbucketSession;
import at.bestsolution.bitbucket.rest.impl.model.base.BaseObject;
import at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil;
import at.bestsolution.bitbucket.rest.model.repositories.RepIssueMilestone;
import com.google.gson.JsonObject;
import org.eclipse.xtend.lib.Data;
import org.eclipse.xtext.xbase.lib.util.ToStringHelper;

@Data
@SuppressWarnings("all")
public class RepIssueMilestoneImpl extends BaseObject implements RepIssueMilestone {
  private final int _objectId;
  
  public int getObjectId() {
    return this._objectId;
  }
  
  private final String _name;
  
  public String getName() {
    return this._name;
  }
  
  public static RepIssueMilestone toRepIssueMilestone(final JsonObject o, final BitbucketSession session) {
    final int _objectId = JsonUtil.integer(o, "object_id");
    final String _name = JsonUtil.string(o, "name");
    RepIssueMilestoneImpl _repIssueMilestoneImpl = new RepIssueMilestoneImpl(session, _objectId, _name);
    final RepIssueMilestone rv = _repIssueMilestoneImpl;
    return rv;
  }
  
  public RepIssueMilestoneImpl(final BitbucketSession session, final int objectId, final String name) {
    super(session);
    this._objectId = objectId;
    this._name = name;
  }
  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + _objectId;
    result = prime * result + ((_name== null) ? 0 : _name.hashCode());
    return result;
  }
  
  @Override
  public boolean equals(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    if (!super.equals(obj))
      return false;
    RepIssueMilestoneImpl other = (RepIssueMilestoneImpl) obj;
    if (other._objectId != _objectId)
      return false;
    if (_name == null) {
      if (other._name != null)
        return false;
    } else if (!_name.equals(other._name))
      return false;
    return true;
  }
  
  @Override
  public String toString() {
    String result = new ToStringHelper().toString(this);
    return result;
  }
}
