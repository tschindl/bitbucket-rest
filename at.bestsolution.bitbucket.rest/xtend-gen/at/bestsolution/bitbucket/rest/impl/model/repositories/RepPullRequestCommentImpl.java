package at.bestsolution.bitbucket.rest.impl.model.repositories;

import at.bestsolution.bitbucket.rest.BitbucketSession;
import at.bestsolution.bitbucket.rest.impl.model.base.BaseObject;
import at.bestsolution.bitbucket.rest.impl.model.user.UserUserImpl;
import at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil;
import at.bestsolution.bitbucket.rest.model.repositories.RepPullRequestComment;
import at.bestsolution.bitbucket.rest.model.user.UserUser;
import com.google.gson.JsonObject;
import java.util.Date;
import org.eclipse.xtend.lib.Data;
import org.eclipse.xtext.xbase.lib.util.ToStringHelper;

@Data
@SuppressWarnings("all")
public class RepPullRequestCommentImpl extends BaseObject implements RepPullRequestComment {
  private final int _pullRequestId;
  
  public int getPullRequestId() {
    return this._pullRequestId;
  }
  
  private final int _commentId;
  
  public int getCommentId() {
    return this._commentId;
  }
  
  private final Integer _parentId;
  
  public Integer getParentId() {
    return this._parentId;
  }
  
  private final Date _utcLastUpdated;
  
  public Date getUtcLastUpdated() {
    return this._utcLastUpdated;
  }
  
  private final String _filenameHash;
  
  public String getFilenameHash() {
    return this._filenameHash;
  }
  
  private final String _baseRev;
  
  public String getBaseRev() {
    return this._baseRev;
  }
  
  private final String _filename;
  
  public String getFilename() {
    return this._filename;
  }
  
  private final String _content;
  
  public String getContent() {
    return this._content;
  }
  
  private final String _contentRendered;
  
  public String getContentRendered() {
    return this._contentRendered;
  }
  
  private final UserUser _authorInfo;
  
  public UserUser getAuthorInfo() {
    return this._authorInfo;
  }
  
  private final Integer _lineFrom;
  
  public Integer getLineFrom() {
    return this._lineFrom;
  }
  
  private final Integer _lineTo;
  
  public Integer getLineTo() {
    return this._lineTo;
  }
  
  private final String _destRev;
  
  public String getDestRev() {
    return this._destRev;
  }
  
  private final Date _utcCreatedOn;
  
  public Date getUtcCreatedOn() {
    return this._utcCreatedOn;
  }
  
  private final String _anchor;
  
  public String getAnchor() {
    return this._anchor;
  }
  
  private final boolean _spam;
  
  public boolean isSpam() {
    return this._spam;
  }
  
  public static RepPullRequestComment toRepPullRequestComment(final JsonObject o, final BitbucketSession session) {
    final int _pullRequestId = JsonUtil.integer(o, "pull_request_id");
    final int _commentId = JsonUtil.integer(o, "comment_id");
    final Integer _parentId = JsonUtil.integerObject(o, "parent_id");
    final Date _utcLastUpdated = JsonUtil.date(o, "utc_last_updated");
    final String _filenameHash = JsonUtil.string(o, "filename_hash");
    final String _baseRev = JsonUtil.string(o, "base_rev");
    final String _filename = JsonUtil.string(o, "filename");
    final String _content = JsonUtil.string(o, "content");
    final String _contentRendered = JsonUtil.string(o, "content_rendered");
    JsonObject _jsonObject = JsonUtil.jsonObject(o, "author_info");
    final UserUser _authorInfo = UserUserImpl.toUserUser(_jsonObject, session);
    final Integer _lineFrom = JsonUtil.integerObject(o, "line_from");
    final Integer _lineTo = JsonUtil.integerObject(o, "line_to");
    final String _destRev = JsonUtil.string(o, "dest_rev");
    final Date _utcCreatedOn = JsonUtil.date(o, "utc_created_on");
    final String _anchor = JsonUtil.string(o, "anchor");
    final boolean _spam = JsonUtil.bool(o, "spam");
    RepPullRequestCommentImpl _repPullRequestCommentImpl = new RepPullRequestCommentImpl(session, _pullRequestId, _commentId, _parentId, _utcLastUpdated, _filenameHash, _baseRev, _filename, _content, _contentRendered, _authorInfo, _lineFrom, _lineTo, _destRev, _utcCreatedOn, _anchor, _spam);
    final RepPullRequestComment rv = _repPullRequestCommentImpl;
    return rv;
  }
  
  public RepPullRequestCommentImpl(final BitbucketSession session, final int pullRequestId, final int commentId, final Integer parentId, final Date utcLastUpdated, final String filenameHash, final String baseRev, final String filename, final String content, final String contentRendered, final UserUser authorInfo, final Integer lineFrom, final Integer lineTo, final String destRev, final Date utcCreatedOn, final String anchor, final boolean spam) {
    super(session);
    this._pullRequestId = pullRequestId;
    this._commentId = commentId;
    this._parentId = parentId;
    this._utcLastUpdated = utcLastUpdated;
    this._filenameHash = filenameHash;
    this._baseRev = baseRev;
    this._filename = filename;
    this._content = content;
    this._contentRendered = contentRendered;
    this._authorInfo = authorInfo;
    this._lineFrom = lineFrom;
    this._lineTo = lineTo;
    this._destRev = destRev;
    this._utcCreatedOn = utcCreatedOn;
    this._anchor = anchor;
    this._spam = spam;
  }
  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + _pullRequestId;
    result = prime * result + _commentId;
    result = prime * result + ((_parentId== null) ? 0 : _parentId.hashCode());
    result = prime * result + ((_utcLastUpdated== null) ? 0 : _utcLastUpdated.hashCode());
    result = prime * result + ((_filenameHash== null) ? 0 : _filenameHash.hashCode());
    result = prime * result + ((_baseRev== null) ? 0 : _baseRev.hashCode());
    result = prime * result + ((_filename== null) ? 0 : _filename.hashCode());
    result = prime * result + ((_content== null) ? 0 : _content.hashCode());
    result = prime * result + ((_contentRendered== null) ? 0 : _contentRendered.hashCode());
    result = prime * result + ((_authorInfo== null) ? 0 : _authorInfo.hashCode());
    result = prime * result + ((_lineFrom== null) ? 0 : _lineFrom.hashCode());
    result = prime * result + ((_lineTo== null) ? 0 : _lineTo.hashCode());
    result = prime * result + ((_destRev== null) ? 0 : _destRev.hashCode());
    result = prime * result + ((_utcCreatedOn== null) ? 0 : _utcCreatedOn.hashCode());
    result = prime * result + ((_anchor== null) ? 0 : _anchor.hashCode());
    result = prime * result + (_spam ? 1231 : 1237);
    return result;
  }
  
  @Override
  public boolean equals(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    if (!super.equals(obj))
      return false;
    RepPullRequestCommentImpl other = (RepPullRequestCommentImpl) obj;
    if (other._pullRequestId != _pullRequestId)
      return false;
    if (other._commentId != _commentId)
      return false;
    if (_parentId == null) {
      if (other._parentId != null)
        return false;
    } else if (!_parentId.equals(other._parentId))
      return false;
    if (_utcLastUpdated == null) {
      if (other._utcLastUpdated != null)
        return false;
    } else if (!_utcLastUpdated.equals(other._utcLastUpdated))
      return false;
    if (_filenameHash == null) {
      if (other._filenameHash != null)
        return false;
    } else if (!_filenameHash.equals(other._filenameHash))
      return false;
    if (_baseRev == null) {
      if (other._baseRev != null)
        return false;
    } else if (!_baseRev.equals(other._baseRev))
      return false;
    if (_filename == null) {
      if (other._filename != null)
        return false;
    } else if (!_filename.equals(other._filename))
      return false;
    if (_content == null) {
      if (other._content != null)
        return false;
    } else if (!_content.equals(other._content))
      return false;
    if (_contentRendered == null) {
      if (other._contentRendered != null)
        return false;
    } else if (!_contentRendered.equals(other._contentRendered))
      return false;
    if (_authorInfo == null) {
      if (other._authorInfo != null)
        return false;
    } else if (!_authorInfo.equals(other._authorInfo))
      return false;
    if (_lineFrom == null) {
      if (other._lineFrom != null)
        return false;
    } else if (!_lineFrom.equals(other._lineFrom))
      return false;
    if (_lineTo == null) {
      if (other._lineTo != null)
        return false;
    } else if (!_lineTo.equals(other._lineTo))
      return false;
    if (_destRev == null) {
      if (other._destRev != null)
        return false;
    } else if (!_destRev.equals(other._destRev))
      return false;
    if (_utcCreatedOn == null) {
      if (other._utcCreatedOn != null)
        return false;
    } else if (!_utcCreatedOn.equals(other._utcCreatedOn))
      return false;
    if (_anchor == null) {
      if (other._anchor != null)
        return false;
    } else if (!_anchor.equals(other._anchor))
      return false;
    if (other._spam != _spam)
      return false;
    return true;
  }
  
  @Override
  public String toString() {
    String result = new ToStringHelper().toString(this);
    return result;
  }
}
