package at.bestsolution.bitbucket.rest.impl.model.repositories;

import at.bestsolution.bitbucket.rest.BitbucketSession;
import at.bestsolution.bitbucket.rest.impl.model.base.BaseObject;
import at.bestsolution.bitbucket.rest.impl.model.user.UserUserImpl;
import at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil;
import at.bestsolution.bitbucket.rest.model.repositories.RepPullRequestLike;
import at.bestsolution.bitbucket.rest.model.user.UserUser;
import com.google.gson.JsonObject;
import java.util.Date;
import org.eclipse.xtend.lib.Data;
import org.eclipse.xtext.xbase.lib.util.ToStringHelper;

@Data
@SuppressWarnings("all")
public class RepPullRequestLikeImpl extends BaseObject implements RepPullRequestLike {
  private final Date _utcLikedOn;
  
  public Date getUtcLikedOn() {
    return this._utcLikedOn;
  }
  
  private final UserUser _user;
  
  public UserUser getUser() {
    return this._user;
  }
  
  public static RepPullRequestLike toRepPullRequestLike(final JsonObject o, final BitbucketSession session) {
    final Date _utcLikedOn = JsonUtil.date(o, "utc_liked_on");
    final UserUser _user = UserUserImpl.toUserUser(o, session);
    RepPullRequestLikeImpl _repPullRequestLikeImpl = new RepPullRequestLikeImpl(session, _utcLikedOn, _user);
    final RepPullRequestLike rv = _repPullRequestLikeImpl;
    return rv;
  }
  
  public RepPullRequestLikeImpl(final BitbucketSession session, final Date utcLikedOn, final UserUser user) {
    super(session);
    this._utcLikedOn = utcLikedOn;
    this._user = user;
  }
  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + ((_utcLikedOn== null) ? 0 : _utcLikedOn.hashCode());
    result = prime * result + ((_user== null) ? 0 : _user.hashCode());
    return result;
  }
  
  @Override
  public boolean equals(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    if (!super.equals(obj))
      return false;
    RepPullRequestLikeImpl other = (RepPullRequestLikeImpl) obj;
    if (_utcLikedOn == null) {
      if (other._utcLikedOn != null)
        return false;
    } else if (!_utcLikedOn.equals(other._utcLikedOn))
      return false;
    if (_user == null) {
      if (other._user != null)
        return false;
    } else if (!_user.equals(other._user))
      return false;
    return true;
  }
  
  @Override
  public String toString() {
    String result = new ToStringHelper().toString(this);
    return result;
  }
}
