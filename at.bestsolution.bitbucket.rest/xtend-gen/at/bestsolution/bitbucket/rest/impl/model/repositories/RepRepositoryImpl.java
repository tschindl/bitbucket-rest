package at.bestsolution.bitbucket.rest.impl.model.repositories;

import at.bestsolution.bitbucket.rest.BitbucketSession;
import at.bestsolution.bitbucket.rest.impl.model.base.BasicRepositoryImpl;
import at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil;
import at.bestsolution.bitbucket.rest.model.repositories.RepRepository;
import com.google.gson.JsonObject;
import java.util.Date;
import org.eclipse.xtend.lib.Data;
import org.eclipse.xtext.xbase.lib.util.ToStringHelper;

@Data
@SuppressWarnings("all")
public class RepRepositoryImpl extends BasicRepositoryImpl implements RepRepository {
  private final String _scm;
  
  public String getScm() {
    return this._scm;
  }
  
  private final boolean _wiki;
  
  public boolean isWiki() {
    return this._wiki;
  }
  
  private final String _creator;
  
  public String getCreator() {
    return this._creator;
  }
  
  private final String _logo;
  
  public String getLogo() {
    return this._logo;
  }
  
  private final String _emailMailinglist;
  
  public String getEmailMailinglist() {
    return this._emailMailinglist;
  }
  
  private final boolean _mq;
  
  public boolean isMq() {
    return this._mq;
  }
  
  private final int _size;
  
  public int getSize() {
    return this._size;
  }
  
  private final boolean _readOnly;
  
  public boolean isReadOnly() {
    return this._readOnly;
  }
  
  private final String _forkOf;
  
  public String getForkOf() {
    return this._forkOf;
  }
  
  private final String _mqOf;
  
  public String getMqOf() {
    return this._mqOf;
  }
  
  private final int _followersCount;
  
  public int getFollowersCount() {
    return this._followersCount;
  }
  
  private final Date _utcCreatedOn;
  
  public Date getUtcCreatedOn() {
    return this._utcCreatedOn;
  }
  
  private final String _website;
  
  public String getWebsite() {
    return this._website;
  }
  
  private final String _description;
  
  public String getDescription() {
    return this._description;
  }
  
  private final boolean _issues;
  
  public boolean isIssues() {
    return this._issues;
  }
  
  private final boolean _fork;
  
  public boolean isFork() {
    return this._fork;
  }
  
  private final String _language;
  
  public String getLanguage() {
    return this._language;
  }
  
  private final Date _utcLastUpdated;
  
  public Date getUtcLastUpdated() {
    return this._utcLastUpdated;
  }
  
  private final boolean _emailWriters;
  
  public boolean isEmailWriters() {
    return this._emailWriters;
  }
  
  private final boolean _noPublicForks;
  
  public boolean isNoPublicForks() {
    return this._noPublicForks;
  }
  
  private final String _resourceURI;
  
  public String getResourceURI() {
    return this._resourceURI;
  }
  
  private final int _forksCount;
  
  public int getForksCount() {
    return this._forksCount;
  }
  
  private final String _state;
  
  public String getState() {
    return this._state;
  }
  
  public boolean hasIssues() {
    return this.isIssues();
  }
  
  public boolean hasWiki() {
    return this.isWiki();
  }
  
  public static RepRepository toRepRepository(final JsonObject o, final BitbucketSession session) {
    final String _name = JsonUtil.string(o, "name");
    final String _owner = JsonUtil.string(o, "owner");
    final String _slug = JsonUtil.string(o, "slug");
    final boolean _private = JsonUtil.bool(o, "is_private");
    final String _scm = JsonUtil.string(o, "scm");
    final boolean _wiki = JsonUtil.bool(o, "has_wiki");
    final String _creator = JsonUtil.string(o, "creator");
    final String _logo = JsonUtil.string(o, "logo");
    final String _emailMailinglist = JsonUtil.string(o, "email_mailinglist");
    final boolean _mq = JsonUtil.bool(o, "is_mq");
    final int _size = JsonUtil.integer(o, "size");
    final boolean _readOnly = JsonUtil.bool(o, "read_only");
    final String _forkOf = JsonUtil.string(o, "fork_of");
    final String _mqOf = JsonUtil.string(o, "mq_of");
    final int _followersCount = JsonUtil.integer(o, "followers_count");
    final Date _utcCreatedOn = JsonUtil.date(o, "utc_created_on");
    final String _website = JsonUtil.string(o, "website");
    final String _description = JsonUtil.string(o, "description");
    final boolean _issues = JsonUtil.bool(o, "has_issues");
    final boolean _fork = JsonUtil.bool(o, "is_fork");
    final String _language = JsonUtil.string(o, "language");
    final Date _utcLastUpdated = JsonUtil.date(o, "utc_last_update");
    final boolean _emailWriters = JsonUtil.bool(o, "email_writers");
    final boolean _noPublicForks = JsonUtil.bool(o, "no_public_forks");
    final String _resourceURI = JsonUtil.string(o, "resoure_uri");
    final int _forksCount = JsonUtil.integer(o, "forks_count");
    final String _state = JsonUtil.string(o, "state");
    RepRepositoryImpl _repRepositoryImpl = new RepRepositoryImpl(session, _name, _owner, _slug, _private, _scm, _wiki, _creator, _logo, _emailMailinglist, _mq, _size, _readOnly, _forkOf, _mqOf, _followersCount, _utcCreatedOn, _website, _description, _issues, _fork, _language, _utcLastUpdated, _emailWriters, _noPublicForks, _resourceURI, _forksCount, _state);
    final RepRepository rv = _repRepositoryImpl;
    return rv;
  }
  
  public RepRepositoryImpl(final BitbucketSession session, final String name, final String owner, final String slug, final boolean _private, final String scm, final boolean wiki, final String creator, final String logo, final String emailMailinglist, final boolean mq, final int size, final boolean readOnly, final String forkOf, final String mqOf, final int followersCount, final Date utcCreatedOn, final String website, final String description, final boolean issues, final boolean fork, final String language, final Date utcLastUpdated, final boolean emailWriters, final boolean noPublicForks, final String resourceURI, final int forksCount, final String state) {
    super(session, name, owner, slug, _private);
    this._scm = scm;
    this._wiki = wiki;
    this._creator = creator;
    this._logo = logo;
    this._emailMailinglist = emailMailinglist;
    this._mq = mq;
    this._size = size;
    this._readOnly = readOnly;
    this._forkOf = forkOf;
    this._mqOf = mqOf;
    this._followersCount = followersCount;
    this._utcCreatedOn = utcCreatedOn;
    this._website = website;
    this._description = description;
    this._issues = issues;
    this._fork = fork;
    this._language = language;
    this._utcLastUpdated = utcLastUpdated;
    this._emailWriters = emailWriters;
    this._noPublicForks = noPublicForks;
    this._resourceURI = resourceURI;
    this._forksCount = forksCount;
    this._state = state;
  }
  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + ((_scm== null) ? 0 : _scm.hashCode());
    result = prime * result + (_wiki ? 1231 : 1237);
    result = prime * result + ((_creator== null) ? 0 : _creator.hashCode());
    result = prime * result + ((_logo== null) ? 0 : _logo.hashCode());
    result = prime * result + ((_emailMailinglist== null) ? 0 : _emailMailinglist.hashCode());
    result = prime * result + (_mq ? 1231 : 1237);
    result = prime * result + _size;
    result = prime * result + (_readOnly ? 1231 : 1237);
    result = prime * result + ((_forkOf== null) ? 0 : _forkOf.hashCode());
    result = prime * result + ((_mqOf== null) ? 0 : _mqOf.hashCode());
    result = prime * result + _followersCount;
    result = prime * result + ((_utcCreatedOn== null) ? 0 : _utcCreatedOn.hashCode());
    result = prime * result + ((_website== null) ? 0 : _website.hashCode());
    result = prime * result + ((_description== null) ? 0 : _description.hashCode());
    result = prime * result + (_issues ? 1231 : 1237);
    result = prime * result + (_fork ? 1231 : 1237);
    result = prime * result + ((_language== null) ? 0 : _language.hashCode());
    result = prime * result + ((_utcLastUpdated== null) ? 0 : _utcLastUpdated.hashCode());
    result = prime * result + (_emailWriters ? 1231 : 1237);
    result = prime * result + (_noPublicForks ? 1231 : 1237);
    result = prime * result + ((_resourceURI== null) ? 0 : _resourceURI.hashCode());
    result = prime * result + _forksCount;
    result = prime * result + ((_state== null) ? 0 : _state.hashCode());
    return result;
  }
  
  @Override
  public boolean equals(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    if (!super.equals(obj))
      return false;
    RepRepositoryImpl other = (RepRepositoryImpl) obj;
    if (_scm == null) {
      if (other._scm != null)
        return false;
    } else if (!_scm.equals(other._scm))
      return false;
    if (other._wiki != _wiki)
      return false;
    if (_creator == null) {
      if (other._creator != null)
        return false;
    } else if (!_creator.equals(other._creator))
      return false;
    if (_logo == null) {
      if (other._logo != null)
        return false;
    } else if (!_logo.equals(other._logo))
      return false;
    if (_emailMailinglist == null) {
      if (other._emailMailinglist != null)
        return false;
    } else if (!_emailMailinglist.equals(other._emailMailinglist))
      return false;
    if (other._mq != _mq)
      return false;
    if (other._size != _size)
      return false;
    if (other._readOnly != _readOnly)
      return false;
    if (_forkOf == null) {
      if (other._forkOf != null)
        return false;
    } else if (!_forkOf.equals(other._forkOf))
      return false;
    if (_mqOf == null) {
      if (other._mqOf != null)
        return false;
    } else if (!_mqOf.equals(other._mqOf))
      return false;
    if (other._followersCount != _followersCount)
      return false;
    if (_utcCreatedOn == null) {
      if (other._utcCreatedOn != null)
        return false;
    } else if (!_utcCreatedOn.equals(other._utcCreatedOn))
      return false;
    if (_website == null) {
      if (other._website != null)
        return false;
    } else if (!_website.equals(other._website))
      return false;
    if (_description == null) {
      if (other._description != null)
        return false;
    } else if (!_description.equals(other._description))
      return false;
    if (other._issues != _issues)
      return false;
    if (other._fork != _fork)
      return false;
    if (_language == null) {
      if (other._language != null)
        return false;
    } else if (!_language.equals(other._language))
      return false;
    if (_utcLastUpdated == null) {
      if (other._utcLastUpdated != null)
        return false;
    } else if (!_utcLastUpdated.equals(other._utcLastUpdated))
      return false;
    if (other._emailWriters != _emailWriters)
      return false;
    if (other._noPublicForks != _noPublicForks)
      return false;
    if (_resourceURI == null) {
      if (other._resourceURI != null)
        return false;
    } else if (!_resourceURI.equals(other._resourceURI))
      return false;
    if (other._forksCount != _forksCount)
      return false;
    if (_state == null) {
      if (other._state != null)
        return false;
    } else if (!_state.equals(other._state))
      return false;
    return true;
  }
  
  @Override
  public String toString() {
    String result = new ToStringHelper().toString(this);
    return result;
  }
}
