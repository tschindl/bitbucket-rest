package at.bestsolution.bitbucket.rest.impl.model.repositories;

import at.bestsolution.bitbucket.rest.BitbucketSession;
import at.bestsolution.bitbucket.rest.impl.model.base.BaseObject;
import at.bestsolution.bitbucket.rest.impl.model.repositories.RepSlugTagImpl_File;
import at.bestsolution.bitbucket.rest.impl.services.util.JsonObjectConverter;
import at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil;
import at.bestsolution.bitbucket.rest.model.repositories.RepSlugTag;
import at.bestsolution.bitbucket.rest.model.repositories.RepSlugTag.File;
import com.google.gson.JsonObject;
import java.util.Date;
import java.util.List;
import org.eclipse.xtend.lib.Data;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.util.ToStringHelper;

@Data
@SuppressWarnings("all")
public class RepSlugTagImpl extends BaseObject implements RepSlugTag {
  private final String _node;
  
  public String getNode() {
    return this._node;
  }
  
  private final List<File> _files;
  
  public List<File> getFiles() {
    return this._files;
  }
  
  private final String _rawAuthor;
  
  public String getRawAuthor() {
    return this._rawAuthor;
  }
  
  private final Date _utcTimestamp;
  
  public Date getUtcTimestamp() {
    return this._utcTimestamp;
  }
  
  private final String _author;
  
  public String getAuthor() {
    return this._author;
  }
  
  private final String _rawNode;
  
  public String getRawNode() {
    return this._rawNode;
  }
  
  private final List<String> _parents;
  
  public List<String> getParents() {
    return this._parents;
  }
  
  private final String _branch;
  
  public String getBranch() {
    return this._branch;
  }
  
  private final String _message;
  
  public String getMessage() {
    return this._message;
  }
  
  private final int _revision;
  
  public int getRevision() {
    return this._revision;
  }
  
  private final int _size;
  
  public int getSize() {
    return this._size;
  }
  
  public static RepSlugTag toRepSlugTag(final JsonObject o, final BitbucketSession session) {
    final String _node = JsonUtil.string(o, "node");
    final Function1<JsonObject,File> _function = new Function1<JsonObject,File>() {
        public File apply(final JsonObject e) {
          File _repSlugTag_File = RepSlugTagImpl_File.toRepSlugTag_File(e, session);
          return _repSlugTag_File;
        }
      };
    final List<File> _files = JsonUtil.<File>list(o, "files", new JsonObjectConverter<File>() {
        public File create(JsonObject o) {
          return _function.apply(o);
        }
    });
    final String _rawAuthor = JsonUtil.string(o, "raw_author");
    final Date _utcTimestamp = JsonUtil.date(o, "utctimestamp");
    final String _author = JsonUtil.string(o, "author");
    final String _rawNode = JsonUtil.string(o, "raw_node");
    final Function1<JsonObject,String> _function_1 = new Function1<JsonObject,String>() {
        public String apply(final JsonObject e) {
          String _asString = e.getAsString();
          return _asString;
        }
      };
    final List<String> _parents = JsonUtil.<String>list(o, "parents", new JsonObjectConverter<String>() {
        public String create(JsonObject o) {
          return _function_1.apply(o);
        }
    });
    final String _branch = JsonUtil.string(o, "branch");
    final String _message = JsonUtil.string(o, "message");
    final int _revision = JsonUtil.integer(o, "revision");
    final int _size = JsonUtil.integer(o, "size");
    RepSlugTagImpl _repSlugTagImpl = new RepSlugTagImpl(session, _node, _files, _rawAuthor, _utcTimestamp, _author, _rawNode, _parents, _branch, _message, _revision, _size);
    final RepSlugTag rv = _repSlugTagImpl;
    return rv;
  }
  
  public RepSlugTagImpl(final BitbucketSession session, final String node, final List<File> files, final String rawAuthor, final Date utcTimestamp, final String author, final String rawNode, final List<String> parents, final String branch, final String message, final int revision, final int size) {
    super(session);
    this._node = node;
    this._files = files;
    this._rawAuthor = rawAuthor;
    this._utcTimestamp = utcTimestamp;
    this._author = author;
    this._rawNode = rawNode;
    this._parents = parents;
    this._branch = branch;
    this._message = message;
    this._revision = revision;
    this._size = size;
  }
  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + ((_node== null) ? 0 : _node.hashCode());
    result = prime * result + ((_files== null) ? 0 : _files.hashCode());
    result = prime * result + ((_rawAuthor== null) ? 0 : _rawAuthor.hashCode());
    result = prime * result + ((_utcTimestamp== null) ? 0 : _utcTimestamp.hashCode());
    result = prime * result + ((_author== null) ? 0 : _author.hashCode());
    result = prime * result + ((_rawNode== null) ? 0 : _rawNode.hashCode());
    result = prime * result + ((_parents== null) ? 0 : _parents.hashCode());
    result = prime * result + ((_branch== null) ? 0 : _branch.hashCode());
    result = prime * result + ((_message== null) ? 0 : _message.hashCode());
    result = prime * result + _revision;
    result = prime * result + _size;
    return result;
  }
  
  @Override
  public boolean equals(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    if (!super.equals(obj))
      return false;
    RepSlugTagImpl other = (RepSlugTagImpl) obj;
    if (_node == null) {
      if (other._node != null)
        return false;
    } else if (!_node.equals(other._node))
      return false;
    if (_files == null) {
      if (other._files != null)
        return false;
    } else if (!_files.equals(other._files))
      return false;
    if (_rawAuthor == null) {
      if (other._rawAuthor != null)
        return false;
    } else if (!_rawAuthor.equals(other._rawAuthor))
      return false;
    if (_utcTimestamp == null) {
      if (other._utcTimestamp != null)
        return false;
    } else if (!_utcTimestamp.equals(other._utcTimestamp))
      return false;
    if (_author == null) {
      if (other._author != null)
        return false;
    } else if (!_author.equals(other._author))
      return false;
    if (_rawNode == null) {
      if (other._rawNode != null)
        return false;
    } else if (!_rawNode.equals(other._rawNode))
      return false;
    if (_parents == null) {
      if (other._parents != null)
        return false;
    } else if (!_parents.equals(other._parents))
      return false;
    if (_branch == null) {
      if (other._branch != null)
        return false;
    } else if (!_branch.equals(other._branch))
      return false;
    if (_message == null) {
      if (other._message != null)
        return false;
    } else if (!_message.equals(other._message))
      return false;
    if (other._revision != _revision)
      return false;
    if (other._size != _size)
      return false;
    return true;
  }
  
  @Override
  public String toString() {
    String result = new ToStringHelper().toString(this);
    return result;
  }
}
