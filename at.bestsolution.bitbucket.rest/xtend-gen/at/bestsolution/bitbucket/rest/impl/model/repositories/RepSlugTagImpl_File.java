package at.bestsolution.bitbucket.rest.impl.model.repositories;

import at.bestsolution.bitbucket.rest.BitbucketSession;
import at.bestsolution.bitbucket.rest.impl.model.base.BaseObject;
import at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil;
import at.bestsolution.bitbucket.rest.model.repositories.RepSlugTag.File;
import com.google.gson.JsonObject;
import org.eclipse.xtend.lib.Data;
import org.eclipse.xtext.xbase.lib.util.ToStringHelper;

@Data
@SuppressWarnings("all")
public class RepSlugTagImpl_File extends BaseObject implements File {
  private final String _type;
  
  public String getType() {
    return this._type;
  }
  
  private final String _file;
  
  public String getFile() {
    return this._file;
  }
  
  public static File toRepSlugTag_File(final JsonObject o, final BitbucketSession session) {
    final String _type = JsonUtil.string(o, "type");
    final String _file = JsonUtil.string(o, "file");
    RepSlugTagImpl_File _repSlugTagImpl_File = new RepSlugTagImpl_File(session, _type, _file);
    final File rv = _repSlugTagImpl_File;
    return rv;
  }
  
  public RepSlugTagImpl_File(final BitbucketSession session, final String type, final String file) {
    super(session);
    this._type = type;
    this._file = file;
  }
  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + ((_type== null) ? 0 : _type.hashCode());
    result = prime * result + ((_file== null) ? 0 : _file.hashCode());
    return result;
  }
  
  @Override
  public boolean equals(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    if (!super.equals(obj))
      return false;
    RepSlugTagImpl_File other = (RepSlugTagImpl_File) obj;
    if (_type == null) {
      if (other._type != null)
        return false;
    } else if (!_type.equals(other._type))
      return false;
    if (_file == null) {
      if (other._file != null)
        return false;
    } else if (!_file.equals(other._file))
      return false;
    return true;
  }
  
  @Override
  public String toString() {
    String result = new ToStringHelper().toString(this);
    return result;
  }
}
