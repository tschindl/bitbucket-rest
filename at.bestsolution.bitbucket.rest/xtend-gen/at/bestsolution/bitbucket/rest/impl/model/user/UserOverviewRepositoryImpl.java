package at.bestsolution.bitbucket.rest.impl.model.user;

import at.bestsolution.bitbucket.rest.BitbucketSession;
import at.bestsolution.bitbucket.rest.impl.model.base.BasicRepositoryImpl;
import at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil;
import at.bestsolution.bitbucket.rest.model.user.UserOverviewRepository;
import com.google.gson.JsonObject;
import org.eclipse.xtend.lib.Data;
import org.eclipse.xtext.xbase.lib.util.ToStringHelper;

@Data
@SuppressWarnings("all")
public class UserOverviewRepositoryImpl extends BasicRepositoryImpl implements UserOverviewRepository {
  private final String _avatar;
  
  public String getAvatar() {
    return this._avatar;
  }
  
  public static UserOverviewRepository toUserOverviewRepository(final JsonObject o, final BitbucketSession session) {
    final String _name = JsonUtil.string(o, "name");
    final String _owner = JsonUtil.string(o, "owner");
    final String _slug = JsonUtil.string(o, "slug");
    final boolean _private = JsonUtil.bool(o, "is_private");
    final String _avatar = JsonUtil.string(o, "avatar");
    UserOverviewRepositoryImpl _userOverviewRepositoryImpl = new UserOverviewRepositoryImpl(session, _name, _owner, _slug, _private, _avatar);
    final UserOverviewRepository rv = _userOverviewRepositoryImpl;
    return rv;
  }
  
  public UserOverviewRepositoryImpl(final BitbucketSession session, final String name, final String owner, final String slug, final boolean _private, final String avatar) {
    super(session, name, owner, slug, _private);
    this._avatar = avatar;
  }
  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + ((_avatar== null) ? 0 : _avatar.hashCode());
    return result;
  }
  
  @Override
  public boolean equals(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    if (!super.equals(obj))
      return false;
    UserOverviewRepositoryImpl other = (UserOverviewRepositoryImpl) obj;
    if (_avatar == null) {
      if (other._avatar != null)
        return false;
    } else if (!_avatar.equals(other._avatar))
      return false;
    return true;
  }
  
  @Override
  public String toString() {
    String result = new ToStringHelper().toString(this);
    return result;
  }
}
