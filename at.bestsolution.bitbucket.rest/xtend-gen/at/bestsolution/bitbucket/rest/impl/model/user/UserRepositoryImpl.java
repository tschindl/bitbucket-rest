package at.bestsolution.bitbucket.rest.impl.model.user;

import at.bestsolution.bitbucket.rest.BitbucketSession;
import at.bestsolution.bitbucket.rest.impl.model.base.BasicRepositoryImpl;
import at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil;
import at.bestsolution.bitbucket.rest.model.user.UserRepository;
import com.google.gson.JsonObject;
import org.eclipse.xtend.lib.Data;
import org.eclipse.xtext.xbase.lib.util.ToStringHelper;

@Data
@SuppressWarnings("all")
public class UserRepositoryImpl extends BasicRepositoryImpl implements UserRepository {
  private final String _scm;
  
  public String getScm() {
    return this._scm;
  }
  
  public static UserRepository toUserRepository(final JsonObject o, final BitbucketSession session) {
    final String _name = JsonUtil.string(o, "name");
    final String _owner = JsonUtil.string(o, "owner");
    final String _slug = JsonUtil.string(o, "slug");
    final boolean _private = JsonUtil.bool(o, "is_private");
    final String _scm = JsonUtil.string(o, "name");
    UserRepositoryImpl _userRepositoryImpl = new UserRepositoryImpl(session, _name, _owner, _slug, _private, _scm);
    final UserRepository r = _userRepositoryImpl;
    return r;
  }
  
  public UserRepositoryImpl(final BitbucketSession session, final String name, final String owner, final String slug, final boolean _private, final String scm) {
    super(session, name, owner, slug, _private);
    this._scm = scm;
  }
  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + ((_scm== null) ? 0 : _scm.hashCode());
    return result;
  }
  
  @Override
  public boolean equals(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    if (!super.equals(obj))
      return false;
    UserRepositoryImpl other = (UserRepositoryImpl) obj;
    if (_scm == null) {
      if (other._scm != null)
        return false;
    } else if (!_scm.equals(other._scm))
      return false;
    return true;
  }
  
  @Override
  public String toString() {
    String result = new ToStringHelper().toString(this);
    return result;
  }
}
