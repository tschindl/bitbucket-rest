package at.bestsolution.bitbucket.rest.impl.model.user;

import at.bestsolution.bitbucket.rest.BitbucketSession;
import at.bestsolution.bitbucket.rest.impl.model.base.BaseObject;
import at.bestsolution.bitbucket.rest.impl.model.user.UserOverviewRepositoryImpl;
import at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil;
import at.bestsolution.bitbucket.rest.model.user.UserOverviewRepository;
import at.bestsolution.bitbucket.rest.model.user.UserRepositoryOverview;
import com.google.common.collect.ImmutableList;
import com.google.gson.JsonObject;
import java.util.List;
import org.eclipse.xtend.lib.Data;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.util.ToStringHelper;

@Data
@SuppressWarnings("all")
public class UserRepositoryOverviewImpl extends BaseObject implements UserRepositoryOverview {
  private final List<UserOverviewRepository> _updated;
  
  public List<UserOverviewRepository> getUpdated() {
    return this._updated;
  }
  
  private final List<UserOverviewRepository> _viewed;
  
  public List<UserOverviewRepository> getViewed() {
    return this._viewed;
  }
  
  public static UserRepositoryOverview toUserRepositoryOverview(final JsonObject o, final BitbucketSession session) {
    final Iterable<JsonObject> _updated = JsonUtil.<JsonObject>iterable(o, "updated");
    final Iterable<JsonObject> _viewed = JsonUtil.<JsonObject>iterable(o, "viewed");
    final Function1<JsonObject,UserOverviewRepository> _function = new Function1<JsonObject,UserOverviewRepository>() {
        public UserOverviewRepository apply(final JsonObject e) {
          UserOverviewRepository _userOverviewRepository = UserOverviewRepositoryImpl.toUserOverviewRepository(e, session);
          return _userOverviewRepository;
        }
      };
    Iterable<UserOverviewRepository> _map = IterableExtensions.<JsonObject, UserOverviewRepository>map(_updated, _function);
    List<UserOverviewRepository> _list = IterableExtensions.<UserOverviewRepository>toList(_map);
    final List<UserOverviewRepository> updatedList = ImmutableList.<UserOverviewRepository>copyOf(_list);
    final Function1<JsonObject,UserOverviewRepository> _function_1 = new Function1<JsonObject,UserOverviewRepository>() {
        public UserOverviewRepository apply(final JsonObject e) {
          UserOverviewRepository _userOverviewRepository = UserOverviewRepositoryImpl.toUserOverviewRepository(e, session);
          return _userOverviewRepository;
        }
      };
    Iterable<UserOverviewRepository> _map_1 = IterableExtensions.<JsonObject, UserOverviewRepository>map(_viewed, _function_1);
    List<UserOverviewRepository> _list_1 = IterableExtensions.<UserOverviewRepository>toList(_map_1);
    final List<UserOverviewRepository> viewedList = ImmutableList.<UserOverviewRepository>copyOf(_list_1);
    UserRepositoryOverviewImpl _userRepositoryOverviewImpl = new UserRepositoryOverviewImpl(session, updatedList, viewedList);
    final UserRepositoryOverview rv = _userRepositoryOverviewImpl;
    return rv;
  }
  
  public UserRepositoryOverviewImpl(final BitbucketSession session, final List<UserOverviewRepository> updated, final List<UserOverviewRepository> viewed) {
    super(session);
    this._updated = updated;
    this._viewed = viewed;
  }
  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + ((_updated== null) ? 0 : _updated.hashCode());
    result = prime * result + ((_viewed== null) ? 0 : _viewed.hashCode());
    return result;
  }
  
  @Override
  public boolean equals(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    if (!super.equals(obj))
      return false;
    UserRepositoryOverviewImpl other = (UserRepositoryOverviewImpl) obj;
    if (_updated == null) {
      if (other._updated != null)
        return false;
    } else if (!_updated.equals(other._updated))
      return false;
    if (_viewed == null) {
      if (other._viewed != null)
        return false;
    } else if (!_viewed.equals(other._viewed))
      return false;
    return true;
  }
  
  @Override
  public String toString() {
    String result = new ToStringHelper().toString(this);
    return result;
  }
}
