package at.bestsolution.bitbucket.rest.impl.model.user;

import at.bestsolution.bitbucket.rest.BitbucketSession;
import at.bestsolution.bitbucket.rest.impl.model.base.BaseObject;
import at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil;
import at.bestsolution.bitbucket.rest.model.user.UserUser;
import com.google.gson.JsonObject;
import org.eclipse.xtend.lib.Data;
import org.eclipse.xtext.xbase.lib.util.ToStringHelper;

@Data
@SuppressWarnings("all")
public class UserUserImpl extends BaseObject implements UserUser {
  private final String _username;
  
  public String getUsername() {
    return this._username;
  }
  
  private final String _firstName;
  
  public String getFirstName() {
    return this._firstName;
  }
  
  private final String _lastName;
  
  public String getLastName() {
    return this._lastName;
  }
  
  private final String _avatar;
  
  public String getAvatar() {
    return this._avatar;
  }
  
  private final String _resourceUri;
  
  public String getResourceUri() {
    return this._resourceUri;
  }
  
  private final boolean _team;
  
  public boolean isTeam() {
    return this._team;
  }
  
  public static UserUser toUserUser(final JsonObject o, final BitbucketSession session) {
    final String _username = JsonUtil.string(o, "username");
    final String _firstName = JsonUtil.string(o, "first_name");
    final String _lastName = JsonUtil.string(o, "last_name");
    final String _avatar = JsonUtil.string(o, "avatar");
    final String _resourceUri = JsonUtil.string(o, "resource_uri");
    final boolean _team = JsonUtil.bool(o, "is_team");
    UserUserImpl _userUserImpl = new UserUserImpl(session, _username, _firstName, _lastName, _avatar, _resourceUri, _team);
    final UserUser rv = _userUserImpl;
    return rv;
  }
  
  public UserUserImpl(final BitbucketSession session, final String username, final String firstName, final String lastName, final String avatar, final String resourceUri, final boolean team) {
    super(session);
    this._username = username;
    this._firstName = firstName;
    this._lastName = lastName;
    this._avatar = avatar;
    this._resourceUri = resourceUri;
    this._team = team;
  }
  
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + ((_username== null) ? 0 : _username.hashCode());
    result = prime * result + ((_firstName== null) ? 0 : _firstName.hashCode());
    result = prime * result + ((_lastName== null) ? 0 : _lastName.hashCode());
    result = prime * result + ((_avatar== null) ? 0 : _avatar.hashCode());
    result = prime * result + ((_resourceUri== null) ? 0 : _resourceUri.hashCode());
    result = prime * result + (_team ? 1231 : 1237);
    return result;
  }
  
  @Override
  public boolean equals(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    if (!super.equals(obj))
      return false;
    UserUserImpl other = (UserUserImpl) obj;
    if (_username == null) {
      if (other._username != null)
        return false;
    } else if (!_username.equals(other._username))
      return false;
    if (_firstName == null) {
      if (other._firstName != null)
        return false;
    } else if (!_firstName.equals(other._firstName))
      return false;
    if (_lastName == null) {
      if (other._lastName != null)
        return false;
    } else if (!_lastName.equals(other._lastName))
      return false;
    if (_avatar == null) {
      if (other._avatar != null)
        return false;
    } else if (!_avatar.equals(other._avatar))
      return false;
    if (_resourceUri == null) {
      if (other._resourceUri != null)
        return false;
    } else if (!_resourceUri.equals(other._resourceUri))
      return false;
    if (other._team != _team)
      return false;
    return true;
  }
  
  @Override
  public String toString() {
    String result = new ToStringHelper().toString(this);
    return result;
  }
}
