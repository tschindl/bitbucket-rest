package at.bestsolution.bitbucket.rest.impl.services;

import at.bestsolution.bitbucket.rest.BitbucketRepositoriesService;
import at.bestsolution.bitbucket.rest.BitbucketSession;
import at.bestsolution.bitbucket.rest.impl.BitbucketServiceImpl.ServiceEndpoint;
import at.bestsolution.bitbucket.rest.impl.BitbucketServiceImpl.ServiceResponse;
import at.bestsolution.bitbucket.rest.impl.model.repositories.RepChangesetCommentImpl;
import at.bestsolution.bitbucket.rest.impl.model.repositories.RepChangesetDiffImpl;
import at.bestsolution.bitbucket.rest.impl.model.repositories.RepChangesetDiffStatImpl;
import at.bestsolution.bitbucket.rest.impl.model.repositories.RepChangesetImpl;
import at.bestsolution.bitbucket.rest.impl.model.repositories.RepChangesetLikeImpl;
import at.bestsolution.bitbucket.rest.impl.model.repositories.RepIssueCommentImpl;
import at.bestsolution.bitbucket.rest.impl.model.repositories.RepIssueComponentImpl;
import at.bestsolution.bitbucket.rest.impl.model.repositories.RepIssueImpl;
import at.bestsolution.bitbucket.rest.impl.model.repositories.RepIssueMilestoneImpl;
import at.bestsolution.bitbucket.rest.impl.model.repositories.RepIssueVersionImpl;
import at.bestsolution.bitbucket.rest.impl.model.repositories.RepPullRequestCommentImpl;
import at.bestsolution.bitbucket.rest.impl.model.repositories.RepPullRequestLikeImpl;
import at.bestsolution.bitbucket.rest.impl.model.repositories.RepRepositoryImpl;
import at.bestsolution.bitbucket.rest.impl.model.repositories.RepSlugBranchImpl;
import at.bestsolution.bitbucket.rest.impl.model.repositories.RepSlugFileHistoryImpl;
import at.bestsolution.bitbucket.rest.impl.model.repositories.RepSlugTagImpl;
import at.bestsolution.bitbucket.rest.impl.model.user.UserUserImpl;
import at.bestsolution.bitbucket.rest.impl.services.BasicServiceImpl;
import at.bestsolution.bitbucket.rest.impl.services.util.BasicSegementedList;
import at.bestsolution.bitbucket.rest.impl.services.util.BasicSegementedList.SegmentCallback;
import at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil;
import at.bestsolution.bitbucket.rest.model.repositories.RepChangeset;
import at.bestsolution.bitbucket.rest.model.repositories.RepChangesetComment;
import at.bestsolution.bitbucket.rest.model.repositories.RepChangesetDiff;
import at.bestsolution.bitbucket.rest.model.repositories.RepChangesetDiffStat;
import at.bestsolution.bitbucket.rest.model.repositories.RepChangesetLike;
import at.bestsolution.bitbucket.rest.model.repositories.RepIssue;
import at.bestsolution.bitbucket.rest.model.repositories.RepIssueComment;
import at.bestsolution.bitbucket.rest.model.repositories.RepIssueComponent;
import at.bestsolution.bitbucket.rest.model.repositories.RepIssueMilestone;
import at.bestsolution.bitbucket.rest.model.repositories.RepIssueVersion;
import at.bestsolution.bitbucket.rest.model.repositories.RepPullRequestComment;
import at.bestsolution.bitbucket.rest.model.repositories.RepPullRequestLike;
import at.bestsolution.bitbucket.rest.model.repositories.RepRepository;
import at.bestsolution.bitbucket.rest.model.repositories.RepSlugBranch;
import at.bestsolution.bitbucket.rest.model.repositories.RepSlugFileHistory;
import at.bestsolution.bitbucket.rest.model.repositories.RepSlugTag;
import at.bestsolution.bitbucket.rest.model.user.UserUser;
import at.bestsolution.bitbucket.rest.util.ProgressMonitor;
import at.bestsolution.bitbucket.rest.util.ResponseObject;
import at.bestsolution.bitbucket.rest.util.SegmentedList;
import com.google.common.base.Objects;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.Functions.Function4;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;

@SuppressWarnings("all")
public class BitbucketRepositoriesServiceImpl extends BasicServiceImpl implements BitbucketRepositoriesService {
  private final int CHANGESET_MAX_PATCHSIZE = 50;
  
  private final int DEFAULT_SEGMENT_SIZE = 20;
  
  public BitbucketRepositoriesServiceImpl(final BitbucketSession session) {
    super(session);
  }
  
  /**
   * =========================================================================================
   * REPOSITORY APIS
   * =========================================================================================
   */
  public ResponseObject<RepRepository> getRepository(final ProgressMonitor monitor, final String slug) {
    BitbucketSession _session = this.getSession();
    String _username = _session.getUsername();
    return this.getRepository(monitor, _username, slug);
  }
  
  public ResponseObject<RepRepository> getRepository(final ProgressMonitor monitor, final String accountname, final String slug) {
    String _plus = ("repositories/" + accountname);
    String _plus_1 = (_plus + "/");
    String _plus_2 = (_plus_1 + slug);
    ServiceEndpoint _createEndpoint = this.createEndpoint(_plus_2);
    final ServiceResponse<JsonObject> r = _createEndpoint.<JsonObject>get();
    boolean _isOk = r.isOk();
    if (_isOk) {
      JsonObject _content = r.getContent();
      BitbucketSession _session = this.getSession();
      RepRepository _repRepository = RepRepositoryImpl.toRepRepository(_content, _session);
      return BasicServiceImpl.<RepRepository>createResponseObject(_repRepository);
    }
    IOException _exception = r.getException();
    return BasicServiceImpl.<RepRepository>createErrorObject(_exception);
  }
  
  /**
   * =========================================================================================
   * CHANGESET APIS
   * =========================================================================================
   */
  public ResponseObject<SegmentedList<RepChangeset>> getSegmentedChangesets(final ProgressMonitor monitor, final String accountname, final String slug) {
    return this.getSegmentedChangesets(monitor, accountname, slug, null);
  }
  
  public ResponseObject<SegmentedList<RepChangeset>> getSegmentedChangesets(final ProgressMonitor monitor, final String accountname, final String slug, final Integer segmentSize) {
    Integer segSize = segmentSize;
    boolean _equals = Objects.equal(segmentSize, null);
    if (_equals) {
      segSize = Integer.valueOf(this.DEFAULT_SEGMENT_SIZE);
    }
    final Integer fSegSize = segSize;
    String _plus = ("repositories/" + accountname);
    String _plus_1 = (_plus + "/");
    String _plus_2 = (_plus_1 + slug);
    String _plus_3 = (_plus_2 + "/changesets");
    final ServiceEndpoint endpoint = this.createEndpoint(_plus_3);
    HashMap<String,String> _hashMap = new HashMap<String,String>();
    final HashMap<String,String> p = _hashMap;
    p.put("limit", "1");
    final ServiceResponse<JsonObject> r = endpoint.<JsonObject>get(p);
    boolean _isOk = r.isOk();
    if (_isOk) {
      JsonObject _content = r.getContent();
      final int itemCount = JsonUtil.integer(_content, "count");
      boolean _equals_1 = (itemCount == 0);
      if (_equals_1) {
        SegmentedList<RepChangeset> _emptyList = BasicSegementedList.<RepChangeset>emptyList();
        return BasicServiceImpl.<SegmentedList<RepChangeset>>createResponseObject(_emptyList);
      } else {
        final Function4<ProgressMonitor,SegmentedList<RepChangeset>,Integer,Integer,List<RepChangeset>> _function = new Function4<ProgressMonitor,SegmentedList<RepChangeset>,Integer,Integer,List<RepChangeset>>() {
            public List<RepChangeset> apply(final ProgressMonitor mon, final SegmentedList<RepChangeset> l, final Integer idx, final Integer segsize) {
              boolean _equals = (idx == 0);
              if (_equals) {
                final ResponseObject<List<RepChangeset>> rv = BitbucketRepositoriesServiceImpl.this.getChangesets(mon, accountname, slug, fSegSize, null);
                return rv.value();
              } else {
                int _minus = (idx - 1);
                final List<RepChangeset> tmp = l.getSegment(mon, _minus);
                int _plus = ((fSegSize).intValue() + 1);
                RepChangeset _get = tmp.get(0);
                String _node = _get.getNode();
                final ResponseObject<List<RepChangeset>> rv_1 = BitbucketRepositoriesServiceImpl.this.getChangesets(mon, accountname, slug, Integer.valueOf(_plus), _node);
                List<RepChangeset> _value = rv_1.value();
                _value.remove(0);
                return rv_1.value();
              }
            }
          };
        SegmentedList<RepChangeset> _list = BasicSegementedList.<RepChangeset>list(itemCount, (segSize).intValue(), new SegmentCallback<RepChangeset>() {
            public List<RepChangeset> fetch(ProgressMonitor monitor,SegmentedList<RepChangeset> l,int segmentIndex,int segmentCount) {
              return _function.apply(monitor,l,segmentIndex,segmentCount);
            }
        });
        return BasicServiceImpl.<SegmentedList<RepChangeset>>createResponseObject(_list);
      }
    }
    IOException _exception = r.getException();
    return BasicServiceImpl.<SegmentedList<RepChangeset>>createErrorObject(_exception);
  }
  
  public ResponseObject<List<RepChangeset>> getChangesets(final ProgressMonitor monitor, final String accountname, final String slug) {
    return this.getChangesets(monitor, accountname, slug, null, null);
  }
  
  public ResponseObject<List<RepChangeset>> getChangesets(final ProgressMonitor monitor, final String accountname, final String slug, final Integer limit, final String start) {
    HashMap<String,String> _hashMap = new HashMap<String,String>();
    final HashMap<String,String> p = _hashMap;
    boolean _notEquals = (!Objects.equal(start, null));
    if (_notEquals) {
      p.put("start", start);
    }
    String _plus = ("repositories/" + accountname);
    String _plus_1 = (_plus + "/");
    String _plus_2 = (_plus_1 + slug);
    String _plus_3 = (_plus_2 + "/changesets");
    final ServiceEndpoint endpoint = this.createEndpoint(_plus_3);
    boolean _and = false;
    boolean _notEquals_1 = (!Objects.equal(limit, null));
    if (!_notEquals_1) {
      _and = false;
    } else {
      boolean _greaterThan = ((limit).intValue() > this.CHANGESET_MAX_PATCHSIZE);
      _and = (_notEquals_1 && _greaterThan);
    }
    if (_and) {
      String _plus_4 = (Integer.valueOf(this.CHANGESET_MAX_PATCHSIZE) + "");
      p.put("limit", _plus_4);
      ArrayList<RepChangeset> _arrayList = new ArrayList<RepChangeset>();
      final List<RepChangeset> rv = _arrayList;
      ServiceResponse<JsonObject> r = endpoint.<JsonObject>get(p);
      int iLimit = limit.intValue();
      boolean _while = true;
      while (_while) {
        boolean _isOk = r.isOk();
        if (_isOk) {
          JsonObject _content = r.getContent();
          final Iterable<JsonObject> items = JsonUtil.<JsonObject>iterable(_content, "changesets");
          final Function1<JsonObject,RepChangeset> _function = new Function1<JsonObject,RepChangeset>() {
              public RepChangeset apply(final JsonObject e) {
                BitbucketSession _session = BitbucketRepositoriesServiceImpl.this.getSession();
                RepChangeset _repChangeset = RepChangesetImpl.toRepChangeset(e, _session);
                return _repChangeset;
              }
            };
          Iterable<RepChangeset> _map = IterableExtensions.<JsonObject, RepChangeset>map(items, _function);
          List<RepChangeset> _list = IterableExtensions.<RepChangeset>toList(_map);
          final List<RepChangeset> tmp = ListExtensions.<RepChangeset>reverse(_list);
          final int length = tmp.size();
          int _size = rv.size();
          boolean _notEquals_2 = (_size != 0);
          if (_notEquals_2) {
            tmp.remove(0);
          }
          rv.addAll(tmp);
          boolean _lessThan = (length < this.CHANGESET_MAX_PATCHSIZE);
          if (_lessThan) {
            return BasicServiceImpl.<List<RepChangeset>>createResponseObject(rv);
          }
          RepChangeset _last = IterableExtensions.<RepChangeset>last(tmp);
          String _node = _last.getNode();
          p.put("start", _node);
          int _size_1 = tmp.size();
          int _minus = (iLimit - _size_1);
          iLimit = _minus;
          boolean _lessEqualsThan = (iLimit <= 0);
          if (_lessEqualsThan) {
            return BasicServiceImpl.<List<RepChangeset>>createResponseObject(rv);
          }
          boolean _lessThan_1 = (iLimit < this.CHANGESET_MAX_PATCHSIZE);
          if (_lessThan_1) {
            int _plus_5 = (iLimit + 1);
            iLimit = _plus_5;
          }
          String _plus_6 = (Integer.valueOf(iLimit) + "");
          p.put("limit", _plus_6);
          ServiceResponse<JsonObject> _get = endpoint.<JsonObject>get(p);
          r = _get;
        } else {
          IOException _exception = r.getException();
          return BasicServiceImpl.<List<RepChangeset>>createErrorObject(_exception);
        }
        _while = true;
      }
      return BasicServiceImpl.<List<RepChangeset>>createResponseObject(rv);
    } else {
      boolean _notEquals_2 = (!Objects.equal(limit, null));
      if (_notEquals_2) {
        String _string = limit.toString();
        p.put("limit", _string);
      }
      final ServiceResponse<JsonObject> r_1 = endpoint.<JsonObject>get(p);
      boolean _isOk = r_1.isOk();
      if (_isOk) {
        JsonObject _content = r_1.getContent();
        final Iterable<JsonObject> items = JsonUtil.<JsonObject>iterable(_content, "changesets");
        final Function1<JsonObject,RepChangeset> _function = new Function1<JsonObject,RepChangeset>() {
            public RepChangeset apply(final JsonObject e) {
              BitbucketSession _session = BitbucketRepositoriesServiceImpl.this.getSession();
              RepChangeset _repChangeset = RepChangesetImpl.toRepChangeset(e, _session);
              return _repChangeset;
            }
          };
        Iterable<RepChangeset> _map = IterableExtensions.<JsonObject, RepChangeset>map(items, _function);
        List<RepChangeset> _list = IterableExtensions.<RepChangeset>toList(_map);
        List<RepChangeset> _reverse = ListExtensions.<RepChangeset>reverse(_list);
        return BasicServiceImpl.<List<RepChangeset>>createResponseObject(_reverse);
      }
      IOException _exception = r_1.getException();
      return BasicServiceImpl.<List<RepChangeset>>createErrorObject(_exception);
    }
  }
  
  public ResponseObject<RepChangeset> getChangeset(final ProgressMonitor monitor, final String accountname, final String slug, final String rawNode) {
    String _plus = ("repositories/" + accountname);
    String _plus_1 = (_plus + "/");
    String _plus_2 = (_plus_1 + slug);
    String _plus_3 = (_plus_2 + "/changesets/");
    String _plus_4 = (_plus_3 + rawNode);
    final ServiceEndpoint endpoint = this.createEndpoint(_plus_4);
    ServiceResponse<JsonObject> r = endpoint.<JsonObject>get();
    boolean _isOk = r.isOk();
    if (_isOk) {
      JsonObject _content = r.getContent();
      BitbucketSession _session = this.getSession();
      RepChangeset _repChangeset = RepChangesetImpl.toRepChangeset(_content, _session);
      return BasicServiceImpl.<RepChangeset>createResponseObject(_repChangeset);
    }
    IOException _exception = r.getException();
    return BasicServiceImpl.<RepChangeset>createErrorObject(_exception);
  }
  
  public ResponseObject<List<RepChangesetDiff>> getChangesetDiffs(final ProgressMonitor monitor, final String accountname, final String slug, final String rawNode) {
    String _plus = ("repositories/" + accountname);
    String _plus_1 = (_plus + "/");
    String _plus_2 = (_plus_1 + slug);
    String _plus_3 = (_plus_2 + "/changesets/");
    String _plus_4 = (_plus_3 + rawNode);
    String _plus_5 = (_plus_4 + "/diff");
    final ServiceEndpoint endpoint = this.createEndpoint(_plus_5);
    ServiceResponse<JsonObject> r = endpoint.<JsonObject>get();
    boolean _isOk = r.isOk();
    if (_isOk) {
      JsonObject _content = r.getContent();
      JsonArray _asJsonArray = _content.getAsJsonArray();
      Iterable<JsonObject> diffs = JsonUtil.<JsonObject>iterable(_asJsonArray);
      final Function1<JsonObject,RepChangesetDiff> _function = new Function1<JsonObject,RepChangesetDiff>() {
          public RepChangesetDiff apply(final JsonObject e) {
            BitbucketSession _session = BitbucketRepositoriesServiceImpl.this.getSession();
            RepChangesetDiff _repChangesetDiff = RepChangesetDiffImpl.toRepChangesetDiff(e, _session);
            return _repChangesetDiff;
          }
        };
      Iterable<RepChangesetDiff> _map = IterableExtensions.<JsonObject, RepChangesetDiff>map(diffs, _function);
      List<RepChangesetDiff> _list = IterableExtensions.<RepChangesetDiff>toList(_map);
      return BasicServiceImpl.<List<RepChangesetDiff>>createResponseObject(_list);
    }
    IOException _exception = r.getException();
    return BasicServiceImpl.<List<RepChangesetDiff>>createErrorObject(_exception);
  }
  
  public ResponseObject<RepChangesetLike> getChangesetLike(final ProgressMonitor monitor, final String accountname, final String slug, final String rawNode, final String likeraccount) {
    String _plus = ("repositories/" + accountname);
    String _plus_1 = (_plus + "/");
    String _plus_2 = (_plus_1 + slug);
    String _plus_3 = (_plus_2 + "/changesets/");
    String _plus_4 = (_plus_3 + rawNode);
    String _plus_5 = (_plus_4 + "/likes/");
    String _plus_6 = (_plus_5 + likeraccount);
    final ServiceEndpoint endpoint = this.createEndpoint(_plus_6);
    ServiceResponse<JsonObject> r = endpoint.<JsonObject>get();
    boolean _isOk = r.isOk();
    if (_isOk) {
      JsonObject _content = r.getContent();
      BitbucketSession _session = this.getSession();
      RepChangesetLike _repChangesetLike = RepChangesetLikeImpl.toRepChangesetLike(_content, _session);
      return BasicServiceImpl.<RepChangesetLike>createResponseObject(_repChangesetLike);
    }
    IOException _exception = r.getException();
    return BasicServiceImpl.<RepChangesetLike>createErrorObject(_exception);
  }
  
  public ResponseObject<List<RepChangesetLike>> getChangesetLikes(final ProgressMonitor monitor, final String accountname, final String slug, final String rawNode) {
    String _plus = ("repositories/" + accountname);
    String _plus_1 = (_plus + "/");
    String _plus_2 = (_plus_1 + slug);
    String _plus_3 = (_plus_2 + "/changesets/");
    String _plus_4 = (_plus_3 + rawNode);
    String _plus_5 = (_plus_4 + "/likes");
    final ServiceEndpoint endpoint = this.createEndpoint(_plus_5);
    ServiceResponse<JsonObject> r = endpoint.<JsonObject>get();
    boolean _isOk = r.isOk();
    if (_isOk) {
      JsonObject _content = r.getContent();
      JsonArray _asJsonArray = _content.getAsJsonArray();
      Iterable<JsonObject> likes = JsonUtil.<JsonObject>iterable(_asJsonArray);
      final Function1<JsonObject,RepChangesetLike> _function = new Function1<JsonObject,RepChangesetLike>() {
          public RepChangesetLike apply(final JsonObject e) {
            BitbucketSession _session = BitbucketRepositoriesServiceImpl.this.getSession();
            RepChangesetLike _repChangesetLike = RepChangesetLikeImpl.toRepChangesetLike(e, _session);
            return _repChangesetLike;
          }
        };
      Iterable<RepChangesetLike> _map = IterableExtensions.<JsonObject, RepChangesetLike>map(likes, _function);
      List<RepChangesetLike> _list = IterableExtensions.<RepChangesetLike>toList(_map);
      return BasicServiceImpl.<List<RepChangesetLike>>createResponseObject(_list);
    }
    IOException _exception = r.getException();
    return BasicServiceImpl.<List<RepChangesetLike>>createErrorObject(_exception);
  }
  
  public ResponseObject<List<RepChangesetDiffStat>> getChangesetStats(final ProgressMonitor monitor, final String accountname, final String slug, final String rawNode) {
    String _plus = ("repositories/" + accountname);
    String _plus_1 = (_plus + "/");
    String _plus_2 = (_plus_1 + slug);
    String _plus_3 = (_plus_2 + "/changesets/");
    String _plus_4 = (_plus_3 + rawNode);
    String _plus_5 = (_plus_4 + "/diffstat");
    final ServiceEndpoint endpoint = this.createEndpoint(_plus_5);
    ServiceResponse<JsonObject> r = endpoint.<JsonObject>get();
    boolean _isOk = r.isOk();
    if (_isOk) {
      JsonObject _content = r.getContent();
      JsonArray _asJsonArray = _content.getAsJsonArray();
      Iterable<JsonObject> stats = JsonUtil.<JsonObject>iterable(_asJsonArray);
      final Function1<JsonObject,RepChangesetDiffStat> _function = new Function1<JsonObject,RepChangesetDiffStat>() {
          public RepChangesetDiffStat apply(final JsonObject e) {
            BitbucketSession _session = BitbucketRepositoriesServiceImpl.this.getSession();
            RepChangesetDiffStat _repChangesetDiffStat = RepChangesetDiffStatImpl.toRepChangesetDiffStat(e, _session);
            return _repChangesetDiffStat;
          }
        };
      Iterable<RepChangesetDiffStat> _map = IterableExtensions.<JsonObject, RepChangesetDiffStat>map(stats, _function);
      List<RepChangesetDiffStat> _list = IterableExtensions.<RepChangesetDiffStat>toList(_map);
      return BasicServiceImpl.<List<RepChangesetDiffStat>>createResponseObject(_list);
    }
    IOException _exception = r.getException();
    return BasicServiceImpl.<List<RepChangesetDiffStat>>createErrorObject(_exception);
  }
  
  public ResponseObject<List<RepChangesetComment>> getChangesetComments(final ProgressMonitor monitor, final String accountname, final String slug, final String rawNode) {
    String _plus = ("repositories/" + accountname);
    String _plus_1 = (_plus + "/");
    String _plus_2 = (_plus_1 + slug);
    String _plus_3 = (_plus_2 + "/changesets/");
    String _plus_4 = (_plus_3 + rawNode);
    String _plus_5 = (_plus_4 + "/comments");
    final ServiceEndpoint endpoint = this.createEndpoint(_plus_5);
    ServiceResponse<JsonObject> r = endpoint.<JsonObject>get();
    boolean _isOk = r.isOk();
    if (_isOk) {
      JsonObject _content = r.getContent();
      JsonArray _asJsonArray = _content.getAsJsonArray();
      Iterable<JsonObject> comments = JsonUtil.<JsonObject>iterable(_asJsonArray);
      final Function1<JsonObject,RepChangesetComment> _function = new Function1<JsonObject,RepChangesetComment>() {
          public RepChangesetComment apply(final JsonObject e) {
            BitbucketSession _session = BitbucketRepositoriesServiceImpl.this.getSession();
            RepChangesetComment _repChangesetComment = RepChangesetCommentImpl.toRepChangesetComment(e, _session);
            return _repChangesetComment;
          }
        };
      Iterable<RepChangesetComment> _map = IterableExtensions.<JsonObject, RepChangesetComment>map(comments, _function);
      List<RepChangesetComment> _list = IterableExtensions.<RepChangesetComment>toList(_map);
      return BasicServiceImpl.<List<RepChangesetComment>>createResponseObject(_list);
    }
    IOException _exception = r.getException();
    return BasicServiceImpl.<List<RepChangesetComment>>createErrorObject(_exception);
  }
  
  /**
   * =========================================================================================
   * ISSUE APIS
   * =========================================================================================
   */
  public ResponseObject<SegmentedList<RepIssue>> getSegmentedIssues(final ProgressMonitor monitor, final String accountname, final String slug) {
    return this.getSegmentedIssues(monitor, accountname, slug, null, null);
  }
  
  public ResponseObject<SegmentedList<RepIssue>> getSegmentedIssues(final ProgressMonitor monitor, final String accountname, final String slug, final Integer segmentSize, final Integer start) {
    Integer segSize = segmentSize;
    boolean _equals = Objects.equal(segmentSize, null);
    if (_equals) {
      segSize = Integer.valueOf(this.DEFAULT_SEGMENT_SIZE);
    }
    final Integer fSegSize = segSize;
    String _plus = ("repositories/" + accountname);
    String _plus_1 = (_plus + "/");
    String _plus_2 = (_plus_1 + slug);
    String _plus_3 = (_plus_2 + "/issues");
    final ServiceEndpoint endpoint = this.createEndpoint(_plus_3);
    HashMap<String,String> _hashMap = new HashMap<String,String>();
    final HashMap<String,String> p = _hashMap;
    p.put("limit", "1");
    final ServiceResponse<JsonObject> r = endpoint.<JsonObject>get(p);
    boolean _isOk = r.isOk();
    if (_isOk) {
      JsonObject _content = r.getContent();
      final int itemCount = JsonUtil.integer(_content, "count");
      boolean _equals_1 = (itemCount == 0);
      if (_equals_1) {
        SegmentedList<RepIssue> _emptyList = BasicSegementedList.<RepIssue>emptyList();
        return BasicServiceImpl.<SegmentedList<RepIssue>>createResponseObject(_emptyList);
      } else {
        final Function4<ProgressMonitor,SegmentedList<RepIssue>,Integer,Integer,List<RepIssue>> _function = new Function4<ProgressMonitor,SegmentedList<RepIssue>,Integer,Integer,List<RepIssue>>() {
            public List<RepIssue> apply(final ProgressMonitor mon, final SegmentedList<RepIssue> l, final Integer idx, final Integer segsize) {
              boolean _equals = (idx == 0);
              if (_equals) {
                final ResponseObject<List<RepIssue>> rv = BitbucketRepositoriesServiceImpl.this.getIssues(mon, accountname, slug, fSegSize, null);
                return rv.value();
              } else {
                int _multiply = (idx * (fSegSize).intValue());
                final ResponseObject<List<RepIssue>> rv_1 = BitbucketRepositoriesServiceImpl.this.getIssues(mon, accountname, slug, fSegSize, Integer.valueOf(_multiply));
                return rv_1.value();
              }
            }
          };
        SegmentedList<RepIssue> _list = BasicSegementedList.<RepIssue>list(itemCount, (segSize).intValue(), new SegmentCallback<RepIssue>() {
            public List<RepIssue> fetch(ProgressMonitor monitor,SegmentedList<RepIssue> l,int segmentIndex,int segmentCount) {
              return _function.apply(monitor,l,segmentIndex,segmentCount);
            }
        });
        return BasicServiceImpl.<SegmentedList<RepIssue>>createResponseObject(_list);
      }
    }
    IOException _exception = r.getException();
    return BasicServiceImpl.<SegmentedList<RepIssue>>createErrorObject(_exception);
  }
  
  public ResponseObject<List<RepIssue>> getIssues(final ProgressMonitor monitor, final String accountname, final String slug) {
    return this.getIssues(monitor, accountname, slug, null, null);
  }
  
  public ResponseObject<List<RepIssue>> getIssues(final ProgressMonitor monitor, final String accountname, final String slug, final Integer limit, final Integer start) {
    HashMap<String,String> _hashMap = new HashMap<String,String>();
    final HashMap<String,String> p = _hashMap;
    boolean _notEquals = (!Objects.equal(start, null));
    if (_notEquals) {
      String _string = start.toString();
      p.put("start", _string);
    }
    String _plus = ("repositories/" + accountname);
    String _plus_1 = (_plus + "/");
    String _plus_2 = (_plus_1 + slug);
    String _plus_3 = (_plus_2 + "/issues");
    final ServiceEndpoint endpoint = this.createEndpoint(_plus_3);
    boolean _and = false;
    boolean _notEquals_1 = (!Objects.equal(limit, null));
    if (!_notEquals_1) {
      _and = false;
    } else {
      boolean _greaterThan = ((limit).intValue() > this.CHANGESET_MAX_PATCHSIZE);
      _and = (_notEquals_1 && _greaterThan);
    }
    if (_and) {
      String _plus_4 = (Integer.valueOf(this.CHANGESET_MAX_PATCHSIZE) + "");
      p.put("limit", _plus_4);
      ArrayList<RepIssue> _arrayList = new ArrayList<RepIssue>();
      final List<RepIssue> rv = _arrayList;
      ServiceResponse<JsonObject> r = endpoint.<JsonObject>get(p);
      int iLimit = limit.intValue();
      boolean _while = true;
      while (_while) {
        boolean _isOk = r.isOk();
        if (_isOk) {
          JsonObject _content = r.getContent();
          final Iterable<JsonObject> items = JsonUtil.<JsonObject>iterable(_content, "issues");
          final Function1<JsonObject,RepIssue> _function = new Function1<JsonObject,RepIssue>() {
              public RepIssue apply(final JsonObject e) {
                BitbucketSession _session = BitbucketRepositoriesServiceImpl.this.getSession();
                RepIssue _repIssue = RepIssueImpl.toRepIssue(e, _session);
                return _repIssue;
              }
            };
          Iterable<RepIssue> _map = IterableExtensions.<JsonObject, RepIssue>map(items, _function);
          final List<RepIssue> tmp = IterableExtensions.<RepIssue>toList(_map);
          final int length = tmp.size();
          rv.addAll(tmp);
          boolean _lessThan = (length < this.CHANGESET_MAX_PATCHSIZE);
          if (_lessThan) {
            return BasicServiceImpl.<List<RepIssue>>createResponseObject(rv);
          }
          int _size = rv.size();
          String _string_1 = Integer.valueOf(_size).toString();
          p.put("start", _string_1);
          int _size_1 = tmp.size();
          int _minus = (iLimit - _size_1);
          iLimit = _minus;
          boolean _lessEqualsThan = (iLimit <= 0);
          if (_lessEqualsThan) {
            return BasicServiceImpl.<List<RepIssue>>createResponseObject(rv);
          }
          String _plus_5 = (Integer.valueOf(iLimit) + "");
          p.put("limit", _plus_5);
          ServiceResponse<JsonObject> _get = endpoint.<JsonObject>get(p);
          r = _get;
        } else {
          IOException _exception = r.getException();
          return BasicServiceImpl.<List<RepIssue>>createErrorObject(_exception);
        }
        _while = true;
      }
      return BasicServiceImpl.<List<RepIssue>>createResponseObject(rv);
    } else {
      boolean _notEquals_2 = (!Objects.equal(limit, null));
      if (_notEquals_2) {
        String _string_1 = limit.toString();
        p.put("limit", _string_1);
      }
      final ServiceResponse<JsonObject> r_1 = endpoint.<JsonObject>get(p);
      boolean _isOk = r_1.isOk();
      if (_isOk) {
        JsonObject _content = r_1.getContent();
        final Iterable<JsonObject> items = JsonUtil.<JsonObject>iterable(_content, "issues");
        final Function1<JsonObject,RepIssue> _function = new Function1<JsonObject,RepIssue>() {
            public RepIssue apply(final JsonObject e) {
              BitbucketSession _session = BitbucketRepositoriesServiceImpl.this.getSession();
              RepIssue _repIssue = RepIssueImpl.toRepIssue(e, _session);
              return _repIssue;
            }
          };
        Iterable<RepIssue> _map = IterableExtensions.<JsonObject, RepIssue>map(items, _function);
        List<RepIssue> _list = IterableExtensions.<RepIssue>toList(_map);
        return BasicServiceImpl.<List<RepIssue>>createResponseObject(_list);
      }
      IOException _exception = r_1.getException();
      return BasicServiceImpl.<List<RepIssue>>createErrorObject(_exception);
    }
  }
  
  public ResponseObject<RepIssue> getIssue(final ProgressMonitor monitor, final String accountname, final String slug, final int issueId) {
    String _plus = ("repositories/" + accountname);
    String _plus_1 = (_plus + "/");
    String _plus_2 = (_plus_1 + slug);
    String _plus_3 = (_plus_2 + "/issues/");
    String _plus_4 = (_plus_3 + Integer.valueOf(issueId));
    final ServiceEndpoint endpoint = this.createEndpoint(_plus_4);
    final ServiceResponse<JsonObject> r = endpoint.<JsonObject>get();
    boolean _isOk = r.isOk();
    if (_isOk) {
      JsonObject _content = r.getContent();
      BitbucketSession _session = this.getSession();
      RepIssue _repIssue = RepIssueImpl.toRepIssue(_content, _session);
      return BasicServiceImpl.<RepIssue>createResponseObject(_repIssue);
    }
    IOException _exception = r.getException();
    return BasicServiceImpl.<RepIssue>createErrorObject(_exception);
  }
  
  public ResponseObject<RepIssueComment> getIssueComment(final ProgressMonitor monitor, final String accountname, final String slug, final int issueId, final int commentId) {
    String _plus = ("repositories/" + accountname);
    String _plus_1 = (_plus + "/");
    String _plus_2 = (_plus_1 + slug);
    String _plus_3 = (_plus_2 + "/issues/");
    String _plus_4 = (_plus_3 + Integer.valueOf(issueId));
    String _plus_5 = (_plus_4 + "/comments/");
    String _plus_6 = (_plus_5 + Integer.valueOf(commentId));
    final ServiceEndpoint endpoint = this.createEndpoint(_plus_6);
    final ServiceResponse<JsonObject> r = endpoint.<JsonObject>get();
    boolean _isOk = r.isOk();
    if (_isOk) {
      JsonObject _content = r.getContent();
      BitbucketSession _session = this.getSession();
      RepIssueComment _repIssueComment = RepIssueCommentImpl.toRepIssueComment(_content, _session);
      return BasicServiceImpl.<RepIssueComment>createResponseObject(_repIssueComment);
    }
    IOException _exception = r.getException();
    return BasicServiceImpl.<RepIssueComment>createErrorObject(_exception);
  }
  
  public ResponseObject<List<RepIssueComment>> getIssueComments(final ProgressMonitor monitor, final String accountname, final String slug, final int issueId) {
    String _plus = ("repositories/" + accountname);
    String _plus_1 = (_plus + "/");
    String _plus_2 = (_plus_1 + slug);
    String _plus_3 = (_plus_2 + "/issues/");
    String _plus_4 = (_plus_3 + Integer.valueOf(issueId));
    String _plus_5 = (_plus_4 + "/comments");
    final ServiceEndpoint endpoint = this.createEndpoint(_plus_5);
    final ServiceResponse<JsonObject> r = endpoint.<JsonObject>get();
    boolean _isOk = r.isOk();
    if (_isOk) {
      JsonObject _content = r.getContent();
      JsonArray _asJsonArray = _content.getAsJsonArray();
      final Iterable<JsonObject> comments = JsonUtil.<JsonObject>iterable(_asJsonArray);
      final Function1<JsonObject,RepIssueComment> _function = new Function1<JsonObject,RepIssueComment>() {
          public RepIssueComment apply(final JsonObject e) {
            BitbucketSession _session = BitbucketRepositoriesServiceImpl.this.getSession();
            RepIssueComment _repIssueComment = RepIssueCommentImpl.toRepIssueComment(e, _session);
            return _repIssueComment;
          }
        };
      Iterable<RepIssueComment> _map = IterableExtensions.<JsonObject, RepIssueComment>map(comments, _function);
      List<RepIssueComment> _list = IterableExtensions.<RepIssueComment>toList(_map);
      return BasicServiceImpl.<List<RepIssueComment>>createResponseObject(_list);
    }
    IOException _exception = r.getException();
    return BasicServiceImpl.<List<RepIssueComment>>createErrorObject(_exception);
  }
  
  public ResponseObject<RepIssueComponent> getIssueComponent(final ProgressMonitor monitor, final String accountname, final String slug, final int id) {
    String _plus = ("repositories/" + accountname);
    String _plus_1 = (_plus + "/");
    String _plus_2 = (_plus_1 + slug);
    String _plus_3 = (_plus_2 + "/issues/components/");
    String _plus_4 = (_plus_3 + Integer.valueOf(id));
    final ServiceEndpoint endpoint = this.createEndpoint(_plus_4);
    final ServiceResponse<JsonObject> r = endpoint.<JsonObject>get();
    boolean _isOk = r.isOk();
    if (_isOk) {
      JsonObject _content = r.getContent();
      BitbucketSession _session = this.getSession();
      RepIssueComponent _repIssueComponent = RepIssueComponentImpl.toRepIssueComponent(_content, _session);
      return BasicServiceImpl.<RepIssueComponent>createResponseObject(_repIssueComponent);
    }
    IOException _exception = r.getException();
    return BasicServiceImpl.<RepIssueComponent>createErrorObject(_exception);
  }
  
  public ResponseObject<List<RepIssueComponent>> getIssueComponents(final ProgressMonitor monitor, final String accountname, final String slug) {
    String _plus = ("repositories/" + accountname);
    String _plus_1 = (_plus + "/");
    String _plus_2 = (_plus_1 + slug);
    String _plus_3 = (_plus_2 + "/issues/components");
    final ServiceEndpoint endpoint = this.createEndpoint(_plus_3);
    final ServiceResponse<JsonObject> r = endpoint.<JsonObject>get();
    boolean _isOk = r.isOk();
    if (_isOk) {
      JsonObject _content = r.getContent();
      JsonArray _asJsonArray = _content.getAsJsonArray();
      final Iterable<JsonObject> components = JsonUtil.<JsonObject>iterable(_asJsonArray);
      final Function1<JsonObject,RepIssueComponent> _function = new Function1<JsonObject,RepIssueComponent>() {
          public RepIssueComponent apply(final JsonObject e) {
            BitbucketSession _session = BitbucketRepositoriesServiceImpl.this.getSession();
            RepIssueComponent _repIssueComponent = RepIssueComponentImpl.toRepIssueComponent(e, _session);
            return _repIssueComponent;
          }
        };
      Iterable<RepIssueComponent> _map = IterableExtensions.<JsonObject, RepIssueComponent>map(components, _function);
      List<RepIssueComponent> _list = IterableExtensions.<RepIssueComponent>toList(_map);
      return BasicServiceImpl.<List<RepIssueComponent>>createResponseObject(_list);
    }
    IOException _exception = r.getException();
    return BasicServiceImpl.<List<RepIssueComponent>>createErrorObject(_exception);
  }
  
  public ResponseObject<List<UserUser>> getIssueFollowers(final ProgressMonitor monitor, final String accountname, final String slug, final int issueId) {
    String _plus = ("repositories/" + accountname);
    String _plus_1 = (_plus + "/");
    String _plus_2 = (_plus_1 + slug);
    String _plus_3 = (_plus_2 + "/issues/");
    String _plus_4 = (_plus_3 + Integer.valueOf(issueId));
    String _plus_5 = (_plus_4 + "/follower");
    final ServiceEndpoint endpoint = this.createEndpoint(_plus_5);
    final ServiceResponse<JsonObject> r = endpoint.<JsonObject>get();
    boolean _isOk = r.isOk();
    if (_isOk) {
      JsonObject _content = r.getContent();
      final Iterable<JsonObject> followers = JsonUtil.<JsonObject>iterable(_content, "followers");
      final Function1<JsonObject,UserUser> _function = new Function1<JsonObject,UserUser>() {
          public UserUser apply(final JsonObject e) {
            BitbucketSession _session = BitbucketRepositoriesServiceImpl.this.getSession();
            UserUser _userUser = UserUserImpl.toUserUser(e, _session);
            return _userUser;
          }
        };
      Iterable<UserUser> _map = IterableExtensions.<JsonObject, UserUser>map(followers, _function);
      List<UserUser> _list = IterableExtensions.<UserUser>toList(_map);
      return BasicServiceImpl.<List<UserUser>>createResponseObject(_list);
    }
    IOException _exception = r.getException();
    return BasicServiceImpl.<List<UserUser>>createErrorObject(_exception);
  }
  
  public ResponseObject<RepIssueMilestone> getIssueMilestone(final ProgressMonitor monitor, final String accountname, final String slug, final int id) {
    String _plus = ("repositories/" + accountname);
    String _plus_1 = (_plus + "/");
    String _plus_2 = (_plus_1 + slug);
    String _plus_3 = (_plus_2 + "/issues/milestones/");
    String _plus_4 = (_plus_3 + Integer.valueOf(id));
    final ServiceEndpoint endpoint = this.createEndpoint(_plus_4);
    final ServiceResponse<JsonObject> r = endpoint.<JsonObject>get();
    boolean _isOk = r.isOk();
    if (_isOk) {
      JsonObject _content = r.getContent();
      BitbucketSession _session = this.getSession();
      RepIssueMilestone _repIssueMilestone = RepIssueMilestoneImpl.toRepIssueMilestone(_content, _session);
      return BasicServiceImpl.<RepIssueMilestone>createResponseObject(_repIssueMilestone);
    }
    IOException _exception = r.getException();
    return BasicServiceImpl.<RepIssueMilestone>createErrorObject(_exception);
  }
  
  public ResponseObject<List<RepIssueMilestone>> getIssueMilestones(final ProgressMonitor monitor, final String accountname, final String slug) {
    String _plus = ("repositories/" + accountname);
    String _plus_1 = (_plus + "/");
    String _plus_2 = (_plus_1 + slug);
    String _plus_3 = (_plus_2 + "/issues/milestones");
    final ServiceEndpoint endpoint = this.createEndpoint(_plus_3);
    final ServiceResponse<JsonObject> r = endpoint.<JsonObject>get();
    boolean _isOk = r.isOk();
    if (_isOk) {
      JsonObject _content = r.getContent();
      JsonArray _asJsonArray = _content.getAsJsonArray();
      final Iterable<JsonObject> components = JsonUtil.<JsonObject>iterable(_asJsonArray);
      final Function1<JsonObject,RepIssueMilestone> _function = new Function1<JsonObject,RepIssueMilestone>() {
          public RepIssueMilestone apply(final JsonObject e) {
            BitbucketSession _session = BitbucketRepositoriesServiceImpl.this.getSession();
            RepIssueMilestone _repIssueMilestone = RepIssueMilestoneImpl.toRepIssueMilestone(e, _session);
            return _repIssueMilestone;
          }
        };
      Iterable<RepIssueMilestone> _map = IterableExtensions.<JsonObject, RepIssueMilestone>map(components, _function);
      List<RepIssueMilestone> _list = IterableExtensions.<RepIssueMilestone>toList(_map);
      return BasicServiceImpl.<List<RepIssueMilestone>>createResponseObject(_list);
    }
    IOException _exception = r.getException();
    return BasicServiceImpl.<List<RepIssueMilestone>>createErrorObject(_exception);
  }
  
  public ResponseObject<RepIssueVersion> getIssueVersion(final ProgressMonitor monitor, final String accountname, final String slug, final int id) {
    String _plus = ("repositories/" + accountname);
    String _plus_1 = (_plus + "/");
    String _plus_2 = (_plus_1 + slug);
    String _plus_3 = (_plus_2 + "/issues/versions/");
    String _plus_4 = (_plus_3 + Integer.valueOf(id));
    final ServiceEndpoint endpoint = this.createEndpoint(_plus_4);
    final ServiceResponse<JsonObject> r = endpoint.<JsonObject>get();
    boolean _isOk = r.isOk();
    if (_isOk) {
      JsonObject _content = r.getContent();
      BitbucketSession _session = this.getSession();
      RepIssueVersion _repIssueVersion = RepIssueVersionImpl.toRepIssueVersion(_content, _session);
      return BasicServiceImpl.<RepIssueVersion>createResponseObject(_repIssueVersion);
    }
    IOException _exception = r.getException();
    return BasicServiceImpl.<RepIssueVersion>createErrorObject(_exception);
  }
  
  public ResponseObject<List<RepIssueVersion>> getIssueVersions(final ProgressMonitor monitor, final String accountname, final String slug) {
    String _plus = ("repositories/" + accountname);
    String _plus_1 = (_plus + "/");
    String _plus_2 = (_plus_1 + slug);
    String _plus_3 = (_plus_2 + "/issues/milestones");
    final ServiceEndpoint endpoint = this.createEndpoint(_plus_3);
    final ServiceResponse<JsonObject> r = endpoint.<JsonObject>get();
    boolean _isOk = r.isOk();
    if (_isOk) {
      JsonObject _content = r.getContent();
      JsonArray _asJsonArray = _content.getAsJsonArray();
      final Iterable<JsonObject> components = JsonUtil.<JsonObject>iterable(_asJsonArray);
      final Function1<JsonObject,RepIssueVersion> _function = new Function1<JsonObject,RepIssueVersion>() {
          public RepIssueVersion apply(final JsonObject e) {
            BitbucketSession _session = BitbucketRepositoriesServiceImpl.this.getSession();
            RepIssueVersion _repIssueVersion = RepIssueVersionImpl.toRepIssueVersion(e, _session);
            return _repIssueVersion;
          }
        };
      Iterable<RepIssueVersion> _map = IterableExtensions.<JsonObject, RepIssueVersion>map(components, _function);
      List<RepIssueVersion> _list = IterableExtensions.<RepIssueVersion>toList(_map);
      return BasicServiceImpl.<List<RepIssueVersion>>createResponseObject(_list);
    }
    IOException _exception = r.getException();
    return BasicServiceImpl.<List<RepIssueVersion>>createErrorObject(_exception);
  }
  
  /**
   * =========================================================================================
   * PULLREQUEST APIS
   * =========================================================================================
   */
  public ResponseObject<List<RepPullRequestComment>> getPullRequestComments(final ProgressMonitor monitor, final String accountname, final String slug, final int pullRequestId) {
    String _plus = ("repositories/" + accountname);
    String _plus_1 = (_plus + "/");
    String _plus_2 = (_plus_1 + slug);
    String _plus_3 = (_plus_2 + "/pullrequests/");
    String _plus_4 = (_plus_3 + Integer.valueOf(pullRequestId));
    String _plus_5 = (_plus_4 + "/comments");
    final ServiceEndpoint endpoint = this.createEndpoint(_plus_5);
    final ServiceResponse<JsonObject> r = endpoint.<JsonObject>get();
    boolean _isOk = r.isOk();
    if (_isOk) {
      JsonObject _content = r.getContent();
      JsonArray _asJsonArray = _content.getAsJsonArray();
      final Iterable<JsonObject> components = JsonUtil.<JsonObject>iterable(_asJsonArray);
      final Function1<JsonObject,RepPullRequestComment> _function = new Function1<JsonObject,RepPullRequestComment>() {
          public RepPullRequestComment apply(final JsonObject e) {
            BitbucketSession _session = BitbucketRepositoriesServiceImpl.this.getSession();
            RepPullRequestComment _repPullRequestComment = RepPullRequestCommentImpl.toRepPullRequestComment(e, _session);
            return _repPullRequestComment;
          }
        };
      Iterable<RepPullRequestComment> _map = IterableExtensions.<JsonObject, RepPullRequestComment>map(components, _function);
      List<RepPullRequestComment> _list = IterableExtensions.<RepPullRequestComment>toList(_map);
      return BasicServiceImpl.<List<RepPullRequestComment>>createResponseObject(_list);
    }
    IOException _exception = r.getException();
    return BasicServiceImpl.<List<RepPullRequestComment>>createErrorObject(_exception);
  }
  
  public ResponseObject<RepPullRequestComment> getPullRequestComment(final ProgressMonitor monitor, final String accountname, final String slug, final int pullRequestId, final int commentId) {
    String _plus = ("repositories/" + accountname);
    String _plus_1 = (_plus + "/");
    String _plus_2 = (_plus_1 + slug);
    String _plus_3 = (_plus_2 + "/pullrequests/");
    String _plus_4 = (_plus_3 + Integer.valueOf(pullRequestId));
    String _plus_5 = (_plus_4 + "/comments/");
    String _plus_6 = (_plus_5 + Integer.valueOf(commentId));
    final ServiceEndpoint endpoint = this.createEndpoint(_plus_6);
    final ServiceResponse<JsonObject> r = endpoint.<JsonObject>get();
    boolean _isOk = r.isOk();
    if (_isOk) {
      JsonObject _content = r.getContent();
      BitbucketSession _session = this.getSession();
      RepPullRequestComment _repPullRequestComment = RepPullRequestCommentImpl.toRepPullRequestComment(_content, _session);
      return BasicServiceImpl.<RepPullRequestComment>createResponseObject(_repPullRequestComment);
    }
    IOException _exception = r.getException();
    return BasicServiceImpl.<RepPullRequestComment>createErrorObject(_exception);
  }
  
  public ResponseObject<RepPullRequestLike> getPullRequestLike(final ProgressMonitor monitor, final String accountname, final String slug, final int pullRequestId, final String likeraccount) {
    String _plus = ("repositories/" + accountname);
    String _plus_1 = (_plus + "/");
    String _plus_2 = (_plus_1 + slug);
    String _plus_3 = (_plus_2 + "/pullrequests/");
    String _plus_4 = (_plus_3 + Integer.valueOf(pullRequestId));
    String _plus_5 = (_plus_4 + "/likes/");
    String _plus_6 = (_plus_5 + likeraccount);
    final ServiceEndpoint endpoint = this.createEndpoint(_plus_6);
    final ServiceResponse<JsonObject> r = endpoint.<JsonObject>get();
    boolean _isOk = r.isOk();
    if (_isOk) {
      JsonObject _content = r.getContent();
      BitbucketSession _session = this.getSession();
      RepPullRequestLike _repPullRequestLike = RepPullRequestLikeImpl.toRepPullRequestLike(_content, _session);
      return BasicServiceImpl.<RepPullRequestLike>createResponseObject(_repPullRequestLike);
    }
    IOException _exception = r.getException();
    return BasicServiceImpl.<RepPullRequestLike>createErrorObject(_exception);
  }
  
  public ResponseObject<List<RepPullRequestLike>> getPullRequestLikes(final ProgressMonitor monitor, final String accountname, final String slug, final int pullRequestId) {
    String _plus = ("repositories/" + accountname);
    String _plus_1 = (_plus + "/");
    String _plus_2 = (_plus_1 + slug);
    String _plus_3 = (_plus_2 + "/pullrequests/");
    String _plus_4 = (_plus_3 + Integer.valueOf(pullRequestId));
    String _plus_5 = (_plus_4 + "/likes");
    final ServiceEndpoint endpoint = this.createEndpoint(_plus_5);
    final ServiceResponse<JsonObject> r = endpoint.<JsonObject>get();
    boolean _isOk = r.isOk();
    if (_isOk) {
      JsonObject _content = r.getContent();
      JsonArray _asJsonArray = _content.getAsJsonArray();
      final Iterable<JsonObject> components = JsonUtil.<JsonObject>iterable(_asJsonArray);
      final Function1<JsonObject,RepPullRequestLike> _function = new Function1<JsonObject,RepPullRequestLike>() {
          public RepPullRequestLike apply(final JsonObject e) {
            BitbucketSession _session = BitbucketRepositoriesServiceImpl.this.getSession();
            RepPullRequestLike _repPullRequestLike = RepPullRequestLikeImpl.toRepPullRequestLike(e, _session);
            return _repPullRequestLike;
          }
        };
      Iterable<RepPullRequestLike> _map = IterableExtensions.<JsonObject, RepPullRequestLike>map(components, _function);
      List<RepPullRequestLike> _list = IterableExtensions.<RepPullRequestLike>toList(_map);
      return BasicServiceImpl.<List<RepPullRequestLike>>createResponseObject(_list);
    }
    IOException _exception = r.getException();
    return BasicServiceImpl.<List<RepPullRequestLike>>createErrorObject(_exception);
  }
  
  /**
   * =========================================================================================
   * Repo Slug APIS
   * =========================================================================================
   */
  public ResponseObject<Map<String,RepSlugBranch>> getBranches(final ProgressMonitor monitor, final String accountname, final String slug) {
    String _plus = ("repositories/" + accountname);
    String _plus_1 = (_plus + "/");
    String _plus_2 = (_plus_1 + slug);
    String _plus_3 = (_plus_2 + "/branches");
    final ServiceEndpoint endpoint = this.createEndpoint(_plus_3);
    final ServiceResponse<JsonObject> r = endpoint.<JsonObject>get();
    boolean _isOk = r.isOk();
    if (_isOk) {
      HashMap<String,RepSlugBranch> _hashMap = new HashMap<String,RepSlugBranch>();
      final Map<String,RepSlugBranch> rv = _hashMap;
      JsonObject _content = r.getContent();
      Set<Entry<String,JsonElement>> _entrySet = _content.entrySet();
      final Procedure1<Entry<String,JsonElement>> _function = new Procedure1<Entry<String,JsonElement>>() {
          public void apply(final Entry<String,JsonElement> e) {
            String _key = e.getKey();
            JsonElement _value = e.getValue();
            JsonObject _asJsonObject = _value.getAsJsonObject();
            BitbucketSession _session = BitbucketRepositoriesServiceImpl.this.getSession();
            RepSlugBranch _repSlugBranch = RepSlugBranchImpl.toRepSlugBranch(_asJsonObject, _session);
            rv.put(_key, _repSlugBranch);
          }
        };
      IterableExtensions.<Entry<String,JsonElement>>forEach(_entrySet, _function);
      return BasicServiceImpl.<Map<String,RepSlugBranch>>createResponseObject(rv);
    }
    IOException _exception = r.getException();
    return BasicServiceImpl.<Map<String,RepSlugBranch>>createErrorObject(_exception);
  }
  
  public ResponseObject<String> getMainBranch(final ProgressMonitor monitor, final String accountname, final String slug) {
    String _plus = ("repositories/" + accountname);
    String _plus_1 = (_plus + "/");
    String _plus_2 = (_plus_1 + slug);
    String _plus_3 = (_plus_2 + "/branches");
    final ServiceEndpoint endpoint = this.createEndpoint(_plus_3);
    final ServiceResponse<JsonObject> r = endpoint.<JsonObject>get();
    boolean _isOk = r.isOk();
    if (_isOk) {
      JsonObject _content = r.getContent();
      String _string = JsonUtil.string(_content, "name");
      return BasicServiceImpl.<String>createResponseObject(_string);
    }
    IOException _exception = r.getException();
    return BasicServiceImpl.<String>createErrorObject(_exception);
  }
  
  public ResponseObject<Map<String,RepSlugTag>> getTags(final ProgressMonitor monitor, final String accountname, final String slug) {
    String _plus = ("repositories/" + accountname);
    String _plus_1 = (_plus + "/");
    String _plus_2 = (_plus_1 + slug);
    String _plus_3 = (_plus_2 + "/branches");
    final ServiceEndpoint endpoint = this.createEndpoint(_plus_3);
    final ServiceResponse<JsonObject> r = endpoint.<JsonObject>get();
    boolean _isOk = r.isOk();
    if (_isOk) {
      HashMap<String,RepSlugTag> _hashMap = new HashMap<String,RepSlugTag>();
      final Map<String,RepSlugTag> rv = _hashMap;
      JsonObject _content = r.getContent();
      Set<Entry<String,JsonElement>> _entrySet = _content.entrySet();
      final Procedure1<Entry<String,JsonElement>> _function = new Procedure1<Entry<String,JsonElement>>() {
          public void apply(final Entry<String,JsonElement> e) {
            String _key = e.getKey();
            JsonElement _value = e.getValue();
            JsonObject _asJsonObject = _value.getAsJsonObject();
            BitbucketSession _session = BitbucketRepositoriesServiceImpl.this.getSession();
            RepSlugTag _repSlugTag = RepSlugTagImpl.toRepSlugTag(_asJsonObject, _session);
            rv.put(_key, _repSlugTag);
          }
        };
      IterableExtensions.<Entry<String,JsonElement>>forEach(_entrySet, _function);
      return BasicServiceImpl.<Map<String,RepSlugTag>>createResponseObject(rv);
    }
    IOException _exception = r.getException();
    return BasicServiceImpl.<Map<String,RepSlugTag>>createErrorObject(_exception);
  }
  
  public ResponseObject<List<RepSlugFileHistory>> getFileHistory(final ProgressMonitor monitor, final String accountname, final String slug, final String node, final String path) {
    String _plus = ("repositories/" + accountname);
    String _plus_1 = (_plus + "/");
    String _plus_2 = (_plus_1 + slug);
    String _plus_3 = (_plus_2 + "/filehistory/");
    String _plus_4 = (_plus_3 + node);
    String _plus_5 = (_plus_4 + "/");
    String _plus_6 = (_plus_5 + path);
    final ServiceEndpoint endpoint = this.createEndpoint(_plus_6);
    final ServiceResponse<JsonObject> r = endpoint.<JsonObject>get();
    boolean _isOk = r.isOk();
    if (_isOk) {
      JsonObject _content = r.getContent();
      JsonArray _asJsonArray = _content.getAsJsonArray();
      final Iterable<JsonObject> histories = JsonUtil.<JsonObject>iterable(_asJsonArray);
      final Function1<JsonObject,RepSlugFileHistory> _function = new Function1<JsonObject,RepSlugFileHistory>() {
          public RepSlugFileHistory apply(final JsonObject e) {
            BitbucketSession _session = BitbucketRepositoriesServiceImpl.this.getSession();
            RepSlugFileHistory _repSlugFileHistory = RepSlugFileHistoryImpl.toRepSlugFileHistory(e, _session);
            return _repSlugFileHistory;
          }
        };
      Iterable<RepSlugFileHistory> _map = IterableExtensions.<JsonObject, RepSlugFileHistory>map(histories, _function);
      List<RepSlugFileHistory> _list = IterableExtensions.<RepSlugFileHistory>toList(_map);
      return BasicServiceImpl.<List<RepSlugFileHistory>>createResponseObject(_list);
    }
    IOException _exception = r.getException();
    return BasicServiceImpl.<List<RepSlugFileHistory>>createErrorObject(_exception);
  }
}
