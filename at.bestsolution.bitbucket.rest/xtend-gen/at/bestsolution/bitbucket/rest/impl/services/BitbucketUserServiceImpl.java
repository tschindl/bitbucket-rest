package at.bestsolution.bitbucket.rest.impl.services;

import at.bestsolution.bitbucket.rest.BitbucketSession;
import at.bestsolution.bitbucket.rest.BitbucketUserService;
import at.bestsolution.bitbucket.rest.impl.BitbucketServiceImpl.ServiceEndpoint;
import at.bestsolution.bitbucket.rest.impl.BitbucketServiceImpl.ServiceResponse;
import at.bestsolution.bitbucket.rest.impl.model.user.UserRepositoryImpl;
import at.bestsolution.bitbucket.rest.impl.model.user.UserRepositoryOverviewImpl;
import at.bestsolution.bitbucket.rest.impl.services.BasicServiceImpl;
import at.bestsolution.bitbucket.rest.impl.services.util.JsonUtil;
import at.bestsolution.bitbucket.rest.model.user.UserRepository;
import at.bestsolution.bitbucket.rest.model.user.UserRepositoryOverview;
import at.bestsolution.bitbucket.rest.util.ProgressMonitor;
import at.bestsolution.bitbucket.rest.util.ResponseObject;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.util.List;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@SuppressWarnings("all")
public class BitbucketUserServiceImpl extends BasicServiceImpl implements BitbucketUserService {
  public BitbucketUserServiceImpl(final BitbucketSession session) {
    super(session);
  }
  
  public ResponseObject<List<UserRepository>> getRepositories(final ProgressMonitor monitor) {
    ServiceEndpoint _createEndpoint = this.createEndpoint("user/repositories");
    final ServiceResponse<JsonArray> r = _createEndpoint.<JsonArray>get();
    boolean _isOk = r.isOk();
    if (_isOk) {
      JsonArray _content = r.getContent();
      final Iterable<JsonObject> items = JsonUtil.<JsonObject>iterable(_content);
      final Function1<JsonObject,UserRepository> _function = new Function1<JsonObject,UserRepository>() {
          public UserRepository apply(final JsonObject e) {
            BitbucketSession _session = BitbucketUserServiceImpl.this.getSession();
            UserRepository _userRepository = UserRepositoryImpl.toUserRepository(e, _session);
            return _userRepository;
          }
        };
      Iterable<UserRepository> _map = IterableExtensions.<JsonObject, UserRepository>map(items, _function);
      List<UserRepository> _list = IterableExtensions.<UserRepository>toList(_map);
      return BasicServiceImpl.<List<UserRepository>>createResponseObject(_list);
    } else {
      IOException _exception = r.getException();
      return BasicServiceImpl.<List<UserRepository>>createErrorObject(_exception);
    }
  }
  
  public ResponseObject<UserRepositoryOverview> getRespositoriesOverview(final ProgressMonitor monitor) {
    ServiceEndpoint _createEndpoint = this.createEndpoint("user/repositories/overview");
    final ServiceResponse<JsonObject> r = _createEndpoint.<JsonObject>get();
    boolean _isOk = r.isOk();
    if (_isOk) {
      JsonObject _content = r.getContent();
      BitbucketSession _session = this.getSession();
      UserRepositoryOverview _userRepositoryOverview = UserRepositoryOverviewImpl.toUserRepositoryOverview(_content, _session);
      return BasicServiceImpl.<UserRepositoryOverview>createResponseObject(_userRepositoryOverview);
    }
    IOException _exception = r.getException();
    return BasicServiceImpl.<UserRepositoryOverview>createErrorObject(_exception);
  }
}
