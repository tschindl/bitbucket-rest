package at.bestsolution.bitbucket.rest.impl.services.util;

import at.bestsolution.bitbucket.rest.impl.services.util.JsonArrayIterator;
import at.bestsolution.bitbucket.rest.impl.services.util.JsonObjectConverter;
import at.bestsolution.bitbucket.rest.model.base.RestEnum;
import com.google.common.base.Objects;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@SuppressWarnings("all")
public class JsonUtil {
  public static String string(final JsonObject o, final String name) {
    String _xblockexpression = null;
    {
      final JsonElement e = o.get(name);
      String _switchResult = null;
      boolean _matched = false;
      if (!_matched) {
        if (e instanceof JsonNull) {
          final JsonNull _jsonNull = (JsonNull)e;
          _matched=true;
          _switchResult = null;
        }
      }
      if (!_matched) {
        String _asString = e==null?(String)null:e.getAsString();
        _switchResult = _asString;
      }
      _xblockexpression = (_switchResult);
    }
    return _xblockexpression;
  }
  
  public static boolean bool(final JsonObject o, final String name) {
    boolean _xblockexpression = false;
    {
      final JsonElement e = o.get(name);
      boolean _switchResult = false;
      boolean _matched = false;
      if (!_matched) {
        if (e instanceof JsonNull) {
          final JsonNull _jsonNull = (JsonNull)e;
          _matched=true;
          _switchResult = false;
        }
      }
      if (!_matched) {
        boolean _asBoolean = e==null?false:e.getAsBoolean();
        _switchResult = _asBoolean;
      }
      _xblockexpression = (_switchResult);
    }
    return _xblockexpression;
  }
  
  public static int integer(final JsonObject o, final String name) {
    int _xblockexpression = (int) 0;
    {
      final JsonElement e = o.get(name);
      int _switchResult = (int) 0;
      boolean _matched = false;
      if (!_matched) {
        if (e instanceof JsonNull) {
          final JsonNull _jsonNull = (JsonNull)e;
          _matched=true;
          _switchResult = 0;
        }
      }
      if (!_matched) {
        int _asInt = e==null?0:e.getAsInt();
        _switchResult = _asInt;
      }
      _xblockexpression = (_switchResult);
    }
    return _xblockexpression;
  }
  
  public static Integer integerObject(final JsonObject o, final String name) {
    Integer _xblockexpression = null;
    {
      final JsonElement e = o.get(name);
      Integer _switchResult = null;
      boolean _matched = false;
      if (!_matched) {
        if (e instanceof JsonNull) {
          final JsonNull _jsonNull = (JsonNull)e;
          _matched=true;
          _switchResult = null;
        }
      }
      if (!_matched) {
        int _asInt = e==null?0:e.getAsInt();
        _switchResult = _asInt;
      }
      _xblockexpression = (_switchResult);
    }
    return _xblockexpression;
  }
  
  public static JsonObject jsonObject(final JsonObject o, final String name) {
    JsonObject _xblockexpression = null;
    {
      final JsonElement e = o.get(name);
      JsonObject _switchResult = null;
      boolean _matched = false;
      if (!_matched) {
        if (e instanceof JsonNull) {
          final JsonNull _jsonNull = (JsonNull)e;
          _matched=true;
          _switchResult = null;
        }
      }
      if (!_matched) {
        JsonObject _asJsonObject = e==null?(JsonObject)null:e.getAsJsonObject();
        _switchResult = _asJsonObject;
      }
      _xblockexpression = (_switchResult);
    }
    return _xblockexpression;
  }
  
  public static Date date(final JsonObject o, final String name) {
    final JsonElement e = o.get(name);
    final String v = e==null?(String)null:e.getAsString();
    boolean _notEquals = (!Objects.equal(v, null));
    if (_notEquals) {
      try {
        SimpleDateFormat _simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ssX");
        return _simpleDateFormat.parse(v);
      } catch (Exception _e) {
        throw Exceptions.sneakyThrow(_e);
      }
    }
    return null;
  }
  
  public static <T extends Enum<T> & RestEnum> T enumeration(final JsonObject o, final String name, final Class<T> enumClass) {
    final String s = JsonUtil.string(o, name);
    T[] _enumConstants = enumClass.getEnumConstants();
    for (final T t : _enumConstants) {
      String _restValue = t.getRestValue();
      boolean _equals = _restValue.equals(s);
      if (_equals) {
        return t;
      }
    }
    String _plus = ("No enumeration \'" + s);
    String _plus_1 = (_plus + "\' is found in ");
    String _name = enumClass.getName();
    String _plus_2 = (_plus_1 + _name);
    IllegalArgumentException _illegalArgumentException = new IllegalArgumentException(_plus_2);
    throw _illegalArgumentException;
  }
  
  public static <T extends JsonElement> Iterable<T> iterable(final JsonObject o, final String name) {
    final JsonElement e = o.get(name);
    boolean _or = false;
    boolean _equals = Objects.equal(e, null);
    if (_equals) {
      _or = true;
    } else {
      _or = (_equals || (e instanceof JsonNull));
    }
    if (_or) {
      return Collections.<T>emptyList();
    }
    JsonElement _get = o.get(name);
    JsonArray _asJsonArray = _get.getAsJsonArray();
    return JsonUtil.<T>iterable(_asJsonArray);
  }
  
  public static <T extends Object> List<T> list(final JsonObject o, final String name, final JsonObjectConverter<T> converter) {
    Iterable<?> _iterable = JsonUtil.iterable(o, name);
    final Function1<JsonObject,T> _function = new Function1<JsonObject,T>() {
        public T apply(final JsonObject e) {
          T _create = converter.create(e);
          return _create;
        }
      };
    Iterable<T> _map = IterableExtensions.<JsonObject, T>map(((Iterable<JsonObject>) _iterable), _function);
    return IterableExtensions.<T>toList(_map);
  }
  
  public static <T extends JsonElement> Iterable<T> iterable(final JsonArray o) {
    return JsonArrayIterator.<T>createIterable(o);
  }
}
